# RadioMonteCarlow 2 Monte Carlo results

This is the repository containing the Monte Carlo configuration, runcards, analysis scripts (where applicable), and results for the RadioMonteCarlow 2 review that followed the [5<sup>th</sup> workstop](https://indico.psi.ch/event/13707/) at the University of Zurich.

We aim for 100% reproducibility and Open Science by providing the exact configurations we have used to obtain the results.
This naturally includes the definition of the observables (see below) and the runcards but also the exact versions of the code used (either using tarballs or git submodules) and even the exact Linux distro and compiler version used.
Where possible, we achieve this using Containerfiles that are stored here.
A snapshot of this repository will be archived on Zenodo in due time.

The data in this repository is licenced under [CC-BY-4.0](https://gitlab.com/radiomontecarlow2/monte-carlo-results/-/blob/root/LICENCE?ref_type=heads).
The CI code is licenced under [GPLv3](https://gitlab.com/radiomontecarlow2/monte-carlo-results/-/blob/root/.ci/LICENCE?ref_type=heads).
The Monte Carlo codes included in this repository are licenced under their own licences.

## Monte Carlo codes

The following codes will definitely be included while other codes might be added later.
In alphabetical order

 * [BabaYaga@NLO](https://www2.pv.infn.it/~hepcomplex/babayaga.html)

 * [KKMC](https://dx.doi.org/10.17632/8w4vrpttpc.1)

 * MCGPJ

 * [McMule](https://mule-tools.gitlab.io/)

 * [PHOKHARA v10](https://looptreeduality.csic.es/phokhara/)

 * [Sherpa](https://sherpa-team.gitlab.io/)

The overall run coordinator is @yannickulrich.
If you have questions or concerns, please get in touch.

When adding your code to the repositoy as tarballs, please use Git LFS.

### Git LFS
If you have never used git LFS before, you need to install it
```shell
$ dnf install git-lfs  # Fedora, RHEL, CentOS, etc.
$ apt install git-lfs  # Ubuntu, Debian, etc.
$ apk add git-lfs      # Alpine
$ brew install git-lfs # macOS
```
If you are using [git for windows](https://gitforwindows.org/) you will already have LFS.
Alternatively, you can obtain a binary [here](https://github.com/git-lfs/git-lfs/releases).
Once you have install `git-lfs`, you need to run
```shell
$ git lfs install
```
once.
From this point on, you will automagically use LFS for large blobs.
If you already have a local version of this (or any other repository), please also run
```shell
$ git lfs pull
```
to update any local copy.

## Structure of this repository

This repository has two folders `codes` and `scenarios`.
The former contains a subfolder for each Monte Carlo including the code itself and build instructions (ideally as a Containerfile).
The `scenarios` folder contains four scenarios we plan to run each code on, eg. `scenarios/cmd/babayaga` or `scenarios/kloe/mcmule`.
The organisation of these folders are mostly left to the run coordinators as long as the metadata files (see below) are present.

Your analysis scripts may produce plots but please do not add them to this repository!
Plotting will be done centrally using a CI script and the data and metadata you provide.

## Commits
Please prefix any commit with the name of the code or scenario (please be consistent), `docs`, or `infra`.

## Metadata

Each run folder should contain at least the following [YAML](https://yaml.org) metadata file
```yaml
run:
  code: <name of your code>
  scenario: <name of your scenario>
  effects:
    Please write a short comment (consistent across all your runs)
    effects your code includes here. If this is different for the
    different channels, please add this to the channel

  channels:
    ee: # only include if ee->ee is present
      effects:
        Space for extra comments on effects
      runtime: <total runtime in seconds for this channel>
      xsec:
        - contribution: LO
          value: <integrated cross section of the LO in nano-barn>
          error: <error of value>
        - contribution: NLO
          value: <integrated cross section of the NLO in nano-barn>
          error: <error of value>
        - contribution: PS
          value: <integrated cross section of the PS in nano-barn>
          error: <error of value>
        - contribution: PS+NLO
          value: <integrated cross section of the PS+NLO in nano-barn>
          error: <error of value>
      best: <name of your best contribution>
      observables:
        - file: /path/to/obs1.csv
          x: "th+"
          contributions:
            - LO
            - NLO
            - PS
            - PS+NLO
            - ...
    mm: # only include if ee->\mu\mu is present
    pp: # only include if ee->\pi\pi is present
```
A schema for this can be found in `.ci/metadata.py`.
`best` should refer to the contribution your code claims is the best possible for this channel.
This will be used for code-by-code comparisons.
In the above example it could be `PS+NLO`.
Please also include the total integrated cross section as `xsec` for each contribution.
If you code produces this independently from the histograms, please use this value.
Otherwise, integrate one of the histograms that do not have optional cuts.

## Data storage

All resulting histograms must be presented in a uniform CSV format that is *directly* produced by the analysis scripts included in this repository.
No manual modification or conversion should be required (please get in touch if you need help with this).
Please add one CSV file for each observable with columns names as laid out in the `contributions` section of the metadata.

Please always include the statistical error as column prefixed with `d`, i.e. `dLO` or `dNNLO`.
This does not count as a seperate `contributions` for the metadata.

Example:
```csv
# xl,xh,LO,dLO,NLO,dNLO
0.00e+00,1.00e-01,2.00e-05,0.00e+00,0.00e+00,0.00e+00
1.00e-01,2.00e-01,2.00e-05,4.16e-02,8.49e-03,3.31e-02
2.00e-01,3.00e-01,3.00e-05,1.66e-01,6.79e-02,9.85e-02
```
The *x* value should be given as *x*<sub>*l*</sub> and *x*<sub>*h*</sub>, the lower and upper bounds of the histogram bins.
While this introduces redundancy, it is less ambigous than alternatives (only bin centre, only lower bound, etc).
The different values (`LO`, `NLO`, etc.) should be $`d\sigma/dX`$ for the distribution wrt. to $`X`$.
Please normalise so that integrating over the distribution gives the cross section in nano-barn, i.e.
```math
\sigma = \int_{\rm lower}^{\rm upper} dX\frac{d\sigma}{dX} \approx \frac{1}{\Delta}\sum_{i=1}^n \text{bin}_i
```
where $`\Delta`$ is the bin width (0.1 in the example above).

If you are using python, you can use this code
```python
np.savetxt('/path/to/data/file.csv',dat, delimiter=',', header="xl,xh,LO,dLO,NLO,dNLO")
```

## Automated plotting

This repository uses a CI script for automated validation and plotting.
You can run this locally by
```shell
# validation only
$ python .ci/loader.py
...
Parsing complete, 0 errors and 8 warnings
# validation and plotting
$ python .ci/main.py
```
For the validation you need to install [numpy](https://pypi.org/project/numpy/) and [strictyaml](https://pypi.org/project/strictyaml/)
```shell
$ pip install numpy strictyaml
```
For the plotting you also need [jinja2](https://pypi.org/project/jinja2), [matplotlib](https://pypi.org/project/matplotlib), [mistune](https://pypi.org/project/mistune), and [mpld3](https://pypi.org/project/mpld3).
```shell
$ pip install jinja2 matplotlib mistune mpld3
```
Please at least make sure that the validation passes.
