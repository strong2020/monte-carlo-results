# Vacuum Polarizatin tables 
Most precise VP tables available from this compilations of R(s) data:  

- KNT18 (v3.0)
- [hadr5x23 by Fred Jegerlehner](http://www-com.physik.hu-berlin.de/~fjeger/software.html)
- [NSK v2.9](https://cmd.inp.nsk.su/~ignatov/vpl/)

## NSK version
Precalculated VP tables:

- vpol_all_bare_sum_v2.9.dat - full VP
- vpol_bare_lept_v2.9.dat - lepton VP
- hadronic part can be taken as VPfull-VPlept

Format of files: 
$s, GeV^2; Re P(-s) ; Re P(s); Im P(s); \sigma(Re P(-s)); \sigma(Re P(s)); \sigma(Im P(s)); cov[Re P(s),Im P(s)]/(\sigma_{Re}\sigma_{Im})$

The table can be used only with linear interpolation between nodes (had better avoid using splines).  
C++ example is available at [the NSK webpage](https://cmd.inp.nsk.su/~ignatov/vpl/).  
Fortran example is in the current directory:
```
gfortran vpolNsk.F test.F
./a.out
```

The vacuum polarization, $P(s)$ - complex function, is applied to $\alpha_{QED}$ as:  
$\alpha(s) = \alpha_0/(1-P(s))$

## Known issues
KNT and alphaQED - don't use proper "bared" parameters of narrow resonances like $J/\psi$, etc - the VP in that regions cannot be used from these packages.  
alphaQED19 - use dressed PDG parameters of $\phi$ , the VP values are shifted by up to 2.5% at $\phi$ resonance (2% error effect at $\sqrt{s}=1.02\, GeV$).     
alphaQED23 - has not a smooth behaviour at $\phi$ resonance and about 1% away from KNT and NSK values at $\sqrt{s}=1.02\, GeV$.

