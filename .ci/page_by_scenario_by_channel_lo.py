from pathlib import Path
import jinja2
from build_page import *

folder = "by-scenario-lo"
label = "by scenario at LO"
description = "browser the results by experimental scenario and channel and compare the LO result of each code"


def main(results: list[MonteCarlo], root: Path) -> None:
    templateEnv = jinja2.Environment(
        loader=templates_loader, trim_blocks=True, lstrip_blocks=True
    )
    template = templateEnv.get_template(
        "index.html.jinja2",
    )

    # sort results by scenario
    scenarios: dict[tuple[str, str], list[MonteCarlo]] = {}
    channel_list: dict[str, set[str]] = {}
    for result in results:
        for c_name in result.channels.keys():
            try:
                scenarios[(result.scenario.name, c_name)].append(result)
            except KeyError:
                scenarios[(result.scenario.name, c_name)] = [result]

            try:
                channel_list[result.scenario.name].add(c_name)
            except KeyError:
                channel_list[result.scenario.name] = set([c_name])

    for (s_name, c_name), results in scenarios.items():
        scenario = results[0].scenario
        channel = results[0].channels[c_name]
        plots = []

        for obs in scenario.observables.values():
            data = []
            colours = []
            for result in results:
                try:
                    hist = result.channels[c_name].observables[obs.name]
                    hist.get("LO")
                    data.append(
                        (
                            result.channels[c_name].observables[obs.name],
                            "LO",
                            result.code,
                        )
                    )
                    colours.append(f"C{colour_map.get(result.code)}")
                except KeyError:
                    print(
                        f"Warning: {result.code} is missing {obs.name} for channel {c_name} @ LO"
                    )
                if result.channels[c_name].is_reference:
                    nref = len(data) - 1

            plots.append(
                (
                    f"Observable \\({obs.latex}\\)",
                    build_plot(
                        data,
                        *[(i, nref, "ratio") for i in range(len(data))],
                        cmp_same_panel="ratio",
                        colours=colours,
                    ),
                )
            )

        path = root / s_name / c_name / "index.html"
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(
            build_plot_page(
                f"All results for {s_name} channel \\({channel.channel2latex[c_name]}\\)",
                plots,
                readme=results[0].scenario.docs,
            )
        )

    for s_name, channels in channel_list.items():
        (root / s_name).mkdir(parents=True, exist_ok=True)
        path = root / s_name / "index.html"
        path.write_text(
            template.render(
                title=f"Channels for scenario {s_name}",
                children=[(c, f"\\({channel.channel2latex[c]}\\)") for c in channels],
                description=f'Learn more about {s_name} on <a href="{GITLAB_URL}/-/tree/root/scenarios/{s_name}">GitLab</a>',
            )
        )

    (root / "index.html").write_text(
        template.render(
            title=f"Scenario",
            children=[(c, c) for c in channel_list.keys()],
        )
    )
