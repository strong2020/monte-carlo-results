from pathlib import Path
from strictyaml import Any as Any, Map, ScalarValidator

GITLAB_URL: str
WEB_ROOT: str
CI_ROOT: Path
PAGES_ROOT: Path
run_schema: Map

class Maths(ScalarValidator):
    def validate_scalar(self, chunk): ...

scenario_schema: Map
schema: list[str]
colour_map: dict[str, int]
