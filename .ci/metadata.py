import strictyaml  # type: ignore
from strictyaml import (
    Any,
    Bool,
    Float,
    Int,
    Map,
    MapCombined,
    Optional,
    ScalarValidator,
    Seq,
    Str,
)
from strictyaml.exceptions import YAMLSerializationError  # type: ignore
import math
from pathlib import Path

GITLAB_URL = "https://gitlab.com/strong2020/monte-carlo-results/"
WEB_ROOT = "https://strong2020.gitlab.io/monte-carlo-results/"
CI_ROOT: Path = Path(".ci")
PAGES_ROOT: Path = Path("public")


_channel_schema: Map = Map(
    {
        "runtime": Float(),
        "xsec": Seq(
            Map(
                {
                    "contribution": Str(),
                    "value": Float(),
                    Optional("error", default=0): Float(),
                }
            )
        ),
        Optional("effects", ""): Str(),
        "observables": Seq(
            Map(
                {
                    "file": Str(),
                    "x": Str(),
                    "contributions": Seq(Str()),
                }
            )
        ),
        "best": Str(),
    }
)


run_schema: Map = Map(
    {
        "run": Map(
            {
                "code": Str(),
                "scenario": Str(),
                "effects": Str(),
                "channels": Map(
                    {
                        Optional("ee"): _channel_schema,
                        Optional("mm"): _channel_schema,
                        Optional("pp"): _channel_schema,
                        Optional("gg"): _channel_schema,
                        Optional("eeg"): _channel_schema,
                        Optional("mmg"): _channel_schema,
                        Optional("ppg"): _channel_schema,
                    }
                ),
            }
        )
    }
)


class Maths(ScalarValidator):  # type: ignore
    def validate_scalar(self, chunk):  # type: ignore
        val = chunk.contents
        try:
            return eval(val, {}, math.__dict__)
        except Exception as e:
            raise YAMLSerializationError(
                f"encoutered error {e} during parsing of {val}"
            )


_observable_schema = Map(
    {
        "latex": Str(),
        "lower": Maths(),
        "upper": Maths(),
        "nbins": Int(),
        "tree": Bool(),
        Optional("pref"): Maths(),
        Optional("units"): Str(),
        Optional("extracut"): Bool(),
    }
)

scenario_schema: Map = Map(
    {
        "scenario": Map(
            {
                "name": Str(),
                "energy": Float(),
                "ISR": Bool(),
                "ncuts": Int(),
                "observables": MapCombined({}, Str(), _observable_schema),
                "references": MapCombined({}, Str(), Str()),
            }
        )
    }
)

schema: list[str] = [
    "#029e73",
    "#d55e00",
    "#cc78bc",
    "#ca9161",
    "#fbafe4",
    "#949494",
    "#ece133",
    "#56b4e9",
    "#0173b2",
    "#de8f05",
]

colour_map: dict[str, int] = {
    "AfkQed": 7,
    "BabaYaga": 8,
    "KKMC": 4,
    "MCGPJ": 3,
    "McMule": 5,
    "Phokhara": 1,
    "Sherpa": 0,
}
