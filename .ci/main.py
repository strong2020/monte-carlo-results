from build_page import *
from typing import Any, Generator
from pathlib import Path
import jinja2
import importlib
from cycler import cycler
from matplotlib import rc


def import_modules(root: Path = CI_ROOT) -> Generator[Any, None, None]:
    for fn in root.iterdir():
        if fn.is_dir():
            continue
        if fn.suffix != ".py":
            continue
        if not fn.name.startswith("page_"):
            continue
        yield importlib.import_module(fn.stem)


def main() -> None:
    templateEnv = jinja2.Environment(
        loader=templates_loader, trim_blocks=True, lstrip_blocks=True
    )
    template = templateEnv.get_template(
        "main.html.jinja2",
    )
    children = []

    results = list(load_all())
    for mod in import_modules():
        (PAGES_ROOT / mod.folder).mkdir(parents=True, exist_ok=True)
        mod.main(results, PAGES_ROOT / mod.folder)
        children.append((mod.folder, mod.label, mod.description))

    (PAGES_ROOT / "index.html").write_text(
        template.render(title="Results overview", children=children)
    )


if __name__ == "__main__":
    rc("axes", prop_cycle=cycler("color", schema))
    main()
