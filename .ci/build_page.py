import matplotlib.pyplot as plt
import mpld3  # type: ignore
from mpld3._display import NumpyEncoder  # type: ignore
import jinja2
from pathlib import Path
import json
from loader import *
import numpy as np
import mistune
import re

from matplotlib.artist import Artist
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from typing import Any, Generator, Optional, Union, Sequence

templates_loader = jinja2.FileSystemLoader(CI_ROOT / "templates")


class MyRenderer(mistune.HTMLRenderer):
    def heading(self, text: str, level: int, **kwargs: Any) -> str:
        id = kwargs.get("id", text.lower().replace(" ", ""))
        return f'<h{level+1}><a href="#{id}" class="anchor">&#128279;</a>{text}</h{level+1}>'

    def inline_maths(self, text: str) -> str:
        return f"\\({text}\\)"

    def block_code(self, code: str, info: Optional[str] = None) -> str:
        if info == "math":
            return f"$$\n{mistune.escape(code)}\n$$"
        else:
            return f"<pre><code>{mistune.escape(code)}</code></pre>"


def gitlabmaths(md: mistune.Markdown) -> None:
    def parse_inline(
        inline: mistune.InlineParser, m: re.Match[str], state: mistune.InlineState
    ) -> int:
        text = m.group("maths_text")
        state.append_token({"type": "inline_maths", "raw": text})
        return m.end()

    INLINE_MATHS_PATTERN = r"\$`(?!\s)(?P<maths_text>.+?)(?!\s)`\$"
    md.inline.register(
        "inline_maths", INLINE_MATHS_PATTERN, parse_inline, before="codespan"
    )


markdown = mistune.create_markdown(renderer=MyRenderer(), plugins=[gitlabmaths])


def make_plot(
    ax: Axes, x: Any, y: Any, e: Any, colour: Optional[str] = None
) -> list[Artist]:
    if colour:
        artist = ax.step(x, y, where="mid", color=colour)
    else:
        artist = ax.step(x, y, where="mid")
    col = artist[0].get_color()
    error = ax.fill_between(
        x,
        y + e,
        y - e,
        step="mid",
        facecolor=col,
        edgecolor=None,
    )
    return [artist[0], error]


def build_plot(
    entries: Sequence[
        Union[tuple[Histograms, str | None], tuple[Histograms, str | None, str]]
    ],
    *cmp: tuple[int, int, str],
    cmp_same_panel: Optional[str] = None,
    colours: Optional[Sequence[str | None]] = None,
) -> Optional[str]:
    # TODO fix latex https://github.com/mpld3/mpld3/issues/14
    obss = set(i[0].observable for i in entries)
    if len(obss) == 0:
        return None
    elif len(obss) > 1:
        raise ValueError("All histograms need to have the same observable")
    else:
        obs = list(obss)[0]

    if colours is None:
        colours = [None for _ in range(len(entries))]
    elif len(colours) != len(entries):
        raise ValueError(f"Have {len(entries)} entries but {len(colours)} colours")

    legend = {}

    if len(cmp) > 0:
        if cmp_same_panel:
            fig, axs = plt.subplots(2, sharex=True, gridspec_kw={"hspace": 0})
            assert isinstance(axs, np.ndarray)
        else:
            fig, axs = plt.subplots(
                len(cmp) + 1, sharex=True, gridspec_kw={"hspace": 0}
            )
            assert isinstance(axs, np.ndarray)

        for it, c in zip(entries, colours):
            if len(it) == 2:
                hist, contribution = it
                name = f"{hist.parent.parent.code} -- {contribution}"
            elif len(it) == 3:
                hist, contribution, name = it

            dat = hist.get(contribution)
            legend[name] = make_plot(axs[0], hist.x(), dat[:, 0], dat[:, 1], c)

        for n, (i1, i2, method) in enumerate(cmp):
            if len(entries[i1]) == 2:
                hist1, contribution1 = entries[i1]  # type: ignore
                name1 = f"{hist1.parent.parent.code} -- {contribution1}"
            elif len(entries[i1]) == 3:
                hist1, contribution1, name1 = entries[i1]  # type: ignore
            if len(entries[i2]) == 2:
                hist2, contribution2 = entries[i2]  # type: ignore
                name2 = f"{hist2.parent.parent.code} -- {contribution2}"
            elif len(entries[i2]) == 3:
                hist2, contribution2, name2 = entries[i2]  # type: ignore

            dat1 = hist1.get(contribution1)
            dat2 = hist2.get(contribution2)
            c = colours[i1]

            y1 = dat1[:, 0]
            y2 = dat2[:, 0]
            e12 = dat1[:, 1] ** 2
            e22 = dat2[:, 1] ** 2

            if cmp_same_panel:
                ax = axs[1]
            else:
                ax = axs[n + 1]

            if method == "ratio":
                p = make_plot(
                    ax,
                    hist1.x(),
                    y1 / y2,
                    np.sqrt(e22 * y1**2 / y2**4 + e12 / y2**2),
                    c,
                )
                ax.set_ylabel(cmp_same_panel or f"{name1} / {name2}")
            elif method == "ratio-1":
                p = make_plot(
                    ax,
                    hist1.x(),
                    y1 / y2 - 1,
                    np.sqrt(e22 * y1**2 / y2**4 + e12 / y2**2),
                    c,
                )
                ax.set_ylabel(cmp_same_panel or f"{name1} / {name2} - 1")
            elif method == "diff":
                p = make_plot(ax, hist1.x(), y1 - y2, np.sqrt(e12 + e22), c)
                ax.set_ylabel(cmp_same_panel or f"{name1} - {name2}")
            legend[name1] += p

        axy = axs[0]
        axx = axs[-1]

    else:
        fig, ax = plt.subplots()
        for it, c in zip(entries, colours):
            if len(it) == 2:
                hist, contribution = it
                name = f"{hist.parent.parent.code} -- {contribution}"
            elif len(it) == 3:
                hist, contribution, name = it

            dat = hist.get(contribution)
            legend[name] = make_plot(ax, hist.x(), dat[:, 0], dat[:, 1], c)

        axy = ax
        axx = ax

    axy.set_ylabel(f"$\\mathrm{{d}}\\sigma/\\mathrm{{d}}{obs.latex}$")
    if obs.units:
        axx.set_xlabel(f"${obs.latex}\\,/\\,\\mathrm{{ {obs.units} }}$")
    else:
        axx.set_xlabel(f"${obs.latex}$")

    if len(legend) == 0:
        return  # type: ignore
    labels, handles = zip(*legend.items())
    interactive_legend = mpld3.plugins.InteractiveLegendPlugin(
        handles,
        labels,
        alpha_unsel=0,
        alpha_over=1.5,
        start_visible=True,
    )
    mpld3.plugins.connect(fig, interactive_legend)
    fig.subplots_adjust(right=0.8)
    mpld3fig = json.dumps(mpld3.fig_to_dict(fig), cls=NumpyEncoder)
    plt.close(fig)
    return mpld3fig


def build_plot_page(
    title: str, plots: list[tuple[str, str | None]], readme: Optional[str] = None
) -> str:
    templateEnv = jinja2.Environment(
        loader=templates_loader, trim_blocks=True, lstrip_blocks=True
    )

    template = templateEnv.get_template(
        "plots.html.jinja2",
    )
    plugin_css = mpld3.plugins.InteractiveLegendPlugin.css_
    plugin_js = mpld3.plugins.InteractiveLegendPlugin.JAVASCRIPT

    if readme:
        description = markdown("# Definitions\n" + readme)
    else:
        description = None

    return template.render(
        enumerate=enumerate,
        title=title,
        d3url=mpld3.urls.D3_URL,
        d3xyurl="https://cdn.jsdelivr.net/npm/d3-xyzoom@1.5.0/build/d3-xyzoom.min.js",
        mpld3url=mpld3.urls.MPLD3MIN_URL,
        plugin_css=plugin_css,
        plugin_js=plugin_js,
        sections=[i for i, j in plots if j],
        description=description,
        plots=[fig for _, fig in plots if fig],
    )
