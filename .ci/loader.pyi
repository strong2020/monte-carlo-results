from metadata import *
from pathlib import Path
from typing import Any, Generator
import numpy.typing

nda = numpy.typing.NDArray[np.float64]
scenario_root: Path

class verr(Exception):
    caller: Any
    msg: str
    severity: int
    def __init__(self, caller: Any, msg: str, severity: int = 1) -> None: ...

class Observable:
    name: str
    latex: str
    lower: float
    upper: float
    nbins: int
    tree: bool
    pref: float
    units: str | None
    def __init__(self, name: str, d: dict[str, Any]) -> None: ...
    @property
    def xvalues(self) -> nda: ...
    def __hash__(self) -> int: ...
    def validate(self) -> Generator[verr, None, None]: ...

class Scenario:
    name: str
    energy: float
    ISR: bool
    ncuts: int
    channels: list[str]
    observables: dict[str, Observable]
    references: dict[str, str]
    folder: Path
    docs: str
    def __init__(self, folder: Path) -> None: ...
    def validate(self) -> Generator[verr, None, None]: ...

class Histograms:
    cols: list[str]
    dx: float
    best: str
    nbins: int
    parent: Channel
    observable: Observable
    file: Path
    def __init__(self, parent: Channel, d: dict[str, Any], obs: Observable) -> None: ...
    def get(self, contr: str | None = None) -> nda: ...
    def extract(self, k: str) -> nda: ...
    def x(self, opt: str = 'centre') -> nda | tuple[nda, nda]: ...
    def validate(self) -> Generator[verr, None, None]: ...

class Channel:
    channel2latex: dict[str, str]
    runtime: float
    xsec: dict[str, float]
    observables: dict[str, Histograms]
    best: str
    is_reference: bool
    parent: MonteCarlo
    name: str
    latex: str
    def __init__(self, parent: MonteCarlo, name: str, d: dict[str, Any]) -> None: ...
    def validate(self) -> Generator[verr, None, None]: ...

class MonteCarlo:
    code: str
    scenario: Scenario
    effects: str
    channels: dict[str, Channel]
    folder: Path
    def __init__(self, scenario: Scenario, folder: Path) -> None: ...
    def validate(self) -> Generator[verr, None, None]: ...

def load_all(scenario_name: str | None = None) -> Generator[MonteCarlo, None, None]: ...
