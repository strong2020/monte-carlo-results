from __future__ import annotations
import string
import numpy as np
from typing import Any, Generator, Optional
import numpy.typing
from pathlib import Path
import strictyaml  # type: ignore
import sys
from metadata import *

nda = numpy.typing.NDArray[np.float64]

scenario_root: Path = Path(__file__).parent.parent.resolve() / "scenarios"


class verr(Exception):
    caller: Any
    msg: str
    severity: int

    def __init__(self, caller: Any, msg: str, severity: int = 1):
        self.caller = caller
        self.msg = msg
        self.severity = severity

    def __str__(self) -> str:
        if self.severity == 1:
            return f"{self.caller.__repr__()} failed: {self.msg}\n"
        else:
            return f"{self.caller.__repr__()} warned: {self.msg}\n"


class Observable:
    name: str
    latex: str
    lower: float
    upper: float
    nbins: int
    tree: bool
    pref: float = 1
    units: Optional[str] = None
    xmapping: dict[str, str] = {"e": "e", "m": "\\mu", "p": "\\pi"}

    def __init__(self, name: str, d: dict[str, Any]):
        self.name = name
        self._dict = d
        self.latex = d["latex"]
        self.lower = d["lower"]
        self.upper = d["upper"]
        self.nbins = d["nbins"]
        self.tree = d["tree"]
        self.pref = d.get("pref", 1)
        self.units = d.get("units", None)

    @property
    def xvalues(self) -> nda:
        x = np.linspace(self.lower, self.upper, self.nbins + 1)
        return np.column_stack((x[:-1], x[1:]))

    def __repr__(self) -> str:
        return f"<Observable {self.name} [{self.lower}{self.units or ''}, {self.upper}{self.units or ''}] {self.nbins} bins>"

    def __hash__(self) -> int:
        return hash(
            (
                self.name,
                self.lower,
                self.upper,
                self.nbins,
                self.tree,
                self.pref,
                self.units,
            )
        )

    def __eq__(self, other: Any) -> bool:
        return hash(self) == hash(other)

    def validate(self) -> Generator[verr, None, None]:
        if self.upper < self.lower:
            yield verr(self, f"upper {self.upper} < lower {self.lower}")
        if self.nbins < 1:
            yield verr(self, f"nbins {self.nbins} < 1")

    def narrow(self, channel: str) -> Observable:
        newself = self.__class__(self.name, self._dict)
        newself.latex = newself.latex.replace("X", self.xmapping[channel])
        return newself


class Scenario:
    name: str
    energy: float
    ISR: bool
    ncuts: int
    channels: list[str]
    observables: dict[str, Observable]
    references: dict[str, str]
    folder: Path
    docs: str

    def __init__(self, folder: Path):
        self.folder = folder
        self._yaml = strictyaml.load(
            (self.folder / "metadata.yaml").read_text(), scenario_schema
        )
        self._dict = self._yaml.data["scenario"]

        self.name = self._dict["name"]
        self.references = self._dict["references"]
        self.energy = self._dict["energy"]
        self.ISR = self._dict["ISR"]
        self.ncuts = self._dict["ncuts"]
        self.channels = list(self.references.keys())
        self.observables = {
            k: Observable(k, v) for k, v in self._dict["observables"].items()
        }
        self._readme = (self.folder / "README.md").read_text()

        self.docs = "\n".join(self._readme.split("\n")[1:]).split("## Histograms")[0]

    def __repr__(self) -> str:
        return f"<Scenario {self.name} @ {self.energy}GeV, {self.ncuts} cuts, {len(self.observables)} histograms>"

    def validate(self) -> Generator[verr, None, None]:
        if not set(self.name).issubset(
            set(string.ascii_letters + string.digits + "-_+")
        ):
            yield verr(self, f"invalid char in {self.name}")

        for o in self.observables.values():
            yield from o.validate()


class Histograms:
    cols: list[str]
    dx: float
    _contributions: dict[str, nda]
    best: str
    nbins: int
    parent: "Channel"
    observable: Observable
    file: Path

    def __init__(self, parent: "Channel", d: dict[str, Any], obs: Observable):
        self._dict = d
        self.observable = obs.narrow(parent.name[0])
        self.parent = parent
        self.file = parent.parent.folder / d["file"]

        with open(self.file) as fp:
            header = fp.readline()
            self._data = np.loadtxt(fp, delimiter=",")

        self.cols = [i.strip().lstrip() for i in header.strip("# \n").split(",")]

        self.dx = self._data[0, 1] - self._data[0, 0]

        self._contributions = {i: self.extract(i) for i in d["contributions"]}
        self.best = self.parent.best

    def get(self, contr: Optional[str] = None) -> nda:
        if contr:
            return self._contributions[contr]
        else:
            return self._contributions[self.best]

    def extract(self, k: str) -> nda:
        if self.observable.nbins > self._data.shape[0]:
            self.nbins = 0
            self._contributions = {}
            raise verr(
                self,
                f"Invalid binning found {self._data.shape[0]}, expected {self.observable.nbins} for {self.file}@{k} ({self.observable.name})",
            )
        elif self.observable.nbins <= self._data.shape[0]:
            (n0,) = np.where(np.isclose(self._data[:, 0], self.observable.lower))
            if len(n0) == 0:
                self.nbins = 0
                self._contributions = {}
                raise verr(
                    self,
                    f"Invalid binning found, cannot find correct start for {self.file}@{k} ({self.observable.name})",
                )
            else:
                n0 = n0[0]
            self._data = self._data[n0 : n0 + self.observable.nbins]
            xtgt = self.observable.xvalues
            if not np.all(np.isclose(xtgt, self._data[:, :2])):
                self.nbins = 0
                self._contributions = {}
                raise verr(
                    self,
                    f"Invalid binning, can't narrow for {self.file}@{k} ({self.observable.name})",
                )

        ans = np.zeros((self.observable.nbins, 2))
        ans[:, 0] = self._data[:, self.cols.index(k)]
        try:
            ans[:, 1] = self._data[:, self.cols.index("d" + k)]
        except ValueError:
            pass

        return ans

    def x(self, opt: str = "centre") -> nda | tuple[nda, nda]:
        if opt == "centre":
            return self._data[:, 0] + self.dx / 2
        elif opt == "lower":
            return self._data[:, 0]
        elif opt == "upper":
            return self._data[:, 1]
        elif opt == "both":
            return self._data[:, 0], self._data[:, 1]
        else:
            raise KeyError(
                f"Did not understand {opt}. opt needs to be either centre, lower, or upper"
            )

    def __repr__(self) -> str:
        return f"<Histogram {self.observable.name} {self.observable.nbins} bins, {len(self._contributions)} cols>"

    def validate(self) -> Generator[verr, None, None]:
        if not self.cols[0] == "xl":
            yield verr(self, f"first column {self.cols[0]} is not xl")
        if not self.cols[1] == "xh":
            yield verr(self, f"second column {self.cols[1]} is not xh")
        nbins, ncols = self._data.shape
        if not nbins == self.observable.nbins:
            yield verr(self, f"found {nbins}, expected {self.observable.nbins} bins")
        if not ncols == len(self.cols):
            yield verr(self, f"found {ncols}, expected {len(self.cols)}")
        if not self.best in self._contributions:
            yield verr(self, f"invalid best {self.best}")


class Channel:
    channel2latex: dict[str, str] = {
        "ee": "e^+e^-\\to e^+e^-",
        "mm": "e^+e^-\\to \\mu^+\\mu^-",
        "pp": "e^+e^-\\to \\pi^+\\pi^-",
    }
    runtime: float
    xsec: dict[str, float]
    observables: dict[str, Histograms]
    best: str
    is_reference: bool
    parent: "MonteCarlo"
    name: str
    latex: str

    def __init__(self, parent: "MonteCarlo", name: str, d: dict[str, Any]):
        self.name = name
        self.latex = self.channel2latex[name]

        self.parent = parent
        self.runtime = d["runtime"]
        self.best = d["best"]
        self.xsec = {i["contribution"]: i["value"] for i in d["xsec"]}
        self.observables = {
            i["x"]: Histograms(self, i, parent.scenario.observables[i["x"]])
            for i in d["observables"]
        }

        self.is_reference = self.parent.code == self.parent.scenario.references[name]

    def __repr__(self) -> str:
        return f"<Channel {len(self.xsec)} contr., {len(self.observables)} hists>"

    def validate(self) -> Generator[verr, None, None]:
        exp = set(self.parent.scenario.observables.keys())
        have = set(self.observables.keys())
        if exp == have:
            pass
        elif exp.issubset(have):
            yield verr(
                self,
                f"too many observables {have-exp} for {self.parent.code} - {self.parent.scenario.name}",
            )
        elif exp.issuperset(have):
            yield verr(
                self,
                f"missing observables {exp-have} for {self.parent.code} - {self.parent.scenario.name}",
                0,
            )

        if self.runtime <= 0:
            yield verr(self, "runtime needs to be positive")

        for h in self.observables.values():
            yield from h.validate()


class MonteCarlo:
    code: str
    scenario: Scenario
    effects: str
    channels: dict[str, Channel]
    folder: Path

    def __init__(self, scenario: Scenario, folder: Path):
        self.scenario = scenario
        self.folder = folder

        self._yaml = strictyaml.load(
            (self.folder / "metadata.yaml").read_text(), run_schema
        )
        self._dict = self._yaml.data["run"]
        self.code = self._dict["code"]
        self.effects = self._dict["effects"]
        self.channels = {
            k: Channel(self, k, v) for k, v in self._dict["channels"].items()
        }

        if self._dict["scenario"] != self.scenario.name:
            raise verr(
                self,
                "Scenario name mismatch, expected {self.scenario.name}, found {self._dict['scenario']}",
            )

    def __repr__(self) -> str:
        return f"<MonteCarlo {self.code} for {self.scenario.name}>"

    def validate(self) -> Generator[verr, None, None]:
        if not set(self.code).issubset(
            set(string.ascii_letters + string.digits + "-_+@")
        ):
            yield verr(self, f"invalid char in {self.code}")

        if self.effects == "":
            yield verr(self, "effects need to be specified")

        for c in self.channels.values():
            yield from c.validate()


def load_all(scenario_name: Optional[str] = None) -> Generator[MonteCarlo, None, None]:
    errors = []
    for scenario in scenario_root.iterdir():
        if not (scenario / "metadata.yaml").exists():
            continue
        try:
            s = Scenario(scenario)
        except Exception as error:
            errors.append(verr(scenario, str(error), 1))
            continue
        if scenario_name:
            if s.name != scenario_name:
                continue

        errors += list(s.validate())
        for run in scenario.iterdir():
            if not run.is_dir():
                continue
            if not (run / "metadata.yaml").exists():
                continue
            try:
                self = MonteCarlo(s, run)
            except verr as error:
                errors.append(error)
                continue
            except Exception as error:
                errors.append(verr(run, str(error), 1))
                continue
            errors += list(self.validate())
            yield self

    nwarn = 0
    nerr = 0
    for err in errors:
        if err.severity == 1:
            nerr += 1
        elif err.severity == 0:
            nwarn += 1
        sys.stderr.write(err.__str__() + "\n")
    sys.stderr.write(f"Parsing complete, {nerr} errors and {nwarn} warnings\n")
    if nerr > 0:
        sys.exit(1)


if __name__ == "__main__":
    list(load_all())
