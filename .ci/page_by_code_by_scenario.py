from pathlib import Path
import jinja2
from typing import Optional
from build_page import *


folder = "by-code"
label = "by code"
description = "browse the results by code and compare the different contributions"


def main(results: list[MonteCarlo], root: Path) -> None:
    data_t = list[tuple[Histograms, Optional[str], str]]

    templateEnv = jinja2.Environment(
        loader=templates_loader, trim_blocks=True, lstrip_blocks=True
    )

    template = templateEnv.get_template(
        "index.html.jinja2",
    )
    codes: dict[str, set[str]] = {}

    for result in results:
        plots = []
        cmp_plots = []
        for obs in result.scenario.observables.values():
            data: data_t = []
            for ch_name, channel in result.channels.items():
                if obs.name in channel.observables:
                    o = channel.observables[obs.name]
                    data.append((o, None, f"${channel.channel2latex[ch_name]}$"))

                    cmp_data: data_t = [(o, None, "best")]
                    cmp_data += [
                        (o, i, str(i)) for i in o._contributions.keys() if i != o.best
                    ]
                    cmp = [(i + 1, 0, "ratio") for i in range(len(cmp_data) - 1)]

                    if len(cmp_data) == 1:
                        continue

                    cmp_plots.append(
                        (
                            f"Observable \\({obs.latex}\\) for \\({channel.channel2latex[ch_name]}\\)",
                            build_plot(cmp_data, *cmp),
                        )
                    )
                else:
                    print(
                        f"Warning: {result.code} is missing {obs.name} for channel {ch_name}"
                    )

            plots.append(
                (
                    f"Observable \\({obs.latex}\\)",
                    build_plot(data),
                )
            )

        path = root / result.code / result.scenario.name / "index.html"
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(
            build_plot_page(
                f"{result.code} for {result.scenario.name}",
                plots + cmp_plots,
                readme=results[0].scenario.docs,
            )
        )

        try:
            codes[result.code].add(result.scenario.name)
        except:
            codes[result.code] = set([result.scenario.name])

    for code, scenarios in codes.items():
        (root / code).mkdir(parents=True, exist_ok=True)
        path = root / code / "index.html"
        path.write_text(
            template.render(
                title=f"Scenarios of {code}",
                children=[(scenario, scenario) for scenario in scenarios],
                description=f'Learn more about {code} on <a href="{GITLAB_URL}/-/tree/root/codes/{code}">GitLab</a>',
            )
        )

    path = root / "index.html"
    path.write_text(
        template.render(
            title="List of codes", children=[(code, code) for code in codes.keys()]
        )
    )
