#ifndef HISTB_H
#define HISTB_H
#include "TLorentzVector.h"
#include "TH1D.h"

namespace histB_nm{

  namespace CutType{
    enum Index_t{
      Th=0x1,
      P=0x2,
      dPsi=0x4,
      Thg=0x8,
      Mxxg=0x10,
      Mxx=0x20
    };
  }

  int iproc;
  double ebeam;

  int selectmaskB(double ebeam, event_t& ev){
    
    int ighard  = -1;
    double emax=0;
    for(int i=2;i<ev.npart;i++){
      if ( ev.Tptot[i] > emax && ev.Tth[i] > 0.6 && ev.Tth[i] < 2.7 ){
        emax=ev.Tptot[i];
        ighard=i;
      }
    }

    TLorentzVector p4vec[3]={
      {0.,0.,ev.Tptot[0],hypot(ev.Tptot[0],mp[iproc])},
      {0.,0.,ev.Tptot[1],hypot(ev.Tptot[1],mp[iproc])}
    };
    if(ighard>=0) p4vec[2]=TLorentzVector(0,0,ev.Tptot[ighard],ev.Tptot[ighard]);

    for(int i=0;i<3;i++){
      if(i==2&&ighard<0) continue;
      p4vec[i].SetTheta(ev.Tth[i<2?i:ighard]);
      p4vec[i].SetPhi(ev.Tphi[i<2?i:ighard]);
    }

    double th[]={ev.Tth[0],ev.Tth[1]};
    double dangle=ighard>=0?(p4vec[0]+p4vec[1]).Angle(-p4vec[2].Vect()):10.;

    TLorentzVector psumg= (p4vec[0]+p4vec[1]+p4vec[2]);
    double mxxg=psumg.M();
    double mxx=(p4vec[0]+p4vec[1]).M();
  
    bool isth   = th[0] > 0.65 && th[0] < 2.75 && th[1] > 0.65 && th[1] < 2.75 ;
    bool ispcut = p4vec[0].P() > 1000 && p4vec[1].P() > 1000 ;
    bool isdpsi = fabs(dangle) < 0.3;
    bool isthg  = ighard>=0 && emax > 3000;
    bool ismxxg = mxxg > 8000;
    bool ismxx  = iproc==0? mxx>300: true; //for ee 
    
    bool iscut[]={!isth,!ispcut,!isdpsi,!isthg,!ismxxg,!ismxx};
    unsigned mask = 0;
    for(int i=0;i<6;i++) if(iscut[i]) mask|=(1<<i);
    
    return mask;
  }

  TH1D *hp[2],*hth[2];
  TH1D *hthg,*heg,*hthav,*hdphi,*hxi,*hmxx,*hmxxrw,*hmxxg,*hthgg;

  std::vector<TH1D*> gethists(){
    std::vector<TH1D*> hists={hp[0],hp[1],hth[0],hth[1],hthg,heg,hthav,hdphi,hxi,hmxx,hmxxrw,hmxxg,hthgg};
    return hists;
  }
  
  void histB_init(int iiproc,double ebeamin){
    iproc = iiproc;
    ebeam = ebeamin;

    TDirectory *curdir=gDirectory;
    curdir->mkdir(Form("%sB",proc[iproc]))->cd();
    
    const char* pplmn[2][2]={{"mn","-"},{"pl","+"}}; //electron along Z axis 

    for(int i=0;i<2;i++){
      const char* chl=pplmn[i][0];
      const char* ch=pplmn[i][1];
      const char* chn=pplmn[1-i][1];
      hp[i]=new TH1D(Form("hp%s",chl),Form("p%s;p^{%s};d#sigma/dp^{#pm}, nb/MeV",ch,ch),125,0.,5000.);
      hth[i] =new TH1D(Form("hth%s",chl),Form("th%s;#theta^{%s}, rad;d#sigma/d#theta, nb/rad",ch,ch),42+21,0.65-0.65,2.75+0.4);
    }
    heg     =new TH1D("heg","eg;E_{#gamma}, MeV;d#sigma/dE_{#gamma}, nb/MeV"                 ,250,0,5000);
    hthg    =new TH1D("hthg","thg;#theta_{#gamma}, rad;d#sigma/d#theta_{#gamma}, nb/rad"     ,42+21,0.6-0.6,2.7+0.45);
    hthav   =new TH1D("hthav","thav;#theta_{av}, rad;d#sigma/d#theta_{av}, nb/rad"           ,50,0.52,2.63);
    hdphi   =new TH1D("hdphi","dphi;#delta#phi/#pi;d#sigma/d(#delta#phi/#pi), nb/#pi"        ,100,0,1);
    hxi     =new TH1D("hxi","xi;#delta#theta/#pi;d#sigma/d(#delta#theta/#pi), nb/#pi"        ,100,0,1);
    hthgg   =new TH1D("hthgg","ang;#delta#psi, rad;d#sigma/d#delta#psi, nb/rad"              ,60,0.,0.3);
    
    hmxx    =new TH1D("hmxx","mxx;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV"                       ,160,0,6400);
    hmxxrw  =new TH1D("hmxxrw","mxxrw;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV"                   ,600,0,2400);
    hmxxg   =new TH1D("hmxxg","mxxg;M_{XX#gamma}, MeV;d#sigma/dM_{XX#gamma}, nb/MeV"         ,250,0,10000);

    curdir->cd();
  }

  bool histB_event(event_t& ev){

    //can be added weights re-tuning of different cross parts,
    //the knowledge of which is improved after event generation compared to pre generation estimation
    double weight = ev.weight;
    
    int ighard  = -1;
    double emax=0;
    for(int i=2;i<ev.npart;i++){
      if ( ev.Tptot[i] > emax && ev.Tth[i] > 0.6 && ev.Tth[i] < 2.7 ){
        emax=ev.Tptot[i];
        ighard=i;
      }
    }

    TLorentzVector p4vec[3]={
      {0.,0.,ev.Tptot[0],hypot(ev.Tptot[0],mp[iproc])},
      {0.,0.,ev.Tptot[1],hypot(ev.Tptot[1],mp[iproc])}
    };
    if(ighard>=0) p4vec[2]=TLorentzVector(0,0,ev.Tptot[ighard],ev.Tptot[ighard]);

    for(int i=0;i<3;i++){
      if(i==2&&ighard<0) continue;
      p4vec[i].SetTheta(ev.Tth[i<2?i:ighard]);
      p4vec[i].SetPhi(ev.Tphi[i<2?i:ighard]);
    }

    double th[]={ev.Tth[0],ev.Tth[1]};
    double P[]={ev.Tptot[0],ev.Tptot[1]};
    double thav=(th[0]-th[1]+TMath::Pi())/2;
    double dth=(th[0]+th[1]-TMath::Pi());
    double dphi=(ev.Tphi[0]-ev.Tphi[1]-TMath::Pi()+(ev.Tphi[0]<ev.Tphi[1]?2*TMath::Pi():0));
    double dangle=ighard>=0?(p4vec[0]+p4vec[1]).Angle(-p4vec[2].Vect()):10.;

    double mxx=(p4vec[0]+p4vec[1]).M();

    TLorentzVector psumg= (p4vec[0]+p4vec[1]+p4vec[2]);
    double mxxg=psumg.M();
    if(mxxg>=2*ebeam&&mxxg<2*ebeam+1e-3) mxxg-=1e-3;//rounding error at peaked sqrt(s)

    //    cout<<mxxg<<" "<<p4vec[0].E()<<" "<<p4vec[1].E()<<" "<<p4vec[2].E()<<" "<<endl;
    
    unsigned evmask=selectmaskB(ebeam,ev);

    for(int i=0;i<2;i++){
      if((evmask&~CutType::Th)==0&&th[1-i]>0.65&&th[1-i]<2.75) hth[i]->Fill(th[i],weight);
      if((evmask&~CutType::P)==0&&P[1-i]>1000) hp[i]->Fill(P[i],weight);
    }
    
    if(evmask==0)                   hthav->Fill(thav,weight);
    if(evmask==0)                   hxi->Fill(fabs(dth)/TMath::Pi(),weight);
    if(evmask==0)                   hdphi->Fill(fabs(dphi)/TMath::Pi(),weight);  
    if((evmask&~CutType::Mxx)==0){
      hmxx->Fill(mxx,weight);
      hmxxrw->Fill(mxx,weight);
    }
    if((evmask&~CutType::Mxxg)==0)  hmxxg->Fill(mxxg,weight);

    if(evmask==0&&ighard>=0){
      hthg->Fill(ev.Tth[ighard],weight);
      heg->Fill(ev.Tptot[ighard],weight);
      hthgg->Fill(dangle,weight);
    }
    
    return (evmask==0);
  }

  void histB_end(double ntot){
  }
}

#endif
