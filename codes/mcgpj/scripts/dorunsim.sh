#!/bin/bash

opt=$1
#outpwd=`pwd`
outpwd=/scratch/ignatov/datahistmcgpj
rootmacro=/user/ignatov/cmd/scripts/loophist_run.C
MCGPJDIR=/user/ignatov/cmd/radcor
rungen=${MCGPJDIR}/hist

export VACUUM_POL_DIR=${MCGPJDIR}/data/
export INTEG_DIR=${MCGPJDIR}/data/

proc=""
[[ $opt =~ "mm" ]] && proc="mm";
[[ $opt =~ "ee" ]] && proc="ee";
[[ $opt =~ "pp" ]] && proc="pp";

scena=""
[[ $opt =~ "cmd" ]] && scena="cmd";
[[ $opt =~ "kloe" ]] && scena="kloe";
[[ $opt =~ "bes" ]] && scena="bes";
[[ $opt =~ "B" ]] && scena="B";


[[ $opt =~ "odddisp" ]]  && INTEG_FNAMEODDFF=${INTEG_DIR}/delta_sqrts_z_dispersive.dat
[[ $opt =~ "oddgvdm" ]]  && INTEG_FNAMEODDFF=${INTEG_DIR}/integoddFFdoublesumBWfit_logb_paper_omega.dat

optnovp=
[[ $opt =~ "novp" ]]  && optnovp="-nvp"
optnoffodd=
[[ $opt =~ "noffodd" ]]  && optnoffodd="-noffodd"

seed=`od -A n -N 4 -t d4 /dev/urandom | sed 's/[- ]*//g'`
#ffver=0  
optffver=
#[ $ffver -ne 0 ] && optffver="-ffver $ffver"

echo "scenario: $scena proc: ${proc}"

( [[ $scena == "" ]] || [[ $proc == "" ]] ) && echo "no Scena or simulation process is defined in run option" && exit

runflags=""

optnlo=""
if [[ ${opt,,} =~ "nloisrla" ]] ; then
    [ $proc == "ee" ] && optnlo="-include 17 -nt0 0. -nja -ti 0.4"
    [ $proc == "mm" ] && optnlo="-include 4 "
    [ $proc == "pp" ] && optnlo="-include 4 "

    [ $scena == "bes" ] && optnlo=${optnlo}" -de 20"
    [ $scena == "B" ] && optnlo=${optnlo}" -de 1000"
fi
#mmnlo="-include 4 5 -nt0 0. "
#ppnlo="-include 4 5 -nt0 0. "
#eenlo="-include 16 17 18 -nt0 0. "



if [[ $scena == "cmd" ]] ; then
   [ $proc == "ee" ] && runflags="-p 0 -e 350 -rer 0.0001 -dt 0.25 -dp 0.15 -tc 1. -cm 0 -em 0 -am 0 -ns 10000000"
   [ $proc == "mm" ] && runflags="-p 1 -e 350 -rer 0.0001 -dt 0.25 -dp 0.15 -tc 1. -cm 0 -em 0 -am 0 -ns 10000000"
   [ $proc == "pp" ] && runflags="-p 2 -ffver 10 -e 350 -rer 0.0001 -dt 0.25 -dp 0.15 -tc 1. -cm 0 -em 0 -am 0 -ns 10000000"
fi

if [[ $scena == "kloe" ]] ; then
    [ $proc == "ee" ] && runflags="-p 0 -e 510 -rer 0.0002 -dt 10.25 -dp 10.15 -tc 0.872 -td 0.872 -cm 50 -em 0 -am 0 -minv 4 -ns 10000000"
    [ $proc == "mm" ] && runflags="-p 1 -e 510 -rer 0.0001 -dt 10.25 -dp 10.15 -tc 0.872 -td 0.872 -cm 0 -em 0 -am 0 -ns 10000000"
    [ $proc == "pp" ] && runflags="-p 2 -ffver 10 -e 510 -rer 0.0001 -dt 10.25 -dp 10.15 -tc 0.872 -td 0.872 -cm 0 -em 0 -am 0 -ns 10000000"
fi

if [[ $scena == "bes" ]] ; then
    [ $proc == "ee" ] && runflags="-p 0 -e 2000 -rer 0.0002 -dt 10.25 -dp 10.15 -tc 0.3763 -td 0.3763 -cm 300 -em 0 -am 0 -minv 20 -ns 1000000"
    [ $proc == "mm" ] && runflags="-p 1 -e 2000 -rer 0.0002 -dt 10.25 -dp 10.15 -tc 0.3763 -td 0.3763 -cm 300 -em 0 -am 0 -ns 1000000"
    [ $proc == "pp" ] && runflags="-p 2 -ffver 10 -e 2000 -rer 0.001 -dt 10.25 -dp 10.15 -tc 0.3763 -td 0.3763 -cm 300 -em 0 -am 0 -aph -ns 1000000"
fi

if [[ $scena == "B" ]] ; then
    [ $proc == "ee" ] && runflags="-p 0 -e 5000 -rer 0.0005 -dt 10.25 -dp 10.15 -tc 0.39 -td 0.39 -cm 0 -em 500 -am 0 -minv 10 -ns 1000000"
    [ $proc == "mm" ] && runflags="-p 1 -e 5000 -rer 0.0005 -dt 10.25 -dp 10.15 -tc 0.39 -td 0.39 -cm 0 -em 500 -am 0 -ns 1000000"
    [ $proc == "pp" ] && runflags="-p 2 -ffver 10 -e 5000 -rer 0.01 -dt 10.25 -dp 10.15 -tc 0.39 -td 0.39 -cm 0 -em 500 -am 0 -aph -ns 1000000"
fi

runflags="-v -s ${seed} -wght ${optnlo} ${runflags} ${optffver} ${optnovp} ${optnoffodd} -tup -fname tuple${opt}.root "


#do simulation in temporary dir (TMPDIR can depend on batch environment)
echo "slurm tmp: "$SLURM_TMPDIR" ."
[ "$SLURM_TMPDIR" != "" ] && cd $SLURM_TMPDIR
pwdpath=`pwd`
echo "current dir: "${pwdpath}
TMP=`mktemp -d ${pwdpath}/mcgpj.XXXXXXXXXX`
echo "Temp dir name: " $TMP
{ [[ -n "$TMP" ]] && mkdir -p "$TMP"; } || { echo "ERROR: unable to create temporary directory!" 1>&2; exit 1; }
trap "[[ -n \"$TMP\" ]] && { cd ; rm -rf \"$TMP\"; }" 0
cd $TMP

#run everything

echo "Working directory "`pwd`
export BASH_XTRACEFD=1
set -o xtrace
(time ${rungen} ${runflags} ) &> log.tuple${opt}
root -b -q -l $rootmacro'("tuple'${opt}'.root","'${opt}'")' &> log.hist${opt}

#mv log.tuple${opt} log.hist${opt} histout${opt}.root tuple${opt}.root ${outpwd}/
mv log.tuple${opt} log.hist${opt} histout${opt}.root ${outpwd}/

