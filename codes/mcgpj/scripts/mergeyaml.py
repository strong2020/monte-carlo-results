#!/usr/bin/python
#ls data_vp/metadata_* |sed 's|.*/||g' | xargs -I aaa ./scripts/mergeyaml.py data_novp/aaa data_vp/aaa data/aaa


import ruamel.yaml
import argparse

def first(s):
    if isinstance(s, dict):
        return next(iter(s.items()))
    else:
        return s
    
def merge(object_one, object_two):
    if isinstance(object_one, dict):

        # Add missing keys to object_one
        object_one = {**object_one, **{k: v for k, v in object_two.items() if k not in object_one}}
        # If
        for key in object_one:
            #print(key,"-",object_one[key], object_two[key])
            if key == "runtime":
                object_one[key] = object_one[key]+object_two[key]
            elif (tmp := merge(object_one[key], object_two[key])) is not None:
                object_one[key] = tmp

        #print("dictmerge",object_one)
        return object_one
    elif isinstance(object_one, list):
        for index_two in range(len(object_two)):
            found = False
            key2=first(object_two[index_two])
            #print(index_two,object_two)
            for index in range(len(object_one)):
                key1=first(object_one[index])
                if key1==key2:
                    object_one[index] = merge(object_one[index], object_two[index_two])
                    found = True

            # If not found for merge, just append
            if not found:
                object_one.append(object_two[index_two])

        #print("listmerge",object_one)
        return object_one
    else:
        # If both objects dont' match, return None which signals to the previous stack to merge one level up
        if object_one == object_two:
            return object_one
        return object_one + object_two
    
    return object_one

parser = argparse.ArgumentParser(description='Merge two yaml files')
parser.add_argument('file1')
parser.add_argument('file2')
parser.add_argument('fileout')
args = parser.parse_args()

print("merge ",args.file1,args.file2,"to",args.fileout)

yaml = ruamel.yaml.YAML()
#Load the yaml files
d1=yaml.load(open(args.file1))
d2=yaml.load(open(args.file2))

data=merge(d1,d2)
yaml.dump(data,open(args.fileout, 'w'))
