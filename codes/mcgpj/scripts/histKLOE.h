#ifndef HISTKLOE_H
#define HISTKLOE_H
#include "TLorentzVector.h"
#include "TH1D.h"

namespace histKLOE_nm{

  namespace CutType{
    enum Index_t{
      Th=0x1,
      P=0x2,
      Thm=0x4,
      Thg=0x8,
      Mxx=0x10
    };
  }

  int iproc;
  double ebeam;
 
  int selectmaskKLOE_SA(double ebeam, event_t& ev){
    
    double th[]={ev.Tth[0]*TMath::RadToDeg(),ev.Tth[1]*TMath::RadToDeg()};
    double pz[]={ev.Tptot[0]*cos(ev.Tth[0]),ev.Tptot[1]*cos(ev.Tth[1])};
    double ptr[]={ev.Tptot[0]*sin(ev.Tth[0]),ev.Tptot[1]*sin(ev.Tth[1])};
    
    TLorentzVector p4vec[2]={
      {0.,0.,ev.Tptot[0],hypot(ev.Tptot[0],mp[iproc])},
      {0.,0.,ev.Tptot[1],hypot(ev.Tptot[1],mp[iproc])}
    };
    for(int i=0;i<2;i++){
      p4vec[i].SetTheta(ev.Tth[i]);
      p4vec[i].SetPhi(ev.Tphi[i]);
    }

    TLorentzVector psum= (p4vec[0]+p4vec[1]);
    double mxx2= psum.M2();
    double thm = (TMath::Pi() - psum.Theta())*TMath::RadToDeg();
    
    bool isth   = th[0] > 50. && th[0] < 130. && th[1] > 50. && th[1] < 130. ;
    bool ispcut = (fabs(pz[0]) > 90 || fabs(ptr[0]) > 160 ) && (fabs(pz[1]) > 90 || fabs(ptr[1]) > 160 );
    bool isthm  = thm < 15 || thm > 165 ;
    bool ismxx  = mxx2 > 0.35e6 && mxx2 < 0.95e6 ;//&& psum.P()>1e-4; 
    
    bool iscut[]={!isth,!ispcut,!isthm,false,!ismxx};
    unsigned mask = 0;
    for(int i=0;i<5;i++) if(iscut[i]) mask|=(1<<i);
    
    return mask;
  }

  int selectmaskKLOE_LA(double ebeam, event_t& ev){
    
    double th[]={ev.Tth[0]*TMath::RadToDeg(),ev.Tth[1]*TMath::RadToDeg()};
    double pz[]={ev.Tptot[0]*cos(ev.Tth[0]),ev.Tptot[1]*cos(ev.Tth[1])};
    double ptr[]={ev.Tptot[0]*sin(ev.Tth[0]),ev.Tptot[1]*sin(ev.Tth[1])};
    
    TLorentzVector p4vec[2]={
      {0.,0.,ev.Tptot[0],hypot(ev.Tptot[0],mp[iproc])},
      {0.,0.,ev.Tptot[1],hypot(ev.Tptot[1],mp[iproc])}
    };
    for(int i=0;i<2;i++){
      p4vec[i].SetTheta(ev.Tth[i]);
      p4vec[i].SetPhi(ev.Tphi[i]);
    }

    TLorentzVector psum= (p4vec[0]+p4vec[1]);
    double mxx2= psum.M2();
    double thm = (TMath::Pi() - psum.Theta())*TMath::RadToDeg();
    
    bool isth   = th[0] > 50. && th[0] < 130. && th[1] > 50. && th[1] < 130. ;
    bool ispcut = (fabs(pz[0]) > 90 || fabs(ptr[0]) > 160 ) && (fabs(pz[1]) > 90 || fabs(ptr[1]) > 160 );
    bool isthg  = false;
    for(int i=2;i<ev.npart;i++){
      if ( ev.Tptot[i] > 20 && ev.Tth[i]*TMath::RadToDeg() > 50 && ev.Tth[i]*TMath::RadToDeg() < 130 ) isthg=true;
    }
    bool ismxx  = mxx2 > 0.1e6 && mxx2 < 0.85e6 ;//&& psum.P()>1e-4; 
   
    bool iscut[]={!isth,!ispcut,false,!isthg,!ismxx};
    unsigned mask = 0;
    for(int i=0;i<5;i++) if(iscut[i]) mask|=(1<<i);
    
    return mask;
  }

  TH1D *hth[2][2],*hp[2][2],*hpz[2][2],*hptr[2][2];
  TH1D *hmxx[2],*hmtrk[2],*hxi[2],*hdphi[2],*hthav[2],*hthtg[2],*hthtgl[2],*hthtgs[2];
  TH1D *hmxxcut[2],*hmxxcutmu[2],*hmxxcutpi[2],*hmtrkcut[2];
  TH1D *hthg[2],*heg[2],*hmxxg[2],*hthgh[2],*hegh[2],*hmxxgh[2];
  
  std::vector<TH1D*> gethists(){
    std::vector<TH1D*> hists={
      hth[0][0],hth[0][1],hp[0][0],hp[0][1],hpz[0][0],hpz[0][1],hptr[0][0],hptr[0][1],
      hmxx[0],hmtrk[0],hxi[0],hdphi[0],hthav[0],hthtg[0],hthtgl[0],hthtgs[0],
      hmxxcut[0],hmxxcutmu[0],hmxxcutpi[0],hmtrkcut[0],
      hthg[0],heg[0],hmxxg[0],hthgh[0],hegh[0],hmxxgh[0],
      hth[1][0],hth[1][1],hp[1][0],hp[1][1],hpz[1][0],hpz[1][1],hptr[1][0],hptr[1][1],
      hmxx[1],hmtrk[1],hxi[1],hdphi[1],hthav[1],hthtg[1],hthtgl[1],hthtgs[1],
      hmxxcut[1],hmxxcutmu[1],hmxxcutpi[1],hmtrkcut[1],
      hthg[1],heg[1],hmxxg[1],hthgh[1],hegh[1],hmxxgh[1]
    };
    return hists;
  }
  
  void histKLOE_init(int iiproc,double ebeamin){
    iproc = iiproc;
    ebeam = ebeamin;

    TDirectory *curdir=gDirectory;
    
    const char* pplmn[2][2]={{"mn","-"},{"pl","+"}}; //electron along Z axis 

    for(int isl=0;isl<2;isl++){
      curdir->mkdir(Form(isl==0?"%skloeSA":"%skloeLA",proc[iproc]))->cd();
      
      for(int i=0;i<2;i++){
        const char* chl=pplmn[i][0];
        const char* ch=pplmn[i][1];
        const char* chn=pplmn[1-i][1];
        hth[isl][i] =new TH1D(Form("hth%s",chl),Form("th%s;#theta^{%s}, deg;d#sigma/d#theta, nb/deg",ch,ch),80,50,130);
        hp[isl][i]  =new TH1D(Form("hp%s",chl) ,Form("p%s;p^{%s}, MeV;d#sigma/dp, nb/MeV",ch,ch)           ,75,140.,515.);   //60,215.,515.
        hpz[isl][i] =new TH1D(Form("hpz%s",chl),Form("pz%s;|p_{z}^{%s}|, MeV;d#sigma/d|p_{z}|, nb/MeV",ch,ch)  ,75,0,375); //150,-375.,375.); 
        hptr[isl][i]=new TH1D(Form("hpp%s",chl),Form("pp%s;p_{tr}^{%s}, MeV;d#sigma/dp_{tr}, nb/MeV",ch,ch),isl==0?60:75,isl==0?200.:106,isl==0?500.:518.5);
      }
      int nmxx=(isl==0?50+55:60+20);
      double rmxx[]={isl==0?591.-7.68*49:316-10.1*10,
                     isl==0?975.+7.68*6 :922+10.1*10}; //SA 7.68/bin ,LA 10.1/bin
      hmxx[isl]    =new TH1D("hmxx",    "mxx;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV"                         ,nmxx,rmxx[0],rmxx[1]); // 383/50
      hmtrk[isl]   =new TH1D("hmtrk",   "mtrk;M_{trk}, MeV;d#sigma/dM_{trk}, nb/MeV"                      ,50,0.,500.);
      hmxxcut[isl] =new TH1D("hmxxc",   "mxxc with M_{trk} cut;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV"       ,nmxx,rmxx[0],rmxx[1]);
      hmxxcutmu[isl]=new TH1D("hmxxcmu","mxxcmu with #mu-M_{trk} cut;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV" ,nmxx,rmxx[0],rmxx[1]);
      hmxxcutpi[isl]=new TH1D("hmxxcpi","mxxcpi with #pi-M_{trk} cut;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV" ,nmxx,rmxx[0],rmxx[1]);
      hmtrkcut[isl]=new TH1D("hmtrkc",  "mtrkc with M_{XX} cut;M_{trk}, MeV;d#sigma/dM_{trk}, nb/MeV"     ,50,0.,500.);
      hxi[isl]     =new TH1D("hxi","xi;#delta#theta, deg;d#sigma/d#delta#theta, nb/deg"                   ,isl==0?80:40,0.,80);//60,0,60 ; 40,0,80
      hdphi[isl]   =new TH1D("hdphi","dphi;#delta#phi, deg;d#sigma/d#delta#phi, nb/deg"                   ,isl==0?360:60,0,180);//180*6,0.,180.);//50,0,25 ; 60,0,180
      hthav[isl]   =new TH1D("hthav","thav;#theta_{av}, deg;d#sigma/d#theta_{av}, nb/deg"                 ,80,50,130);
      hthtg[isl]   =new TH1D("hthtg","thtg;#theta_{miss}, deg;d#sigma/d#theta_{miss}, nb/deg"             ,60*12,0,180);
      hthtgl[isl]  =new TH1D("hthtgl","thtgl;#theta_{miss}, deg;d#sigma/d#theta_{miss}, nb/deg"           ,60,165,180);
      hthtgs[isl]  =new TH1D("hthtgs","thtgs;#theta_{miss}, deg;d#sigma/d#theta_{miss}, nb/deg"           ,60,0,15);
      
      hthg[isl]    =new TH1D("hthg","thg at LA;#theta_{#gamma}, deg;d#sigma/d#theta_{#gamma}, nb/deg"     ,180,0,180);
      heg[isl]     =new TH1D("heg","eg at LA;E_{#gamma}, MeV;d#sigma/dE_{#gamma}, nb/MeV"                 ,102,0,510);
      hmxxg[isl]   =new TH1D("hmxxg","mxxg at LA;M_{XX#gamma}, MeV;d#sigma/dM_{XX#gamma}, nb/MeV"         ,102,0,1020); //75,270,1020
      hthgh[isl]   =new TH1D("hthgh","thgh hardest;#theta_{#gamma}, deg;d#sigma/d#theta_{#gamma}, nb/deg" ,180,0,180);
      hegh[isl]    =new TH1D("hegh","egh hardset;E_{#gamma}, MeV;d#sigma/dE_{#gamma}, nb/MeV"             ,102,0,510);
      hmxxgh[isl]  =new TH1D("hmxxgh","mxxgh hardest;M_{XX#gamma}, MeV;d#sigma/dM_{XX#gamma}, nb/MeV"     ,102,0,1020); //75,270,1020
    }

    curdir->cd();
  }

  std::pair<bool,bool> histKLOE_event(event_t& ev){

    //can be added weights re-tuning of different cross parts,
    //the knowledge of which is improved after event generation compared to pre generation estimation
    double weight = ev.weight;
    
    double th[]={ev.Tth[0]*TMath::RadToDeg(),ev.Tth[1]*TMath::RadToDeg()};
    double pz[]={ev.Tptot[0]*cos(ev.Tth[0]),ev.Tptot[1]*cos(ev.Tth[1])};
    double ptr[]={ev.Tptot[0]*sin(ev.Tth[0]),ev.Tptot[1]*sin(ev.Tth[1])};

    std::vector<TLorentzVector> p4vec(ev.npart);
    for(int i=0;i<ev.npart;i++){
      p4vec[i]=TLorentzVector(0.,0.,ev.Tptot[i],hypot(ev.Tptot[i],i<2?mp[iproc]:0)),
      p4vec[i].SetTheta(ev.Tth[i]);
      p4vec[i].SetPhi(ev.Tphi[i]);
    }

    int iglhard=-1;
    double emaxg=0.;
    for(int i=2;i<ev.npart;i++){
      if ( ev.Tptot[i] > 20 && ev.Tth[i]*TMath::RadToDeg() > 50 && ev.Tth[i]*TMath::RadToDeg() < 130 &&
           ev.Tptot[i] > emaxg ) {emaxg=ev.Tptot[i]; iglhard=i;}
    }
    int ighard=iglhard;
    for(int i=2;i<ev.npart;i++){
      if ( ev.Tptot[i] > emaxg ) {emaxg=ev.Tptot[i]; ighard=i;}
    }    
    
    TLorentzVector psum= (p4vec[0]+p4vec[1]);
    double mxx=psum.M();

    TLorentzVector psumg= (p4vec[0]+p4vec[1]);
    if(iglhard>=0) psumg+=p4vec[iglhard];
    double mxxg=psumg.M();
    if(mxxg>=2*ebeam&&mxxg<2*ebeam+1e-4) mxxg-=1e-4;//rounding error at peaked sqrt(s)

    TLorentzVector psumgh= (p4vec[0]+p4vec[1]);
    if(ighard>=0) psumgh+=p4vec[ighard];
    double mxxgh=psumgh.M();
    if(mxxgh>=2*ebeam&&mxxgh<2*ebeam+1e-4) mxxgh-=1e-4;//rounding error at peaked sqrt(s)

    double P[]={ev.Tptot[0],ev.Tptot[1],psum.P()};
 
    double dm2   = pow(2*ebeam - P[2],2)-P[0]*P[0]-P[1]*P[1];
    double mtrk2 = (dm2*dm2-4*P[0]*P[0]*P[1]*P[1])/(P[0]*P[0]+P[1]*P[1]+dm2)/4;
    double mtrk  = TMath::Sign(sqrt(fabs(mtrk2)),mtrk2);
    
    
    double thm = (TMath::Pi() - psum.Theta())*TMath::RadToDeg();
    double thav=(ev.Tth[0]+TMath::Pi()-ev.Tth[1])/2*TMath::RadToDeg();
    double dth=(ev.Tth[0]+ev.Tth[1]-TMath::Pi())*TMath::RadToDeg();
    double dphi=(ev.Tphi[0]-ev.Tphi[1]-TMath::Pi()+(ev.Tphi[0]<ev.Tphi[1]?2*TMath::Pi():0))*TMath::RadToDeg();
   
    bool isMtrkcut[3]={mtrk>130&&mtrk<220,mtrk>80&&mtrk<115,mtrk>130&&mtrk<220};
    bool isRhoMxx    = mxx*mxx>0.5e6&&mxx*mxx<0.7e6;

    unsigned evmaskSA=selectmaskKLOE_SA(ebeam,ev);
    unsigned evmaskLA=selectmaskKLOE_LA(ebeam,ev);
    
    for(int isl=0;isl<2;isl++){
   
      unsigned evmask=isl==0?evmaskSA:evmaskLA;
    
      if((evmask&~CutType::Mxx)==0)                   hmxx[isl]->Fill(mxx,weight);
      if((evmask&~CutType::Mxx)==0&&isMtrkcut[iproc]) hmxxcut[isl]->Fill(mxx,weight);
      if((evmask&~CutType::Mxx)==0&&isMtrkcut[1])     hmxxcutmu[isl]->Fill(mxx,weight);
      if((evmask&~CutType::Mxx)==0&&isMtrkcut[2])     hmxxcutpi[isl]->Fill(mxx,weight);
      if(evmask==0)                                   hmtrk[isl]->Fill(mtrk,weight);
      if(evmask==0&&isRhoMxx)                         hmtrkcut[isl]->Fill(mtrk,weight);
    
    
      if(evmask==0) {
        hxi[isl]->Fill(fabs(dth),weight);
        hdphi[isl]->Fill(fabs(dphi),weight);  
        hthav[isl]->Fill(thav,weight);
      }
      if((evmask&~CutType::Thm)==0) {
        hthtg[isl]->Fill(thm,weight);
        hthtgs[isl]->Fill(thm,weight);
        hthtgl[isl]->Fill(thm,weight);
      }
      
      if(evmask==0&&iglhard>=0){
        hthg[isl]->Fill(p4vec[iglhard].Theta()*TMath::RadToDeg(),weight);
        heg[isl]->Fill(p4vec[iglhard].E(),weight);
        hmxxg[isl]->Fill(mxxg,weight);//float rounding error in bin around sqrt(s)
      }
      if(evmask==0&&ighard>=0){
        hthgh[isl]->Fill(p4vec[ighard].Theta()*TMath::RadToDeg(),weight);
        hegh[isl]->Fill(p4vec[ighard].E(),weight);
        hmxxgh[isl]->Fill(mxxgh,weight);//float rounding error in bin around sqrt(s)
      }
      
      for(int i=0;i<2;i++){
        if((evmask&~CutType::Th)==0&&th[1-i]>50&&th[1-i]<130) hth[isl][i]->Fill(th[i],weight);
        if(evmask==0) {
          hp[isl][i]->Fill(P[i],weight);
          hpz[isl][i]->Fill(fabs(pz[i]),weight);
          hptr[isl][i]->Fill(ptr[i],weight);
        }
      }
    }

    return {evmaskSA==0,evmaskLA==0};
  }

  void histKLOE_end(double ntot){
  }
}

#endif
