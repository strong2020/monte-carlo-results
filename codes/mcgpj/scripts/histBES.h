#ifndef HISTBES_H
#define HISTBES_H
#include "TLorentzVector.h"
#include "TH1D.h"

namespace histBES_nm{

  namespace CutType{
    enum Index_t{
      Th=0x1,
      P=0x2,
      Thg=0x4,
      Eg=0x8,
      Nhard=0x10
    };
  }

  int iproc;
  double ebeam;

  int selectmaskBES(double ebeam, event_t& ev){
    
    int ighard  = -1;
    double emax=0;
    int nhard = 0;
    for(int i=2;i<ev.npart;i++){
      double cthg = fabs(cos(ev.Tth[i]));
      if ( cthg < 0.8 || (cthg>0.86&&cthg<0.92) ){
        if( ev.Tptot[i] > 400 ) nhard++;
        if( ev.Tptot[i] > emax ){
          emax=ev.Tptot[i];
          ighard=i;
        }
      }
    }

    double cth[]={cos(ev.Tth[0]),cos(ev.Tth[1])};
    double ptr[]={ev.Tptot[0]*sin(ev.Tth[0]),ev.Tptot[1]*sin(ev.Tth[1])};
    double cthg = ighard>=0?fabs(cos(ev.Tth[ighard])):1.;
    double eg   = ighard>=0?ev.Tptot[ighard]:0.;
    
    bool isth   = fabs(cth[0]) < 0.93 && fabs(cth[1]) < 0.93;
    bool ispcut = ptr[0] > 300 && ptr[1] > 300;
    bool isthg  = ighard>=0;
    bool iseg   = ighard>=0 && eg > 400;
    bool isnhard = nhard==1;
    
    bool iscut[]={!isth,!ispcut,!isthg,!iseg,!isnhard};
    unsigned mask = 0;
    for(int i=0;i<5;i++) if(iscut[i]) mask|=(1<<i);
    
    return mask;
  }

  TH1D *hp[2],*hptr[2],*hcth[2];
  TH1D *hcthg,*heg,*hthav,*hdphi,*hxi,*hmxx,*hmxxrw,*hmxxg;

  std::vector<TH1D*> gethists(){
    std::vector<TH1D*> hists={
      hp[0],hp[1],hptr[0],hptr[1],hcth[0],hcth[1],
      hcthg,heg,hthav,hdphi,hxi,hmxx,hmxxrw,hmxxg
    };
    return hists;
  }
  
  void histBES_init(int iiproc,double ebeamin){
    iproc = iiproc;
    ebeam = ebeamin;

    TDirectory *curdir=gDirectory;
    curdir->mkdir(Form("%sbes",proc[iproc]))->cd();
    
    const char* pplmn[2][2]={{"mn","-"},{"pl","+"}};//electron along Z axis 

    for(int i=0;i<2;i++){
      const char* chl=pplmn[i][0];
      const char* ch=pplmn[i][1];
      const char* chn=pplmn[1-i][1];
      hp[i]  =new TH1D(Form("hp%s",chl),Form("p%s;p^{%s};d#sigma/dp, nb/MeV",ch,ch)                    ,100,0.,2000.);
      hptr[i]=new TH1D(Form("hpp%s",chl),Form("p%s;p^{%s}_{tr};d#sigma/dp_{tr}, nb/MeV",ch,ch)         ,100,0.,2000.);
      hcth[i]=new TH1D(Form("hcth%s",chl),Form("cth%s;|cos(#theta^{%s})|;d#sigma/d|cos(#theta)|, nb",ch,ch),34,0.,1.02);
    }
    heg     =new TH1D("heg","eg;E_{#gamma}, MeV;d#sigma/dE_{#gamma}, nb/MeV"                 ,80,0.,2000);
    hdphi   =new TH1D("hdphi","dphi;#delta#phi/#pi;d#sigma/d(#delta#phi/#pi), nb/(rad/#pi)"  ,50,0.,1.);
    hxi     =new TH1D("hxi","xi;#delta#theta, rad;d#sigma/d#delta#theta, nb/rad"             ,60,0.,2.4);
    hmxx    =new TH1D("hmxx","mxx;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV"                       ,102,0,4080);
    hmxxrw  =new TH1D("hmxxrw","mxxrw;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV"                   ,600,0,2400);
    hmxxg   =new TH1D("hmxxg","mxxg;M_{XX#gamma}, MeV;d#sigma/dM_{XX#gamma}, nb/MeV"         ,100,0,4000);
    hcthg   =new TH1D("hcthg","cthg;|cos(#theta_{#gamma})|;d#sigma/d|cos(#theta_{#gamma})|, nb" ,50,0,1);
    hthav   =new TH1D("hthav","thav;#theta_{av}, rad;d#sigma/d#theta_{av}, nb/rad"           ,50,0.3,2.8);

    curdir->cd();
  }

  bool histBES_event(event_t& ev){

    //can be added weights re-tuning of different cross parts,
    //the knowledge of which is improved after event generation compared to pre generation estimation
    double weight = ev.weight;
    
    int ighard  = -1;
    double emax=0;
    for(int i=2;i<ev.npart;i++){
      double cthg = fabs(cos(ev.Tth[i]));
      if ( cthg < 0.8 || (cthg>0.86&&cthg<0.92) ){
        if( ev.Tptot[i] > emax ){
          emax=ev.Tptot[i];
          ighard=i;
        }
      }
    }

    TLorentzVector p4vec[3]={
      {0.,0.,ev.Tptot[0],hypot(ev.Tptot[0],mp[iproc])},
      {0.,0.,ev.Tptot[1],hypot(ev.Tptot[1],mp[iproc])}
    };
    if(ighard>=0) p4vec[2]=TLorentzVector(0,0,ev.Tptot[ighard],ev.Tptot[ighard]);

    for(int i=0;i<3;i++){
      if(i==2&&ighard<0) continue;
      p4vec[i].SetTheta(ev.Tth[i<2?i:ighard]);
      p4vec[i].SetPhi(ev.Tphi[i<2?i:ighard]);
    }

    double cth[]={cos(ev.Tth[0]),cos(ev.Tth[1])};
    double P[]={ev.Tptot[0],ev.Tptot[1]};
    double ptr[]={ev.Tptot[0]*sin(ev.Tth[0]),ev.Tptot[1]*sin(ev.Tth[1])};
    double cthg = ighard>=0?cos(ev.Tth[ighard]):1.;
    double eg   = ighard>=0?ev.Tptot[ighard]:0.;
    
    double thav=(ev.Tth[0]-ev.Tth[1]+TMath::Pi())/2;
    double dth =(ev.Tth[0]+ev.Tth[1]-TMath::Pi());
    double dphi=(ev.Tphi[0]-ev.Tphi[1]-TMath::Pi()+(ev.Tphi[0]<ev.Tphi[1]?2*TMath::Pi():0));

    double mxx=(p4vec[0]+p4vec[1]).M();

    TLorentzVector psumg= (p4vec[0]+p4vec[1]+p4vec[2]);
    double mxxg=psumg.M();

    unsigned evmask=selectmaskBES(ebeam,ev);

    for(int i=0;i<2;i++){
      if((evmask&~CutType::Th)==0&&fabs(cth[1-i])<0.93) hcth[i]->Fill(fabs(cth[i]),weight);
      if((evmask&~CutType::P)==0&&ptr[1-i]>300) hptr[i]->Fill(ptr[i],weight);
      if(evmask==0) hp[i]->Fill(P[i],weight);
    }
    
    if(evmask==0) {
      hthav->Fill(thav,weight);
      hxi->Fill(fabs(dth),weight);
      hdphi->Fill(fabs(dphi/TMath::Pi()),weight);  
      hmxx->Fill(mxx,weight);
      hmxxrw->Fill(mxx,weight);
      hmxxg->Fill(mxxg,weight);
    }
    
    if((evmask&~CutType::Thg)==0&&ighard>=0) hcthg->Fill(fabs(cthg),weight);
    if((evmask&~CutType::Eg)==0&&ighard>=0) heg->Fill(eg,weight);
    
    return (evmask==0);
  }

  void histBES_end(double ntot){
  }
}

#endif
