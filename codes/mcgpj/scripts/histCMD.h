#ifndef HISTCMD_H
#define HISTCMD_H
#include "TLorentzVector.h"
#include "TH1D.h"

namespace histCMD_nm{

  namespace CutType{
    enum Index_t{
      Th=0x1,
      P=0x2,
      dTh=0x4,
      dPhi=0x8
    };
  }

  int selectmaskCMD(double ebeam, event_t& ev){
    
    double thav=(ev.Tth[0]+TMath::Pi()-ev.Tth[1])/2;
    double dth=ev.Tth[0]+ev.Tth[1]-TMath::Pi();
    double dphi=ev.Tphi[0]-ev.Tphi[1]-TMath::Pi()+(ev.Tphi[0]<ev.Tphi[1]?2*TMath::Pi():0);
    
    bool isthav = thav > 1 && thav < TMath::Pi()-1;
    bool ispcut = ev.Tptot[0] > 0.45*ebeam && ev.Tptot[1] > 0.45*ebeam;
    bool isdth  = fabs(dth) < 0.25;
    bool isdphi = fabs(dphi) < 0.15;
    
    bool iscut[]={!isthav,!ispcut,!isdth,!isdphi};
    unsigned mask = 0;
    for(int i=0;i<4;i++) if(iscut[i]) mask|=(1<<i);
    
    return mask;
  }

  int iproc;
  double ebeam;
  TH1D *hp[2],*hpcut[2],*hcosth[2];
  TH1D *hthav,*hdphi,*hxi,*hmxx;

  std::vector<TH1D*> gethists(){
    std::vector<TH1D*> hists={hp[0],hp[1],hpcut[0],hpcut[1],hcosth[0],hcosth[1],hthav,hdphi,hxi,hmxx};
    return hists;
  }
  
  void histCMD_init(int iiproc,double ebeamin){
    iproc = iiproc;
    ebeam = ebeamin;

    TDirectory *curdir=gDirectory;
    curdir->mkdir(Form("%scmd",proc[iproc]))->cd();
    
    const char* pplmn[2][2]={{"mn","-"},{"pl","+"}}; //electron along Z axis 

    for(int i=0;i<2;i++){
      const char* chl=pplmn[i][0];
      const char* ch=pplmn[i][1];
      const char* chn=pplmn[1-i][1];
      hp[i]=new TH1D(Form("hp%s",chl),Form("p%s;2p^{%s}/#sqrt{s};d#sigma/d(2p^{#pm}/#sqrt{s}), nb",ch,ch),
                     55,0.45,1.);
      hpcut[i]=new TH1D(Form("hp%sc",chl),Form("p%s with 0.45<p%s/Ebeam<0.6;2p^{%s}/#sqrt{s};d#sigma/d(2p^{#pm}/#sqrt{s}), nb",ch,chn,ch),
                        55,0.45,1.);
      hcosth[i]=new TH1D(Form("hcth%s",chl),Form("cth%s;cos(#theta^{%s});d#sigma/dcos(#theta), nb",ch,ch),64,-0.64,0.64);
    }
    hthav=new TH1D("hthav","thav;#theta_{av}, rad;d#sigma/d#theta_{av}, nb/rad",50,1,TMath::Pi()-1);
    hdphi=new TH1D("hdphi","dphi;#delta#phi, rad;d#sigma/d#delta#phi, nb/rad",30,0.,0.15);
    hxi=new TH1D("hxi","xi;#delta#theta, rad;d#sigma/d#delta#theta, nb/rad",50,0.,0.25);
    hmxx=new TH1D("hmxx","mxx;M_{XX}, MeV;d#sigma/dM_{XX}, nb/MeV",80,300.,700.);

    curdir->cd();
  }

  bool histCMD_event(event_t& ev){

    //can be added weights re-tuning of different cross parts,
    //the knowledge of which is improved after event generation compared to pre generation estimation
    double weight = ev.weight;
    
    double thav=(ev.Tth[0]+TMath::Pi()-ev.Tth[1])/2;
    double dth=ev.Tth[0]+ev.Tth[1]-TMath::Pi();
    double dphi=ev.Tphi[0]-ev.Tphi[1]-TMath::Pi()+(ev.Tphi[0]<ev.Tphi[1]?2*TMath::Pi():0);
    double pp[]={ev.Tptot[0]/ebeam,ev.Tptot[1]/ebeam};
    TLorentzVector p4vec[2]={
      {0.,0.,ev.Tptot[0],hypot(ev.Tptot[0],mp[iproc])},
      {0.,0.,ev.Tptot[1],hypot(ev.Tptot[1],mp[iproc])}
    };
    for(int i=0;i<2;i++){
      p4vec[i].SetTheta(ev.Tth[i]);
      p4vec[i].SetPhi(ev.Tphi[i]);
    }
    
    double mxx=(p4vec[0]+p4vec[1]).M();
    
    unsigned evmask=selectmaskCMD(ebeam,ev);
    
    if((evmask&~CutType::Th)==0) hthav->Fill(thav,weight);
    if((evmask&~CutType::dTh)==0) hxi->Fill(fabs(dth),weight);
    if((evmask&~CutType::dPhi)==0) hdphi->Fill(fabs(dphi),weight);  
    if(evmask==0) hmxx->Fill(mxx-1e-8,weight);
    //if(mxx>700) cout<<mxx<<" "<<Tptot[0]<<" "<<Tptot[1]<<endl;
     
    for(int i=0;i<2;i++){
      if((evmask&~CutType::P)==0&&pp[1-i]>0.45) hp[i]->Fill(pp[i]-1e-10,weight);
      if((evmask&~CutType::P)==0&&pp[1-i]>0.45&&pp[1-i]<0.6) hpcut[i]->Fill(pp[i]-1e-10,weight);
      if(evmask==0) hcosth[i]->Fill(cos(ev.Tth[i]),weight);
    }

    return (evmask==0);
  }

  void histCMD_end(double ntot){
    int ibinmin=hthav->FindBin(1.+1e-3);
    int ibinmax=hthav->FindBin(TMath::Pi()-1.-1e-3);
    int ibinmid=(ibinmin+ibinmax)/2;
    // std::cout<<ibinmin<<" "<<ibinmid<<" "<<ibinmax<<std::endl;

    double enn[2];
    double nn[]={hthav->IntegralAndError(ibinmin,ibinmid,enn[0]),
                 hthav->IntegralAndError(ibinmid+1,ibinmax,enn[1])};
    double asym=(nn[0]-nn[1])/(nn[0]+nn[1]);
    //error is assuming not correlated nn,
    //same formula in case binominal distribution with ntot=nn[0]+nn[1]
    double easym=2*sqrt(pow(enn[0]*nn[1],2)+pow(enn[1]*nn[0],2))/pow(nn[0]+nn[1],2);
    std::cout<<"Asymmetry "<<asym<<" +- "<<easym<<std::endl;
  }
}

#endif
