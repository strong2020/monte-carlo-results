# MCGPJ 
Authors: A.B. Arbuzov, G.V. Fedotovich, F.V. Ignatov, E.A. Kuraev, A.L. Sibidanov  
[Eur.Phys.J.C 46 (2006) 689-703](https://inspirehep.net/literature/681327)

## Blurb
Monte-Carlo generator for e+e- annihilation into lepton and hadron pairs using collinear structures for photons jets

## Installation and usage
ROOT must be installed and configured (most probably with mathmore option and gsl)
```
tar -jxf MCGPJ_svn1522.tar.bz2
cd radcor
make
export MCGPJDIR=`pwd`
```
To run event generations, for example $e^+e^-\to\pi^+\pi^-$ in the CMD-3 scenario:
```
export VACUUM_POL_DIR=${MCGPJDIR}/data
export INTEG_DIR=${MCGPJDIR}/data
${MCGPJDIR}/hist -v -s 0 -wght -p 2 -ffver 10 -e 350 -rer 0.0001 -dt 0.25 -dp 0.15 -tc 1. -cm 0 -em 0 -am 0 -tup -fname pptuplecmd.root -ns 1000000 &> log.pptuple_cmd &
```
which will produce ROOT ntuple with kinematic variables of particles.


Default parameters for all scenario can be found in script **`dorunsim.sh`**, this script can be useful for a batch submission. The environment variables in header of the script should be tuned for the specific MCGPJ installation (outpwd,rootmacro,MCGPJDIR). 
The options to run script should be given as the first argument with possible mixture of keys for process "pp" or "ee" or "mm" and for scenarios "cmd","kloe","bes","B":
```
./dorunsim.sh _ppcmd_1
```
Running time of single job with number of events to generate as in the script is about few hours.    
The output will be the ROOT file with histograms. The particular scenario can be run in many parallel instances (x1000) to get desired statistical precision (with different option labeling `_ppcmd_1`, `_ppcmd_2`, ...).  
All output ROOT files can be merged together, each histogram sets are reside in different directories inside of files. Simple "hadd" files merging can be done assuming particular scenario and process was simulated with same number of events per job run, otherwise more statistically clever summing must be performed:
```
hadd histoutMCGPJ.root [outpwd]/histout_*.root
```
The dump of the ROOT histograms to the csv format can be done by `dumpcsv.C` script (which will also do rescaling by number of runs per the simulation scenario after jobs merging):
```
root -b -q -l 'scripts/dumpcsv.C("histoutMCGPJ.root","ppkloeLA","./csv")'
```

