# Phokhara

Authors: F. Campanario, H. Czyż, J. Gluza, T. Jeliński, G. Rodrigo, S. Tracz and D. Zhuridov
URL: https://looptreeduality.csic.es/phokhara/

## Blurb
PHOKHARA is a Monte Carlo event generator which simulates radiative Muon and Pion processes at the next-to-leading order (NLO) accuracy.
This includes virtual and soft photon corrections to one photon emission events and the emission of two real hard photons.

## Modifications
 * all `._` files, `*.so`, `.o`, and executables were removed
 Added option to have a .csv output where the weights and events are listed
 Added histogram output to match the needs of the collaboration
 Added Fedor's VP and Pion Form Factor routines as agreed by the collaboration
 Added cuts at the generation level