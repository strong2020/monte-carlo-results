# BabaYaga@NLO 

Authors: E. Budassi, C.M. Carloni Calame, A. Gurgone, G. Montagna, O. Nicrosini, F. Piccinini, F.P. Ucci

Getting the code: please contact one of the authors for a recent version

## Short description

BabaYaga is a Monte Carlo event generator for $e^+e^-\to e^+e^-,
\mu^+\mu^-, \gamma\gamma, \pi^+\pi^-$ processes at flavour factories,
i.e. for energies below 10-12 GeV. It includes the relevant QED
radiative corrections in a QED Parton Shower approach matched with NLO
matrix elements.

## Building and running the code

This version of the code can be compiled specifically for the scenarios described in this `gitlab` repository: by means of preprocessor directives portion of
the code are compiled where some cuts and parameters are hardwired, together with a few tweaks in the phase space generation and various importance
samplings. Furthermore, distributions of variables for that scenario are directly produced.

In order to compile the code, `gcc`, `gfortran`, `make` and `cmake`
must be available. The package ships a version of `LoopTools`,
`COLLIER` and a `C` implementation of `RANLUX`. 

For instance, in order to compile `babayaga` for the CMD scenario and build the executable `babayagaCMD`
```
tar -xjvf babayaga.tar.bz2
cd BabaYaga/
make EXP=CMD
```
Similarly, for all other scenarios, by setting `EXP=KLOEI`, `EXP=KLOEII`,
`EXP=B`, `EXP=BES3` after `make`.
