void renormmerged(const char* filename,const char* filenameout){

  TFile *_file0 = TFile::Open(filename);
  TFile *_fileout = TFile::Open(filenameout,"recreate");

  const char* labels[]={
    "ppkloeSA","mmkloeSA",
    "ppkloeLA","mmkloeLA",
    "ppbes","mmbes",
    "ppB","mmB"
  };

  for(int i=0;i<8;i++){
    const char* label=labels[i];
    TDirectory *dir=(TDirectory*)_file0->Get(label);
    
    if(!dir){cout<<"Directory "<<label<<" isn't present in file"<<endl;continue;}

    _fileout->mkdir(label)->cd();
    
    
    TH1D* hcs=(TH1D*)_file0->Get(Form("%s/hcs",label));
    
    if(strcmp(hcs->GetXaxis()->GetBinLabel(5),"Ngen")!=0||
       strcmp(hcs->GetXaxis()->GetBinLabel(6),"Nruns")!=0) {
      cout<<"Labeling of hcs histogram was changed, please fix macro"<<endl;
      return;
    }
    
    double Ngen=hcs->GetBinContent(5);
    double Nruns=hcs->GetBinContent(6);
    double cs[]={hcs->GetBinContent(1)/Nruns,hcs->GetBinError(1)/Nruns};
    std::cout<<label<<" Total number of simulated events "<<Ngen<<" in "<<Nruns<<" runs "<<std::endl;
    std::cout<<label<<" Selected cross section "<<cs[0]<<" +- "<<cs[1]<<" nb "<<std::endl;
    
    TList *keys=dir->GetListOfKeys();
    
    for(int i=0;i<dir->GetNkeys();i++){
      TKey *key=(TKey*)keys->At(i);
      
      TObject *obj=dir->Get(key->GetName());
      TH1* hist=dynamic_cast<TH1*>(obj);
      if(!hist){
        cout<<"object is not hist ";key->Print();
        continue;
      }
      
      //rescale histogram to number of produced runs
      if(strcmp(key->GetName(),"hcs")==0){
        for(int ib=0;ib<3;ib++) {
          hist->SetBinContent(ib+1,hist->GetBinContent(ib+1)/Nruns);
          hist->SetBinError(ib+1,hist->GetBinError(ib+1)/Nruns);
        }
      }else{
        hist->Scale(1./Nruns);
      }
      hist->Write();
    }
  }
}
