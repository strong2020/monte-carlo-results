# AfkQED
Authors: Vladimir Druzhinin et al

## Blurb
Generator of ISR processes with hard photon.

Simulation of hadronic processes (exept 9,13,14) is based on approach
developed by H.Czyz and J.H.Kuhn in Eur. Phys. J. C18(2001), 497. 
The original EVA program by H.Czyz and J.H.Kuhn for 2pi gamma and 
4pi gamma was modified to implement into BaBar software. 

The simulation e+e- -> mu+mu- gamma process is based on
Born cross section formulae from A.B.Arbuzov et al. hep-ph/9702262.
Simulation includes: ISR, FSR and interference between them, Vacuum polarization (both hadronic and leptonic contributions).

For all processes extra photons radiation from the initial state are generated
with the collinear structure functions along initial particles. 
The exact LO mu+mu-gamma and pi+pi-gamma_ISR (with one hard photon at large angle) are generated in the boosted system after collinear ISR radiation. Extra photons radiation from the final mu,pi and K is generated with PHOTOS.

## Installation and usage
ROOT must be installed and configured for events ntuple production.
```
tar -zxf AfkQed.tar.gz
cd AfkQed
make
export AFKDIR=`pwd`

```
To run event generations, for example $e^+e^-\to\pi^+\pi^-\gamma$, FF2 Strong2020 version, BaBar Scenario:
```
${AFKDIR}/afkrun -i 1 --ffver 2 --cmsenergy 10 --minq2 64. --thph 25. --eph 1. -n 1000000 &> log.pptuple &
```
which will produce ROOT ntuple with kinematic variables of particles.
The ROOT output file is compatible with MCGPJ output and the same ROOT macro (scripts/loophist_run.C from the MCGPJ directory) can be used to produce histograms per different scenaria. 

Default parameters for all scenario can be found in script **`doafksim.sh`**, this script can be useful for a batch submission.
The options to run script should be given as the first argument with possible mixture of keys for process "pp" or "mm" and for scenarios "kloe","bes","B":
```
./doafksim.sh _ppB_1
```

