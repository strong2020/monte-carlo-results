#!/bin/bash

opt=$1
outpwd=/scratch/ignatov/datahistafkqed
rootmacro=/user/ignatov/cmd/scripts/loophist_run.C
export AFKDIR=/user/ignatov/cmd/AfkQed

#outpwd=`pwd`
#rootmacro=/data/cmd/genwork/scripts/loophist_run.C
#export AFKDIR=/data/strong2020/AfkQed

rungen=${AFKDIR}/afkrun

proc=""
[[ $opt =~ "mm" ]] && proc="mm";
[[ $opt =~ "pp" ]] && proc="pp";

scena=""
#[[ $opt =~ "cmd" ]] && scena="cmd";
[[ $opt =~ "kloe" ]] && scena="kloe";
[[ $opt =~ "bes" ]] && scena="bes";
[[ $opt =~ "B" ]] && scena="B";


optnovp=
[[ $opt =~ "novp" ]]  && optnovp="--nvp --ffver 3"

optnofsr=
[[ $opt =~ "nofsr" ]]  && optnofsr="-m 1"

seed=`od -A n -N 4 -t d4 /dev/urandom | sed 's/[- ]*//g'`
#ffver=0  
optffver=
#[ $ffver -ne 0 ] && optffver="-ffver $ffver"

echo "scenario: $scena proc: ${proc}"

( [[ $scena == "" ]] || [[ $proc == "" ]] ) && echo "no Scena or simulation process is defined in run option" && exit

runflags=""

if [[ $scena == "cmd" ]] ; then
   [ $proc == "mm" ] && runflags=""
   [ $proc == "pp" ] && runflags=""
fi

if [[ $scena == "kloe" ]] ; then
    [ $proc == "mm" ] && runflags="-i 0 -m 1 -e 1.02 --minq2 0.01117 --thph 0. --eph .02 -n 10000000"
    [ $proc == "pp" ] && runflags="-i 1 -m 1 --ffver 2 -e 1.02 --minq2 0.07792 --thph 0. --eph .02 -n 10000000"
fi

if [[ $scena == "bes" ]] ; then
    [ $proc == "mm" ] && runflags="-i 0 -e 4 --minq2 0.01117 --thph 23. --eph .4 -n 10000000"
    [ $proc == "pp" ] && runflags="-i 1 --ffver 2 -e 4 --minq2 0.07792 --thph 23. --eph .4 -n 1000000"
fi

if [[ $scena == "B" ]] ; then
    [ $proc == "mm" ] && runflags="-i 0 -e 10 --minq2 64. --thph 25. --eph 3. -n 1000000"
    [ $proc == "pp" ] && runflags="-i 1 --ffver 2 -e 10 --minq2 64. --thph 25. --eph 3. -n 1000000"
fi

runflags="-s ${seed} ${runflags} ${optffver} ${optnovp} ${optnofsr} --output tuple${opt}.root "


#do simulation in temporary dir (TMPDIR can depend on batch environment)
echo "slurm tmp: "$SLURM_TMPDIR" ."
[ "$SLURM_TMPDIR" != "" ] && cd $SLURM_TMPDIR
pwdpath=`pwd`
echo "current dir: "${pwdpath}
TMP=`mktemp -d ${pwdpath}/afkqed.XXXXXXXXXX`
echo "Temp dir name: " $TMP
{ [[ -n "$TMP" ]] && mkdir -p "$TMP"; } || { echo "ERROR: unable to create temporary directory!" 1>&2; exit 1; }
trap "[[ -n \"$TMP\" ]] && { cd ; rm -rf \"$TMP\"; }" 0
cd $TMP

#run everything

echo "Working directory "`pwd`
export BASH_XTRACEFD=1
set -o xtrace
(time ${rungen} ${runflags} ) &> log.tuple${opt}
root -b -q -l $rootmacro'("tuple'${opt}'.root","'${opt}'")' &> log.hist${opt}

#mv log.tuple${opt} log.hist${opt} histout${opt}.root tuple${opt}.root ${outpwd}/
mv log.tuple${opt} log.hist${opt} histout${opt}.root ${outpwd}/

