! compile with gfortran ff.f95 test.f95
program test
  use Fpi
  real(kind=prec) :: e(5),ffref(5)
  e = (/0.77, 0.7, 1.02, 4., 10./) * 1000
  ffref = (/43.94450135693525d0, 25.05778788890657d0, 2.642026968077144d0, 0.001233372303697748d0, 4.176667238989264d-05/)
  print*, abs(FormFactor(e**2))**2  / ffref
  print*, abs(FormFactor(e**2))**2 
end program
