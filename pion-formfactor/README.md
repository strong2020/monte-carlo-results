# Pion vector form factor $`F_\pi`$

We parameterise the pion form factor $`F_\pi(q^2)`$ according to the vector meson dominance (VMD) model.
In particular, we consider the sum of four $`\rho`$ resonances with the inclusion of the $`\rho-\omega`$ and $`\rho-\phi`$ interference
```math
F_\pi(q^2) = \frac{g_\rho(q^2) \big[1+ (q^2/m_\omega^2) \, c_\omega\, b_\omega(q^2) + (q^2/m_\phi^2) \, c_\phi\, b_\phi(q^2) \big]}{1+c_{\rho'}+c_{\rho''}+c_{\rho'''}}
\\[1em]
+  \frac{c_{\rho'}\, g_{\rho'}(q^2) 
+ c_{\rho''}\, g_{\rho''}(q^2)
+ c_{\rho'''}\, g_{\rho'''}(q^2)}{1+c_{\rho'}+c_{\rho''}+c_{\rho'''}}
```
The amplitude of each resonance is a complex number, i.e. $`c_x = |c_x|e^{i\varphi_v}`$.
The narrow $`\omega`$ and $`\phi`$ resonances are described by a Breit-Wigner (BW) function with a constant width
```math
b_v(q^2) = \frac{m_v^2}{m_v^2 - q^2 - i m_v \Gamma_v}
```
for $`v = \omega, \phi`$.
The broad $`\rho`$ resonances are described by a [Gounaris-Sakurai (GS)](https://inspirehep.net/literature/53152) function
```math
g_v(q^2) = \frac{m_v^2 + d(m_v)\,m_v\,\Gamma_v}{m_v^2 - q^2 + f(q^2,m_v,\Gamma_v) - i\, m_v\, \Gamma(q^2,m_v,\Gamma_v)}
```
for $`v = \rho,\rho',\rho'',\rho'''`$ with
```math
\Gamma(q^2,m_v,\Gamma_v) = \Gamma_v \frac{m_v}{\sqrt{q^2}} \left[ \frac{p_\pi(q^2)}{p_\pi(m_v^2)} \right]^3 \\[1em]
p_\pi(q^2) = \frac12 \sqrt{q^2 - 4m^2_\pi} \\[1em]
d(m_v) = \frac{3}{\pi} \frac{m_\pi^2}{p_\pi^2(m_v^2)} \ln{\frac{m_v + 2p_\pi(m_v^2)}{2m_\pi}} + \frac{m_v}{2\pi p_\pi(m_v^2)} - \frac{m_v m_\pi^2}{\pi p_\pi^3(m_v^2)} \\[1em]
f(q^2,m_v,\Gamma_v) = \frac{\Gamma_v m_v^2}{p_\pi^3(m_v^2)} \bigg[ p^2_\pi(q^2) \left[h(q^2) - h(m_v^2)\right] + p^2_\pi(m_v^2) \left(m_v^2-q^2\right) \,\frac{\textrm{d}h}{\textrm{d}q^2}\Big\vert_{q^2=m_v^2} \bigg] \\[1em]
h(q^2) = \frac{2}{\pi} \frac{p_\pi(q^2)}{\sqrt{q^2}} \ln{\frac{\sqrt{q^2}+2p_\pi(q^2)}{2m_\pi}} \\[1em]
\frac{\textrm{d}h}{\textrm{d}q^2} = \frac{h(q^2)}{8} \left[\frac{1}{p^2_\pi(q^2)} - \frac{4}{q^2} \right] + \frac{1}{2\pi q^2}
```
We choose the following parameters

|                      | $`\rho   `$ | $`\rho'`$   | $`\rho''`$  | $`\rho'''`$ | $`\omega`$  | $`\phi`$    |
| -------------------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| $`m_{x}`$ (MeV)           | 774.56      | 1485.9      | 1866.8      | 2264.5      | 782.48      | 1019.47     |
| $`\Gamma_{x}`$ (MeV)      | 148.32      | 373.60      | 303.34      | 113.27      | 8.55        | 4.25        |
| $`\vert c_{x}\vert`$ | 1           | 0.14104      | 0.0614      | 0.0047      | 0.00158     | 0.00045     |
| $`\varphi_{x}`$ (rad)      | 0           | 3.7797       | 1.429       | 0.921       | 0.075       | 2.888       |
