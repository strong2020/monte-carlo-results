import numpy as np
import numpy.typing

ndr = numpy.typing.NDArray[np.float64]
ndc = numpy.typing.NDArray[np.complex128]


__all__ = ["formfactor"]

imag = 0 + 1j


def bw(q2: ndr, mass: float, width: float) -> ndc:
    return mass**2 / (mass**2 - q2 - imag * mass * width)


def gs(q2: ndr, mass: float, width: float) -> ndc:
    mpi = 139.57039

    pm = 0.5 * np.sqrt(mass**2 - 4 * mpi**2)
    pq = 0.5 * np.sqrt(q2 - 4 * mpi**2)

    gam = width * (pq / pm) ** 3 * np.sqrt(mass**2 / q2)

    d = (
        3 / np.pi * mpi**2 / pm**2 * np.log((mass + 2 * pm) / 2 / mpi)
        + 0.5 * mass / np.pi / pm
        - mass * mpi**2 / np.pi / pm**3
    )

    hq = 2 / np.pi * pq / np.sqrt(q2) * np.log((np.sqrt(q2) + 2 * pq) / 2 / mpi)
    hm = 2 / np.pi * pm / mass * np.log((mass + 2 * pm) / 2 / mpi)
    hp = 0.5 * mpi**2 / pm**2 / mass**2 * hm + 0.5 / np.pi / mass**2

    f = (
        width
        * mass**2
        / pm**3
        * (pq**2 * (hq - hm) + (mass**2 - q2) * pm**2 * hp)
    )
    return (mass**2 + d * mass * width) / (mass**2 - q2 + f - imag * mass * gam)


def formfactor(q2: ndr) -> ndc:
    mr = 774.56
    gr = 148.32

    mw = 782.48
    gw = 8.55
    cw = 0.00158
    aw = 0.075

    mphi = 1019.47
    gphi = 4.25
    cphi = 0.00045
    aphi = 2.888

    mrr = 1485.9
    grr = 373.60
    crr = 0.14104
    arr = 3.7797

    mrrr = 1866.8
    grrr = 303.34
    crrr = 0.0614
    arrr = 1.429

    mrrrr = 2264.5
    grrrr = 113.27
    crrrr = 0.0047
    arrrr = 0.921

    cw = cw * np.exp(imag * aw)
    cphi = cphi * np.exp(imag * aphi)
    crr = crr * np.exp(imag * arr)
    crrr = crrr * np.exp(imag * arrr)
    crrrr = crrrr * np.exp(imag * arrrr)

    return (
        gs(q2, mr, gr)
        * (
            1
            + q2 / mw**2 * cw * bw(q2, mw, gw)
            + q2 / mphi**2 * cphi * bw(q2, mphi, gphi)
        )
        + crr * gs(q2, mrr, grr)
        + crrr * gs(q2, mrrr, grrr)
        + crrrr * gs(q2, mrrrr, grrrr)
    ) / (1 + crr + crrr + crrrr)


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    e = np.array([0.77, 0.7, 1.02, 4, 10]) * 1000
    ref = np.array(
        [
            -0.0013541250487582996 + 6.629064754796151j,
            3.7949311510768275 + 3.264396643714317j,
            -1.4811580447869879 + 0.6694757758425103j,
            -0.03498032032614448 + 0.0031224179057376686j,
            -0.006244293525371995 + 0.0016659744172375516j,
        ]
    )
    print(np.abs(formfactor(e**2)) ** 2 / np.abs(ref) ** 2)

    e = np.linspace(2 * 139.57039, 3000, 10000)
    plt.plot(e / 1e3, np.abs(formfactor(e**2)) ** 2)
    plt.yscale("log")
    plt.ylabel(r"$|F_\pi(q^2)|^2$")
    plt.xlabel(r"$\sqrt{s}\,/\,{\rm GeV}$")
    plt.show()
