module Fpi
  implicit none
  interface formfactor
    module procedure formfactor_1d, formfactor_0d
  end interface
  public :: formfactor

  private

  integer, public, parameter :: prec = 8

  real(kind=prec), parameter :: pi = 3.14159265358979323846264338328_prec
  real(kind=prec), parameter :: Mpi = 139.57039_prec
  real(kind=prec), parameter :: mr = 774.56_prec
  real(kind=prec), parameter :: gr = 148.32_prec

  real(kind=prec), parameter :: mw = 782.48_prec
  real(kind=prec), parameter :: gw = 8.55_prec
  real(kind=prec), parameter :: cw = 0.00158_prec
  real(kind=prec), parameter :: aw = 0.075_prec

  real(kind=prec), parameter :: mphi = 1019.47_prec
  real(kind=prec), parameter :: gphi = 4.25_prec
  real(kind=prec), parameter :: cphi = 0.00045_prec
  real(kind=prec), parameter :: aphi = 2.888_prec

  real(kind=prec), parameter :: mrr = 1485.9_prec
  real(kind=prec), parameter :: grr = 373.60_prec
  real(kind=prec), parameter :: crr = 0.14104_prec
  real(kind=prec), parameter :: arr = 3.7797_prec

  real(kind=prec), parameter :: mrrr = 1866.8_prec
  real(kind=prec), parameter :: grrr = 303.34_prec
  real(kind=prec), parameter :: crrr = 0.0614_prec
  real(kind=prec), parameter :: arrr = 1.429_prec

  real(kind=prec), parameter :: mrrrr = 2264.5_prec
  real(kind=prec), parameter :: grrrr = 113.27_prec
  real(kind=prec), parameter :: crrrr = 0.0047_prec
  real(kind=prec), parameter :: arrrr = 0.921_prec

  complex(kind=prec), parameter :: i_ = (0._prec, 1._prec)
  complex(kind=prec), parameter :: dw = cw * exp(i_ * aw)
  complex(kind=prec), parameter :: dphi = cphi * exp(i_ * aphi)
  complex(kind=prec), parameter :: drr = crr * exp(i_ * arr)
  complex(kind=prec), parameter :: drrr = crrr * exp(i_ * arrr)
  complex(kind=prec), parameter :: drrrr = crrrr * exp(i_ * arrrr)

contains

  PURE FUNCTION BW(Q2, MASS, WIDTH)
  real(kind=prec), intent(in) :: q2(:), mass, width
  complex(kind=prec) :: bw(size(q2))

  bw = mass**2 / (mass**2 - q2 - i_ * mass * width)

  END FUNCTION BW

  PURE FUNCTION GS(Q2, MASS, WIDTH)
  implicit none
  real(kind=prec), intent(in) :: q2(:), mass, width
  complex(kind=prec) :: gs(size(q2))
  real(kind=prec) :: pm, d, hm, hp
  real(kind=prec), dimension(size(q2)) :: pq, gam, hq, f

  pm = 0.5 * sqrt(mass**2 - 4*mpi**2)
  pq = 0.5 * sqrt(abs(q2 - 4*mpi**2))

  d = 3. / pi * mpi**2 / pm**2 * log((mass + 2 * pm) / 2 / mpi) &
      + 0.5 * mass / pi / pm - mass * mpi**2 / pi / pm**3
  hm = 2 / pi * pm / mass     * log((mass     + 2 * pm) / 2 / mpi)
  hp = 0.5 * mpi**2 / pm**2 / mass**2 * hm + 0.5 / pi / mass**2

  where (q2>4*mpi**2)
     gam = width * (pq / pm) ** 3 * sqrt(mass**2 / q2)
     hq = 2 / pi * pq / sqrt(abs(q2)) * log((sqrt(abs(q2)) + 2 * pq) / 2 / mpi)
     f = width * mass**2 / pm**3 * ((hq - hm) * pq**2 + (mass**2 - q2) * hp * pm**2)
  elsewhere(abs(q2)<1d-50)
     gam = 0.d0
     hq = 1 / pi
     f = width * mass**2 / pm**3 * (-(hq - hm) * pq**2 + (mass**2 - q2) * hp * pm**2)
  elsewhere
     gam = 0.d0
     hq = 2 / pi * pq / sqrt(abs(q2)) * atan(sqrt(abs(q2)) / pq / 2)
     f = width * mass**2 / pm**3 * (-(hq - hm) * pq**2 + (mass**2 - q2) * hp * pm**2)
  end where

  gs = (mass**2 + d * mass * width) / (mass**2 - q2 + f - i_ * mass * gam)

  END FUNCTION GS

  PURE FUNCTION FORMFACTOR_1d(Q2)
  implicit none
  real(kind=prec), intent(in) :: q2(:)
  complex(kind=prec) :: formfactor_1d(size(q2))

  formfactor_1d = &
    gs(q2, mr, gr) * (1 + q2 / mw**2 * dw * bw(q2, mw, gw) + q2 / mphi**2 * dphi * bw(q2, mphi, gphi))
  formfactor_1d = formfactor_1d &
      + drr * gs(q2, mrr, grr) + drrr * gs(q2, mrrr, grrr) + drrrr * gs(q2, mrrrr, grrrr)

  formfactor_1d = formfactor_1d / (1 + drr + drrr + drrrr)

  END FUNCTION FORMFACTOR_1d

  PURE FUNCTION FORMFACTOR_0d(Q2)
  implicit none
  real(kind=prec), intent(in) :: q2
  complex(kind=prec) :: formfactor_0d, buf(1)

  buf = formfactor_1d((/q2/))
  formfactor_0d = buf(1)

  END FUNCTION FORMFACTOR_0d
end module
