We use on-shell masses and coupling

* $`\alpha = 1/137.035999084`$
* $`m_e    =     0.51099895000 \,\mathrm{Mev}`$
* $`m_\mu  =   105.6583755  \,\mathrm{MeV}`$
* $`m_\tau =  1776.86     \,\mathrm{MeV}`$
* $`m_\pi  =   139.57039    \,\mathrm{MeV}`$
* $`m_t    = 172500\,\mathrm{MeV}`$
