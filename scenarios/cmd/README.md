# CMD scenario

 * Process: $`ee\to ee,\mu\mu,\pi\pi`$

 * Energy: $`\sqrt s=0.7\,\mathrm{GeV}`$

## Variables

We define
 * $`\theta_{\rm avg} = (\theta^- - \theta^+ + \pi)/2`$
 * $`p_\pm = |\vec p_\pm|`$
 * $`\delta\phi = ||\phi^+-\phi^-|-\pi|`$
 * $`\xi = |\theta^++\theta^--\pi|`$
 * $`M_{XX} = \sqrt{(p_+ + p_-)^2}`$

The scattering angles are defined with respect to the electron momentum
```math
\cos \theta^{\pm} = \frac{p^{\pm} \cdot p_{e^-}}{|p^{\pm}|\,|p_{e^-}|}
```
## Cuts

We enforce four cuts
 * $`1\,{\rm rad} \le \theta_{\rm avg} \le \pi - 1\,{\rm rad}`$
 * $`p_\pm > 0.45\times \sqrt{s}/2`$
 * $`|\delta\phi| < 0.15\,{\rm rad}`$
 * $`|\xi| < 0.25\,{\rm rad}`$

## Histograms

Uniform binning is achieved with 600 bins

| variable                 | name    | lower  | upper | bins |   | lower | upper | selection |
| ------------------------ | ------- | ------ | ----- | ---- | - | ----- | ----- | --------- |
| $`2p_+/\sqrt{s}`$        | `p+`    | 0.45   | 1.00  | 55   |   | 0.4   | 1.0   | `50::10`  |
| $`2p_-/\sqrt{s}`$        | `p-`    | 0.45   | 1.00  | 55   |   | 0.4   | 1.0   | `50::10`  |
| $`2p_+/\sqrt{s}`$ if #   | `p+c`   | 0.45   | 1.00  | 55   |   | 0.4   | 1.0   | `50::10`  |
| $`2p_-/\sqrt{s}`$ if #   | `p-c`   | 0.45   | 1.00  | 55   |   | 0.4   | 1.0   | `50::10`  |
| $`\theta_{\rm avg}`$     | `thav`  | 1.00   | pi-1  | 50   |   | 1.0   | pi-1  | `::12`    |
| $`\delta\phi`$           | `dphi`  | 0.00   | 0.15  | 30   |   | 0.0   | 0.15  | `::20`    |
| $`\xi`$                  | `xi`    | 0.00   | 0.25  | 50   |   | 0.0   | 0.25  | `::12`    |
| $`M_{XX}`$               | `mxx`   | 300    | 700   | 80   |   | 200   | 700   | `120::6`  |
| $`\cos\theta^+`$         | `cth+`  |-0.64   | 0.64  | 64   |   | -1    | 1   | `108:492:6` |
| $`\cos\theta^-`$         | `cth-`  |-0.64   | 0.64  | 64   |   | -1    | 1   | `108:492:6` |

where # indicates that this is only to be done if the other $`0.45 < 2p_\mp/\sqrt{s} < 0.6`$
