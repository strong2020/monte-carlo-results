                !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use mcmule
  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nrq = 10
  integer, parameter :: nrbins = 600
  integer :: hvp
  real(kind=prec), parameter :: &
     min_val(nrq) = (/ 0.40, 0.40, 1.0, 0.0, 0.00, 200., -1.0, -1.0, 0.40, 0.40 /)
  real(kind=prec), parameter :: &
     max_val(nrq) = (/ 1.00, 1.00, pi-1, 0.15, 0.25, 700., 1.0, 1.0, 1.00, 1.00 /)
  integer :: userdim = 0

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer :: namesLen=6
  integer :: filenamesuffixLen=10
  integer :: nq=nrq
  integer :: nbins=nrbins



!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU
    implicit none

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "This is a McMule userfile for the CMD scenario"
  print*, " * 1rad < th_av < pi-1rad"
  print*, " * |\vec p| > 0.45 sqrt(s)/2"
  print*, " * ||phi+-phi-|-pi| < 0.15rad"
  print*, " * xi < 0.25rad"

  if(which_piece(1:5) == "eb2eb") then
    call initflavour("mu-e", 0.7e3**2)
    print*, " * running e-e+ --> e-e+"
  elseif(which_piece(1:5) == "ee2mm") then
    call initflavour("mu-e", 0.7e3**2)
    print*, " * running e-e+ --> mu-mu+"
  elseif(which_piece(1:5) == "ee2uu") then
    call initflavour("pi-e", 0.7e3**2)
    print*, " * running e-e+ --> pi-pi+"
  endif

  read*, hvp
  write(filenamesuffix,'(I1)') hvp
  if(hvp.eq.1) then
    HVPmodel = 'alphaQEDc19'
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, "Only hadronic vp for fermionic pieces"
  elseif(hvp.eq.2) then
    nhad = 0._prec
    nel  = 1._prec
    nmu  = 1._prec
    ntau = 1._prec
    print*, "Only leptonic vp for fermionic pieces"
  elseif(hvp.eq.3) then
    HVPmodel = 'nsk-2.9'
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, "Only nsk hadronic vp for fermionic pieces"
  endif
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real(kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real(kind=prec) :: quant(nr_q)
  real(kind=prec) :: thm, phim, pm
  real(kind=prec) :: thp, phip, pp
  real(kind=prec) :: thavg, dphi, xi, mxx

  !! ==== keep the line below in any case ==== !!
  call fix_mu

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)
  pol2 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.

  thm = acos(cos_th(q1, q3)) ; phim = phi(q3)
  thp = acos(cos_th(q1, q4)) ; phip = phi(q4)

  pm = absvec(q3)
  pp = absvec(q4)

  thavg = (thm-thp+pi)/2
  dphi  = abs(abs(phip-phim)-pi)
  xi    = abs(thp+thm-pi)

  mxx = sqrt(sq(q3+q4))

  if(1 > thavg .or. thavg > pi-1) pass_cut = .false.
  if(pm < 0.45 * sqrt(scms)/2) pass_cut = .false.
  if(pp < 0.45 * sqrt(scms)/2) pass_cut = .false.
  if(dphi > 0.15) pass_cut = .false.
  if(xi > 0.25) pass_cut = .false.

  if(0.45 > pm / (sqrt(scms)/2) .or. pm / (sqrt(scms)/2) > 0.6) pass_cut( 9) = .false.
  if(0.45 > pp / (sqrt(scms)/2) .or. pp / (sqrt(scms)/2) > 0.6) pass_cut(10) = .false.

  names(1) = "p+"
  quant(1) = pp / (sqrt(scms)/2)
  names(2) = "p-"
  quant(2) = pm / (sqrt(scms)/2)
  names(3) = "thav"
  quant(3) = thavg
  names(4) = "dphi"
  quant(4) = dphi
  names(5) = "xi"
  quant(5) = xi
  names(6) = "mxx"
  quant(6) = mxx
  names(7) = "cth+"
  quant(7) = cos(thp)
  names(8) = "cth-"
  quant(8) = cos(thm)
  names( 9) = "p+c"
  quant( 9) = pp / (sqrt(scms)/2)
  names(10) = "p-c"
  quant(10) = pm / (sqrt(scms)/2)

  END FUNCTION QUANT


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
