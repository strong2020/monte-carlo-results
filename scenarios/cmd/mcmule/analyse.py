from pymule import *

loConv=1000*conv*alpha**2
nloConv=1000*conv*alpha**3
nnloConv=1000*conv*alpha**4

lower = {
    'p+':    6,
    'p-':    6,
    'p+c':   6,
    'p-c':   6,
    'thav':  1,
    'dphi':  1,
    'xi':    1,
    'mxx':  21,
    'cth+': 19,
    'cth-': 19,
}
upper = {
    'p+':   -1,
    'p-':   -1,
    'p+c':  -1,
    'p-c':  -1,
    'thav': -1,
    'dphi': -1,
    'xi':   -1,
    'mxx':  -1,
    'cth+': 83,
    'cth-': 83,
}
setup(merge={
    'p+':   10,
    'p-':   10,
    'p+c':  10,
    'p-c':  10,
    'thav': 12,
    'dphi': 20,
    'xi':   12,
    'mxx':   6,
    'cth+':  6,
    'cth-':  6
})

lo      = {}
dnloB   = {}
dnloBISC = {}
dnloFh  = {}
dnloFhISC = {}
dnloFl  = {}
dnloFlISC = {}
nlo     = {}
nloISC  = {}
nlol   = {}
nlob   = {}
dnnloB  = {}
dnnloFh = {}
dnnloFl = {}
nnlo    = {}
nnlol  = {}
nnlob  = {}

# Bhabha
setup(folder="bhabha-bosonic/out.tar.bz2")
setup(obs="0")
lo['ee']      = mergefks(sigma('eb2eb0')) * loConv
dnloB['ee']   = mergefks(sigma('eb2ebF'),
                         sigma('eb2ebR125'), sigma('eb2ebR35'), sigma('eb2ebR45')) * nloConv
dnloBISC['ee'] = dnloB['ee']
dnnloB['ee']  = mergefks(sigma('eb2ebFF'),
                         sigma('eb2ebRF125'), sigma('eb2ebRF35'), sigma('eb2ebRF45'),
                         sigma('eb2ebRR15162526'), sigma('eb2ebRR3536'), sigma('eb2ebRR4546')) * nnloConv
setup(folder="bhabha-fermionic/out.tar.bz2")
setup(obs="3")
dnloFh['ee']  = mergefks(sigma('eb2ebA')) * nloConv
dnloFhISC['ee'] = dnloFh['ee']
dnnloFh['ee'] = mergefks(sigma('eb2ebAF'), sigma('eb2ebAR125'), sigma('eb2ebAR35'), sigma('eb2ebAR45'),
                         anyxi1=sigma('eb2ebAA'),
                         anyxi2=sigma('eb2ebNFTRHYP'), anyxi3=sigma('eb2ebNFBXDISP')) * nnloConv
setup(obs="2")
dnloFl['ee']  = mergefks(sigma('eb2ebA')) * nloConv
dnloFlISC['ee'] = dnloFl['ee']
dnnloFl['ee'] = mergefks(sigma('eb2ebAF'), sigma('eb2ebAR125'), sigma('eb2ebAR35'), sigma('eb2ebAR45'),
                         anyxi1=sigma('eb2ebAA'),
                         anyxi2=sigma('eb2ebNFTRHYP'), anyxi3=sigma('eb2ebNFBXDISP')) * nnloConv

nlo['ee']   = lo['ee'] + dnloB['ee'] + dnloFh['ee'] + dnloFl['ee']
nloISC['ee'] = nlo['ee']
nlol['ee']  = lo['ee'] + dnloB['ee'] + dnloFl['ee']
nlob['ee']  = lo['ee'] + dnloB['ee']
nnlo['ee']  = nlo['ee'] + dnnloB['ee'] + dnnloFh['ee'] + dnnloFl['ee']
nnlol['ee'] = nlol['ee'] + dnnloB['ee'] + dnnloFl['ee']
nnlob['ee'] = nlob['ee'] + dnnloB['ee']

# Mupair
setup(folder="muons-bosonic/out.tar.bz2")
setup(obs="0")
lo['mm']      = mergefks(sigma('ee2mm0')) * loConv
dnloB['mm']   = mergefks(sigma('ee2mmFEE'), sigma('ee2mmFEM'), sigma('ee2mmFMM'),
                         sigma('ee2mmREE'), sigma('ee2mmREM'), sigma('ee2mmRMM')) * nloConv
dnloBISC['mm']   = mergefks(sigma('ee2mmFEE'), sigma('ee2mmREE')) * nloConv
dnnloB['mm']  = mergefks(sigma('ee2mmFFEEEE'), sigma('ee2mmFFMIXDz'), sigma('ee2mmFFMMMM'),
                         sigma('ee2mmRFEEEE'), sigma('ee2mmRFMIXD'),  sigma('ee2mmRFMMMM'),
                         sigma('ee2mmRREEEE'), sigma('ee2mmRRMIXD'),  sigma('ee2mmRRMMMM')) * nnloConv
setup(folder="muons-fermionic/out.tar.bz2")
setup(obs="3")
dnloFh['mm']  = mergefks(sigma('ee2mmA')) * nloConv
dnloFhISC['mm']  = dnloFh['mm']
dnnloFh['mm'] = mergefks(sigma('ee2mmAFEE'), sigma('ee2mmAFEM'), sigma('ee2mmAFMM'),
                         sigma('ee2mmAREE'), sigma('ee2mmAREM'), sigma('ee2mmARMM'),
                         anyxi=sigma('ee2mmAA'),
                         anyxi1=sigma('ee2mmNFEEHYP'),
                         anyxi2=sigma('ee2mmNFEMDISP'),
                         anyxi3=sigma('ee2mmNFMMHYP')) * nnloConv
setup(obs="2")
dnloFl['mm']  = mergefks(sigma('ee2mmA')) * nloConv
dnloFlISC['mm']  = dnloFl['mm']
dnnloFl['mm'] = mergefks(sigma('ee2mmAFEE'), sigma('ee2mmAFEM'), sigma('ee2mmAFMM'),
                         sigma('ee2mmAREE'), sigma('ee2mmAREM'), sigma('ee2mmARMM'),
                         anyxi=sigma('ee2mmAA'),
                         anyxi1=sigma('ee2mmNFEEHYP'),
                         anyxi2=sigma('ee2mmNFEMDISP'),
                         anyxi3=sigma('ee2mmNFMMHYP')) * nnloConv

nlo['mm']   = lo['mm'] + dnloB['mm'] + dnloFh['mm'] + dnloFl['mm']
nloISC['mm'] = lo['mm'] + dnloBISC['mm'] + dnloFhISC['mm'] + dnloFlISC['mm']
nlol['mm']  = lo['mm'] + dnloB['mm'] + dnloFl['mm']
nlob['mm']  = lo['mm'] + dnloB['mm']
nnlo['mm']  = nlo['mm'] + dnnloB['mm'] + dnnloFh['mm'] + dnnloFl['mm']
nnlol['mm'] = nlol['mm'] + dnnloB['mm'] + dnnloFl['mm']
nnlob['mm'] = nlob['mm'] + dnnloB['mm']

# Pions
setup(folder="pions-bosonic/out.tar.bz2")
setup(obs="0")
lo['uu']      = mergefks(sigma('ee2uu0')) * loConv
dnloB['uu']   = mergefks(sigma('ee2uuFEE'), sigma('ee2uuREE'),
                         sigma('ee2uuFEU0'), sigma('ee2uuFEU1'),sigma('ee2uuFEU2'), sigma('ee2uuREU')) * nloConv
dnloBISC['uu']   = mergefks(sigma('ee2uuFEE'), sigma('ee2uuREE')) * nloConv
dnnloB['uu']  = mergefks(sigma('ee2uuFFEEEE'), sigma('ee2uuRFEEEE'), sigma('ee2uuRREEEE')) * nnloConv
setup(folder="pions-fermionic/out.tar.bz2")
setup(obs="3")
dnnloFh['uu'] = mergefks(sigma('ee2uuNFEE')) * nnloConv
setup(obs="2")
dnnloFl['uu'] = mergefks(sigma('ee2uuNFEE')) * nnloConv

nlo['uu']   = lo['uu'] + dnloB['uu']
nloISC['uu']   = lo['uu'] + dnloBISC['uu']
nlol['uu']  = nlo['uu']
nlob['uu']  = nlo['uu']
nnlo['uu']  = nlo['uu'] + dnnloB['uu'] + dnnloFh['uu'] + dnnloFl['uu']
nnlol['uu'] = nlo['uu'] + dnnloB['uu'] + dnnloFl['uu']
nnlob['uu'] = nlo['uu'] + dnnloB['uu']

# Export
for k in lo.keys():
    print(f"CHANNEL {k}: run-time = {nnlo[k].time:.2f}s")
    print(f"LO xsec = {lo[k].value[0]} +- {lo[k].value[1]} nb")
    print(f"NLO xsec  = {nlo[k].value[0]} +- {nlo[k].value[1]} nb")
    print(f"NLOISC xsec  = {nloISC[k].value[0]} +- {nloISC[k].value[1]} nb")
    print(f"NLOL xsec  = {nlol[k].value[0]} +- {nlol[k].value[1]} nb")
    print(f"NLOB xsec  = {nlob[k].value[0]} +- {nlob[k].value[1]} nb")
    print(f"NNLO xsec = {nnlo[k].value[0]} +- {nnlo[k].value[1]} nb")
    print(f"NNLOL xsec = {nnlol[k].value[0]} +- {nnlol[k].value[1]} nb")
    print(f"NNLOB xsec = {nnlob[k].value[0]} +- {nnlob[k].value[1]} nb")
    for h in lo[k].histograms:
        w = lo[k][h][2,0] - lo[k][h][1,0]
        np.savetxt(f'data/{k}-{h}.csv', np.column_stack([
            lo [k][h][lower[h]:upper[h],0] - w/2,
            lo [k][h][lower[h]:upper[h],0] + w/2,
            lo [k][h][lower[h]:upper[h],1],
            lo [k][h][lower[h]:upper[h],2],
            nlo[k][h][lower[h]:upper[h],1],
            nlo[k][h][lower[h]:upper[h],2],
            nloISC[k][h][lower[h]:upper[h],1],
            nloISC[k][h][lower[h]:upper[h],2],
            nlol[k][h][lower[h]:upper[h],1],
            nlol[k][h][lower[h]:upper[h],2],
            nlob[k][h][lower[h]:upper[h],1],
            nlob[k][h][lower[h]:upper[h],2],
            nnlo[k][h][lower[h]:upper[h],1],
            nnlo[k][h][lower[h]:upper[h],2],
            nnlol[k][h][lower[h]:upper[h],1],
            nnlol[k][h][lower[h]:upper[h],2],
            nnlob[k][h][lower[h]:upper[h],1],
            nnlob[k][h][lower[h]:upper[h],2],
        ]), delimiter=',', header="xl,xh,LO,dLO,NLO,dNLO,NLOISC,dNLOISC,NLOL,dNLOL,NLOnovp,dNLOnovp,NNLO,dNNLO,NNLOL,dNNLOL,NNLOnovp,dNNLOnovp")
