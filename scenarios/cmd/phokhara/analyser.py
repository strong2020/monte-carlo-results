import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

def plotter(columns, weights, bins, error, savename, plotting=False):
      figure = plt.figure()
      axes = figure.add_subplot()
      y, bin_limits, _ = axes.hist(columns, bins=bins, weights=weights )
      bin_centers = 0.5*(bin_limits[1:] + bin_limits[:-1])
      axes.errorbar(bin_centers, y, yerr = error, marker = '.', linestyle="none")

      if (plotting):
          figure.savefig(savename)
      plt.close()
      return

finals = ["Muons", "Pions"]
orders = ["LO", "LOvp"]

for final in finals:
    titles = []
    for order in orders:
        with open(f"{final}/Outputs/{order}.dat", 'r') as file:
                # Iterate through each line in the file
                values = []
                title = None
                for line in file:
                    words = line.split()       
                    try:
                        for word in words:
                             values.append(float(word))
                    except:
                         if len(values)!=0 and title!=None:
                              data = pd.DataFrame(np.array(values).reshape((int(len(values)/4),4)), columns=["xl", "xh", f"{order}", f"d{order}"])
                              data.to_csv(f"{title}_{order}_temp.csv", index=False)
                              plotter(data.xl.values, data[f"{order}"].values, len(data), data[f"d{order}"].values, f"{final}/Plots/{order}_{title}.png", plotting=True)
                              values.clear()
                              title = None
                         if "Summed" in words:
                              title = words[1]
                              if order=="LO": titles.append(title)
                if len(values)!=0 and title!=None:
                              data = pd.DataFrame(np.array(values).reshape((int(len(values)/4),4)), columns=["xl", "xh", f"{order}", f"d{order}"])
                              data.to_csv(f"{title}_{order}_temp.csv", index=False)
                              plotter(data.xl.values, data[f"{order}"].values, len(data), data[f"d{order}"].values, f"{final}/Plots/{order}_{title}.png", plotting=True)
                              values.clear()
                              title = None

    for title in titles:
          for i, order in enumerate(orders):
               if i == 0:
                   data = pd.read_csv(f"{title}_{order}_temp.csv")
               else:
                   data_dummy = pd.read_csv(f"{title}_{order}_temp.csv")
                   data[f"{order}"] = data_dummy[f"{order}"]                  
                   data[f"d{order}"] = data_dummy[f"d{order}"]
                   
               os.remove(f"{title}_{order}_temp.csv")
          data.to_csv(f"{final}/Results/{title}.csv", index=False)
            
 