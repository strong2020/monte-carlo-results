c ========================================================================
c Histograms for dxs/variables for CMD scenario in Strong2020. 
c ------------------------------------------------------------------------
      subroutine output_variables(n, value, i)
       ! n refers to a process with 0, 1 (n=0) or 2 (n=1) photons
       ! value is the weight for the MC histograms
       ! i is 0 (1) for the (un)weighted histograms
      include 'phokhara_10.0.inc'
      integer n, i, k 
      real*8 value, s, pp, pm, ppc, pmc, thav, dphi, xi, mxx, cthp, cthm
      real*8 phip, phim
      real*8, allocatable :: variables(:)
      allocate(variables(1:total_num_hist))

      ! Calculate variables to plot
      s = momenta(3,0) + momenta(4,0) + momenta(6,0) + momenta(7,0)
      pp = dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2) 
      pm = dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2) 
      cthp = -momenta(6,3) / pp  
      cthm = -momenta(7,3) / pm
      thav = (-dacos(cthp)+dacos(cthm)+pi)/2
      xi = abs(dacos(cthp)+dacos(cthm)-pi)
      phip = atan2(momenta(6,2), momenta(6,1))
      phim = atan2(momenta(7,2), momenta(7,1))
      dphi = abs(abs(phip-phim) - pi)
      pp = 2*pp/s
      pm = 2*pm/s
      ppc = 0  ! So that it is out of the histogram bounds
      pmc = 0  ! So that it is out of the histogram bounds
      if (pm.gt.0.45.and.pm.lt.0.6) ppc = pp
      if (pp.gt.0.45.and.pp.lt.0.6) pmc = pm
      mxx =dSqrt((momenta(6,0) + momenta(7,0))**2 -
     &       (momenta(6,1) + momenta(7,1))**2 - 
     &       (momenta(6,2) + momenta(7,2))**2 - 
     &       (momenta(6,3) + momenta(7,3))**2) * 1000

      ! Save to variables in the same order as the requested histograms
      variables = (/pp, pm, ppc, pmc, thav, dphi, xi, mxx, cthp, cthm/)

      ! Save to the corersponding histograms
      call distribute_variables(variables, i, n, value)

      return
      end
c ========================================================================
c --- Experimental cuts for CMD scenario Strong2020
c ------------------------------------------------------------------------
      subroutine advanced_testcuts(accepted)
      include 'phokhara_10.0.inc'
      logical accepted
      real*8 thp, thm, thavg, pp, pm, dphi
      real*8 xi, s, phip, phim
      s = momenta(3,0) + momenta(4,0) + momenta(6,0) + momenta(7,0)

      ! Cut on p+ & p-
      pp = dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2)
      pm = dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2)

      accepted = (accepted.and.(pp.gt.(0.45*s/dSqrt(2.d0))).and.
     &     (pm.gt.(0.45*s/dSqrt(2.d0))))
      if (.not.accepted) return
      
      ! Cut on th+ & th-
      thp = dacos(-momenta(6,3)/pp)
      thm = dacos(-momenta(7,3)/pm)
      
      thavg = (thp - thm + pi)/2
      accepted = (accepted.and.(thavg.ge.1).and.(thavg.le.(pi-1)))
      if (.not.accepted) return
      
      ! Cut on xi
      xi = ABS(thp+thm-pi)
      accepted = (accepted.and.(xi.le.0.25))
      if (.not.accepted) return

      ! Cut on dphi
      phip = atan2(momenta(6,2), momenta(6,1))
      phim = atan2(momenta(7,2), momenta(7,1))
      dphi = abs(abs(phip-phim) - pi)
      accepted = (accepted.and.(dphi.lt.0.15))

      end 