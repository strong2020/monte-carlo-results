                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use mcmule
  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nrq = 13
  integer, parameter :: nrbins = 600
  integer :: scenario
  real(kind=prec), parameter :: &
     min_val(nrq) = (/ 0.25, 0.25, 1000., 1000., 0., 0., 0., 0.52, 8000., 3000., &
                        0.2, 0., 0.                                              /)
  real(kind=prec), parameter :: &
     max_val(nrq) = (/ 2.75, 2.75, 5000., 5000., 8000., 1., 1. , 2.63, 10000., 5000., &
                       2.7, 2400., 0.3                                                       /)
  integer :: userdim = 0
  integer :: hvp

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer :: namesLen=6
  integer :: filenamesuffixLen=10
  integer :: nq=nrq
  integer :: nbins=nrbins



!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU
    implicit none

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "This is a McMule userfile for the B scenario"
  print*, " * 0.65 rad < th_pm < 2.75 rad"
  print*, " * pvec_pm > 1 GeV"
  print*, " * 0.6 rad < th_gamma < 2.7 rad"
  print*, " * E_gamma > 3 GeV"
  print*, " * th_{gamma,tgamma} < 0.3 rad"
  print*, " * M_XXgamma > 8 GeV"
  print*, " * M_ee > 0.3 GeV"


  if(which_piece(1:5) == "eb2eb") then
    call initflavour("mu-e", 10.e3**2)
    print*, " * running e-e+ --> e-e+"
  elseif(which_piece(1:5) == "ee2mm") then
    call initflavour("mu-e", 10.e3**2)
    print*, " * running e-e+ --> mu-mu+"
  elseif(which_piece(1:5) == "ee2uu") then
    call initflavour("pi-e", 10.e3**2)
    print*, " * running e-e+ --> pi-pi+"
  endif

  read*, hvp
  write(filenamesuffix,'(I1)') hvp
  if(hvp.eq.1) then
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, " * only hadronic vp for fermionic pieces"
  elseif(hvp.eq.2) then
    nhad = 0._prec
    nel  = 1._prec
    nmu  = 1._prec
    ntau = 1._prec
    print*, " * only leptonic vp for fermionic pieces"
  elseif(hvp.eq.3) then
    HVPmodel = 'nsk-2.9'
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, " * only nsk hadronic vp for fermionic pieces"
  endif
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real(kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real(kind=prec) :: quant(nr_q)
  real(kind=prec) :: thm, phim, pm
  real(kind=prec) :: thp, phip, pp
  real(kind=prec) :: xi, thav, dphi
  real(kind=prec) :: mxx, ptildegamma(4)
  real(kind=prec) :: qs(4), qh(4), ths, thh, es, eh
  real(kind=prec) :: qhtag(4), eg, thg, mxxg, thgg
  logical :: hardin1, softin1

  !! ==== keep the line below in any case ==== !!
  call fix_mu

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)
  pol2 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.

  thm  = acos(cos_th(q1, q3))
  phim = phi(q3)
  thp  = acos(cos_th(q1, q4))
  phip = phi(q4)

  xi   = abs(thp+thm-pi)
  thav = (thm-thp+pi)/2
  dphi = abs(abs(phip-phim)-pi)

  pm = absvec(q3)
  pp = absvec(q4)

  mxx = sqrt(sq(q3+q4))
  ptildegamma = -(q3+q4)

  if(0.65 > thm .or. thm > 2.75) pass_cut = .false.
  if(0.65 > thp .or. thp > 2.75) pass_cut = .false.
  if(pm < 1000) pass_cut = .false.
  if(pp < 1000) pass_cut = .false.

  call get_sh_g(q5, q6, ths, thh, Es, Eh, qs, qh) !ths/thh in rad

  hardin1 = indetector(0.6, 2.7, 3000., thh, eh)
  softin1 = indetector(0.6, 2.7, 3000., ths, es)

  if(.not.(hardin1.or.softin1)) pass_cut = .false.

  if(hardin1) then
    qhtag = qh
  elseif(softin1) then
    qhtag = qs
  endif

  eg   = qhtag(4)
  thg  = acos(cos_th(q1, qhtag))
  mxxg = sqrt(sq(q3+q4+qhtag))
  thgg = acos(cos_th(ptildegamma,qhtag))

  if(mxxg < 8000) pass_cut = .false.
  if(thgg > 0.3) pass_cut = .false.

  if(which_piece(1:5) == "eb2eb") then
    if(mxx < 300) pass_cut = .false.
  endif

  names(1) = "th+"
  quant(1) = thp
  names(2) = "th-"
  quant(2) = thm
  names(3) = "p+"
  quant(3) = pp
  names(4) = "p-"
  quant(4) = pm
  names(5) = "mxx"
  quant(5) = mxx
  names(6) = "xi"
  quant(6) = xi/pi
  names(7) = "dphi"
  quant(7) = dphi/pi
  names(8) = "thav"
  quant(8) = thav
  names(9) = "mxxg"
  quant(9) = mxxg
  names(10) = "eg"
  quant(10) = eg
  names(11) = "thg"
  quant(11) = thg
  names(12) = "mxxrw"
  quant(12) = mxx
  names(13) = "thgg"
  quant(13) = thgg

  END FUNCTION QUANT

  SUBROUTINE GET_SH_G(q5, q6, ths, thh, Es, Eh, qs, qh)
  implicit none
  real(kind=prec), parameter :: ez(4) = (/ 0., 0., 1._prec, 0._prec/)
  real(kind=prec), intent(in) :: q5(4), q6(4)
  real(kind=prec), intent(out) :: ths, thh, Es, Eh, qs(4), qh(4)
  real(kind=prec) :: e5, e6, th5, th6

  e5 = q5(4)
  e6 = q6(4)

  th5 = acos(cos_th(ez,q5))
  th6 = acos(cos_th(ez,q6))

  if(e5 < e6) then
    ths = th5
    es = e5
    qs = q5
    thh = th6
    eh = e6
    qh = q6
  else
    ths = th6
    es = e6
    qs = q6
    thh = th5
    eh = e5
    qh = q5
  end if

  END SUBROUTINE GET_SH_G

  FUNCTION INDETECTOR(d_thl,d_thu,d_eg, th,eg)
  implicit none
  real(kind=prec), intent(in) :: d_thl,d_thu,d_eg
  real(kind=prec), intent(in) :: th, eg
  logical :: indetector

  indetector = .false.
  if((d_thl < th .and. th < d_thu) .and. eg > d_eg) indetector = .true.
  END FUNCTION INDETECTOR

  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
