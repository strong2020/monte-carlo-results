c ========================================================================
c Histograms for dxs/variables for B scenario in Strong2020. 
c ------------------------------------------------------------------------
      subroutine output_variables(n, value, i)
       ! n refers to a process with 0, 1 (n=0) or 2 (n=1) photons
       ! value is the weight for the MC histograms
       ! i is 0 (1) for the (un)weighted histograms
      include 'phokhara_10.0.inc'
      logical g1_pass_cuts, g2_pass_cuts
      integer i, n, g1_cuts, g2_cuts, k
      real*8 value, pp, pm, eg, thp, thm, thg1, thavg, dphi, xi, thg
      real*8 mxx, mxxg, s, phip, phim, thg2, g1_energy, g2_energy
      real*8 thgb, pgbar(1:3), thgg
      real*8, allocatable :: variables(:)
      allocate(variables(1:total_num_hist))

      ! Calculate variables of interest
      s = momenta(3,0) + momenta(4,0) + momenta(6,0) + momenta(7,0)
      
      pp = dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2)*1000
      pm = dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2)*1000
      thp = dacos(-momenta(6,3)*1000/pp)
      thm = dacos(-momenta(7,3)*1000/pm)
      thavg = (-thp+thm+pi)/2
      xi = ABS(thp+thm-pi)
      phip = atan2(momenta(6,2), momenta(6,1))
      phim = atan2(momenta(7,2), momenta(7,1))
      dphi = abs(abs(phip-phim) - pi)
      mxx =dSqrt((momenta(6,0) + momenta(7,0))**2 -
     &       (momenta(6,1) + momenta(7,1))**2 - 
     &       (momenta(6,2) + momenta(7,2))**2 - 
     &       (momenta(6,3) + momenta(7,3))**2) * 1000

      thg1 = dacos(-momenta(3,3)/
     &         dSqrt(momenta(3,1)**2+momenta(3,2)**2+momenta(3,3)**2))
      thg2 = dacos(-momenta(4,3)/
     &         dSqrt(momenta(4,1)**2+momenta(4,2)**2+momenta(4,3)**2))

      if (thg1 /= thg1) thg1 = 0  ! If it is a nan, set to zero (won't pass the cuts)
      if (thg2 /= thg2) thg2 = 0  ! If it is a nan, set to zero (won't pass the cuts)

      g1_pass_cuts = (momenta(3,0).gt.3.and.
     &                (thg1.ge.0.6.and.thg1.le.2.7))
      g2_pass_cuts = (momenta(4,0).gt.3.and.
     6                (thg2.ge.0.6.and.thg2.le.2.7))
    
      g1_cuts = g1_pass_cuts
      g2_cuts = g2_pass_cuts
      g1_energy = g1_cuts * momenta(3,0)
      g2_energy = g2_cuts * momenta(4,0)

      if (g1_energy.ge.g2_energy) then
       eg = g1_energy * 1000
       thg = thg1
       mxxg = dSqrt((momenta(6,0) + momenta(7,0) + momenta(3,0))**2 -
     &       (momenta(6,1) + momenta(7,1) + momenta(3,1))**2 - 
     &       (momenta(6,2) + momenta(7,2) + momenta(3,2))**2 - 
     &       (momenta(6,3) + momenta(7,3) + momenta(3,3))**2) * 1000

      else
       eg = g2_energy * 1000
       thg = thg2
       mxxg = dSqrt((momenta(6,0) + momenta(7,0) + momenta(4,0))**2 -
     &       (momenta(6,1) + momenta(7,1) + momenta(4,1))**2 - 
     &       (momenta(6,2) + momenta(7,2) + momenta(4,2))**2 - 
     &       (momenta(6,3) + momenta(7,3) + momenta(4,3))**2) * 1000
      end if

      pgbar(1) = -momenta(6,1) - momenta(7,1)
      pgbar(2) = -momenta(6,2) - momenta(7,2)
      pgbar(3) = -momenta(6,3) - momenta(7,3)
      thgb = dacos(-pgbar(3)/
     &         dSqrt(pgbar(1)**2+pgbar(2)**2+pgbar(3)**2))

      thgg = dabs(thgb-thg)

      ! Save to variables in the same order as the requested histograms
      variables = (/pp, thp, pm, thm, eg, thg, thavg, dphi/pi, xi/pi,  
     &             mxx, mxx, mxxg, thgg/)

      ! Save to the corersponding histograms
      call distribute_variables(variables, i, n, value)
      deallocate(variables)

      return
      end

c ========================================================================
c --- Experimental cuts for B scenario Strong2020
c ------------------------------------------------------------------------
      subroutine advanced_testcuts(accepted)
      include 'phokhara_10.0.inc'
      logical accepted, g1_pass_cuts, g2_pass_cuts
      integer g1_cuts, g2_cuts
      real*8 thp, thm, xi, thg1, thg2, g1_energy, g2_energy
      real*8 mxxg, s, pp, pm, pgbar(1:3), thgh, thgb, thgg
      

      s = momenta(3,0) + momenta(4,0) + momenta(6,0) + momenta(7,0)
      pp = dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2)
      pm = dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2)

      ! Cut on th+
      thp = dacos(-momenta(6,3)/
     &         dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2))
      accepted = (accepted.and.(thp.gt.0.65).and.(thp.lt.2.75).and.
     &           (pp.gt.1))
      if (.not.accepted) return

      ! Cut on th-
      thm = dacos(-momenta(7,3)/
     &         dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2))
      accepted = (accepted.and.(thm.gt.0.65).and.(thm.lt.2.75).and.
     &           (pm.gt.1))
      if (.not.accepted) return

      ! Cuts on theta of the photons
      thg1 = dacos(-momenta(3,3)/
     &         dSqrt(momenta(3,1)**2+momenta(3,2)**2+momenta(3,3)**2))
      thg2 = dacos(-momenta(4,3)/
     &         dSqrt(momenta(4,1)**2+momenta(4,2)**2+momenta(4,3)**2))

      if (thg1 /= thg1) thg1 = 0  ! If it is a nan, set to zero (won't pass the cuts)
      if (thg2 /= thg2) thg2 = 0  ! If it is a nan, set to zero (won't pass the cuts)

      g1_pass_cuts = (momenta(3,0).gt.3.and.
     &                (thg1.ge.0.6.and.thg1.le.2.7))
      g2_pass_cuts = (momenta(4,0).gt.3.and.
     6                (thg2.ge.0.6.and.thg2.le.2.7))
      
      accepted = (accepted.and.(g1_pass_cuts.or.g2_pass_cuts))
      if (.not.accepted) return

      ! Cut on mxxg
      g1_cuts = g1_pass_cuts
      g2_cuts = g2_pass_cuts
      g1_energy = g1_cuts * momenta(3,0)
      g2_energy = g2_cuts * momenta(4,0)

      if (g1_energy.ge.g2_energy) then
      mxxg = dSqrt((momenta(6,0) + momenta(7,0) + momenta(3,0))**2 -
     &       (momenta(6,1) + momenta(7,1) + momenta(3,1))**2 - 
     &       (momenta(6,2) + momenta(7,2) + momenta(3,2))**2 - 
     &       (momenta(6,3) + momenta(7,3) + momenta(3,3))**2)
      thgh = thg1
      else
       mxxg = dSqrt((momenta(6,0) + momenta(7,0) + momenta(4,0))**2 -
     &       (momenta(6,1) + momenta(7,1) + momenta(4,1))**2 - 
     &       (momenta(6,2) + momenta(7,2) + momenta(4,2))**2 - 
     &       (momenta(6,3) + momenta(7,3) + momenta(4,3))**2)
      thgh = thg2
      end if
      
      accepted = (accepted.and.(mxxg.gt.8))

      pgbar(1) = -momenta(6,1) - momenta(7,1)
      pgbar(2) = -momenta(6,2) - momenta(7,2)
      pgbar(3) = -momenta(6,3) - momenta(7,3)
      thgb = dacos(-pgbar(3)/
     &         dSqrt(pgbar(1)**2+pgbar(2)**2+pgbar(3)**2))

      thgg = dabs(thgb-thgh)

      accepted = (accepted.and.(thgg.lt.0.3))
      end 