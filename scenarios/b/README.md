# B physics scenario

This is a mixture of Babar and Belle

 * Process: $`ee\to ee,\mu\mu,\pi\pi + \gamma`$

 * Energy: $`\sqrt s=10\,\mathrm{GeV}`$

## Variables

We define
 * $`\theta_{\rm avg} = (\theta^- - \theta^+ + \pi)/2`$
 * $`p_\pm = |\vec p_\pm|`$
 * $`\delta\phi = ||\phi^+-\phi^-|-\pi|`$
 * $`\xi = |\theta^++\theta^--\pi|`$
 * $`M_{XX} = \sqrt{(p_+ + p_-)^2}`$
 * $`M_{XX\gamma} = \sqrt{(p_+ + p_-+p_{\gamma^{(h)}})^2}`$
 * $`\vec{p}_{\widetilde\gamma} = - (\vec{p_+} + \vec{p_-})`$
 * $`\theta_{\gamma^{(h)},\widetilde\gamma} = \sphericalangle(\vec{p}_{\gamma^{(h)}},\vec{p}_{\widetilde\gamma})`$

$`p_{\gamma^{(h)}}`$ corresponds to the hardest photon, i.e. the one with the largest energy among those passing angle and energy cuts.

The scattering angles are defined with respect to the electron momentum
```math
\cos \theta^{\pm} = \frac{p^{\pm} \cdot p_{e^-}}{|p^{\pm}|\,|p_{e^-}|}
```

## Cuts

We enforce cuts
 * $`0.65\,{\rm rad} < \theta_\pm < 2.75\,{\rm rad}`$ $\land$ $`p_\pm > 1\,{\rm GeV}`$
 * $`0.6 \,{\rm rad} < \theta_\gamma < 2.7\,{\rm rad}`$ $\land$ $`E_\gamma > 3\,{\rm GeV}`$
 * $`\theta_{\gamma^{(h)},\widetilde\gamma} < 0.3\,{\rm rad}`$
 * $`M_{XX\gamma} > 8\,{\rm GeV}`$
 * $`M_{ee} > 0.3\,{\rm GeV}`$

## Histograms

Uniform binning is achieved with 600 bins

| variable                                   | name    | lower  | upper | bins |  | lower  | upper | selection |
| ------------------------------------------ | ------- | ------ | ----- | ---- |--| ------ | ----- | --------- |
| $`p_+`$                                    | `p+`    | 1000   | 5000  | 100  |  | 1000   | 5000  | `::6`     |
| $`\theta_+`$                               | `th+`   | 0.65   | 2.75  | 42   |  | 0.25   | 2.75  | `96::12`   |
| $`p_-`$                                    | `p-`    | 1000   | 5000  | 100  |  | 1000   | 5000  | `::6`     |
| $`\theta_-`$                               | `th-`   | 0.65   | 2.75  | 42   |  | 0.25   | 2.75  | `96::12`   |
| $`E_{\gamma^{(h)}}`$                       | `eg`    | 3000   | 5000  | 100  |  | 3000   | 5000  | `::6`     |
| $`\theta_{\gamma^{(h)}}`$                  | `thg`   | 0.6    | 2.7   | 42   |  | 0.2    | 2.7   | `96::12`   |
| $`\theta_{\rm avg}`$                       | `thav`  | 0.52   | 2.63  | 50   |  | 0.52   | 2.63  | `::12`    |
| $`\delta\phi/\pi`$                         | `dphi`  | 0      | 1     | 100  |  | 0      | 1     | `::6`     |
| $`\xi/\pi`$                                | `xi`    | 0      | 1     | 100  |  | 0      | 1     | `::6`     |
| $`M_{XX}`$                                 | `mxx`   | 0      | 6400  | 160  |  | 0      | 8000  | `:480:3` |
| $`M_{XX}`$ (optional)                      | `mxxrw` | 0      | 2400  | 600  |  | 0      | 2400  |           |
| $`M_{XX\gamma}`$                           | `mxxg`  | 8000   | 10000 | 50   |  | 8000   | 10000 | `::12`    |
| $`\theta_{\gamma^{(h)},\widetilde\gamma}`$ | `thgg`  | 0      | 0.3   | 60   |  | 0      | 0.3   |  `::10`   |
