#SBATCH --partition=<partition>
#SBATCH --time=<time limit>
#SBATCH --ntasks=<number of jobs>
#SBATCH --clusters=merlin6
#SBATCH --output=slurm-%j.out

pymule batch slurm <menufile.menu>
