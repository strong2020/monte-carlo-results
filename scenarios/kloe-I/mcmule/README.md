# McMule for the KLOE-I scenario

McMule uses menu files to auto-generate runcards for each job.
These can be found in `bhabha-<bosonic/fermionic>/menu-eb2eb.menu`, `muons-<bosonic/fermionic>/menu-ee2mm.menu`, and `pions-<bosonic/fermionic>/menu-ee2uu.menu`.
The measurement function is defined in `user.f95`.
To run McMule,
```shell
# compile measurement function
$ gfortran -fPIC --shared -o user.so -I ../../../codes/mcmule/build/src/libmcmule.a.p -fdefault-real-8 ../user.f95
$ cp user.so bhabha-<bosonic/fermionic>/ ; cp user.so muons-<bosonic/fermionic>/ ; cp user.so pions-<bosonic/fermionic>/
```
For the compilation above, we have used three versions of McMule:
* `54e1401` on branch `devel` for Bhabha scattering (bosonic)
* `0f34672` on branch `moller` for Bhabha scattering (fermionic)
* `abb0e91` on branch `muone` for mu-pair production
* `4d06b5b` on branch `pion-gstar` for pion-pair production
```shell
# run code
$ nohup pymule batch shell ./bhabha-bosonic/menu-eb2eb.menu &    # you may also use sbatch ./bhabha-bosonic/submit-bhabha-b.sh
$ nohup pymule batch shell ./bhabha-fermionic/menu-eb2eb.menu &  # you may also use sbatch ./bhabha-fermionic/submit-bhabha-f.sh
$ nohup pymule batch shell ./muons-bosonic/menu-ee2mm.menu &     # you may also use sbatch ./muons-bosonic/submit-muons-b.sh
$ nohup pymule batch shell ./muons-fermionic/menu-ee2mm.menu &   # you may also use sbatch ./muons-fermionic/submit-muons-f.sh
$ nohup pymule batch shell ./pions-bosonic/menu-ee2uu.menu &     # you may also use sbatch ./submit.sh
$ nohup pymule batch shell ./pions-fermionic/menu-ee2uu.menu &   # you may also use sbatch ./submit.sh
# create tar balls
$ ( cd bhabha-bosonic/   ; tar cjvf out.tar.bz2 out ; tar czvf worker.tar.gz worker-* ; )
$ ( cd bhabha-fermionic/ ; tar cjvf out.tar.bz2 out ; tar czvf worker.tar.gz worker-* ; )
$ ( cd muons-bosonic/    ; tar cjvf out.tar.bz2 out ; tar czvf worker.tar.gz worker-* ; )
$ ( cd muons-fermionic/  ; tar cjvf out.tar.bz2 out ; tar czvf worker.tar.gz worker-* ; )
$ ( cd pions-bosonic/    ; tar cjvf out.tar.bz2 out ; tar czvf worker.tar.gz worker-* ; )
$ ( cd pions-fermionic/  ; tar cjvf out.tar.bz2 out ; tar czvf worker.tar.gz worker-* ; )
$ python analyse.py
```

# Contributions

We include
* LO
* NLO (complete, including HVP and leptonic VP)
* NLOL (including leptonic VP)
