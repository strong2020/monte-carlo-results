#!/bin/bash
scenario="KLOE-I"
finals=("Muons")
n_cores=250
cd ../../PHOKARA_10.0/
cp ../Strong2020/new"$scenario"/variables_cuts_"$scenario".f variables_cuts.f
make phokhara
cd ../Strong2020/new"$scenario"/

for final in "${finals[@]}"; do
    if [ "$final" == "Muons" ] ; then
     orders=("LO" "NLOnovp" "NLO")
    fi
    if [ $final == "Pions" ] ; then
     orders=("LO" "LOISR" "NLO")
    fi
    for order in "${orders[@]}"; do
        # for core in $(seq 1 "$n_cores"); do
        #     mkdir "$final"_"$order"_"$core"
        #     cd "$final"_"$order"_"$core"
        #     cp ../../../PHOKARA_10.0/const_and_model_paramall10.0.dat .
        #     cp ../../../PHOKARA_10.0/vpol_all_bare_sum_v2.9.dat .
        #     cp ../../../PHOKARA_10.0/vpol_bare_lept_v2.9.dat .
        #     sed "2,1s/^/$core\\n/" ../"$final"/Inputs/input_"$order".dat > input_10.0.dat
        #     sbatch ../run_phok.sh
        #     cd ..
        # done
        wait
        echo "Starting python analysis"
        python3 analyser_core.py "$n_cores" "$final" "$order"
        for core in $(seq 1 "$n_cores"); do
            rm -r "$final"_"$order"_"$core"
        done
    done
done