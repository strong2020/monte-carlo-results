# KLOE scenario I (large angle / tagged)

 * Process: $`ee\to ee,\mu\mu,\pi\pi + \gamma`$

 * Energy: $`\sqrt s=1.02\,\mathrm{GeV}`$
## Variables

We define

 * $`\theta_{\rm avg} = (\theta^- - \theta^+ + \pi)/2`$
 * $`p_\pm = |\vec p_\pm|`$
 * $`\delta\phi = ||\phi^+-\phi^-|-\pi|`$
 * $`\xi = |\theta^++\theta^--\pi|`$
 * $`M_{XX} = \sqrt{(p_+ + p_-)^2}`$
 * $`M_{XX\gamma} = \sqrt{(p_+ + p_-+p_{\gamma^{(h)}})^2}`$
 * $`\left(\sqrt{s}-\sqrt{p^2_+ + M_\text{trk}^2} - \sqrt{p^2_-+M_\text{trk}^2}\right)^2 - (\vec{p}_++\vec{p}_-)^2 \equiv 0`$ (optional)

$`p_{\gamma^{(h)}}`$ corresponds to the hardest photon, i.e. the one with the largest energy among those passing angle and energy cuts.

The track mass can be solved as
```math
M_{\rm trk}^2 = \frac{a}{4} - \frac{p_-^2 + p_+^2}{2} + \frac{\big(p_+^2-p_-^2\big)^2}{4a}
\quad\text{with}\quad
a = \big(\sqrt{s}-|\vec{p_-}+\vec{p_+}|\big)^2
```

The scattering angles are defined with respect to the electron momentum
```math
\cos \theta^{\pm} = \frac{p^{\pm} \cdot p_{e^-}}{|p^{\pm}|\,|p_{e^-}|}
```

## Cuts

We enforce cuts
 * $`50^\circ \le \theta_{\pm} \le 130^\circ`$
 * $`|p^z_{\pm}|> 90\,\mathrm{MeV}`$ $\lor$ $`|p^\bot_{\pm}|> 160\,\mathrm{MeV}`$
* $`50^\circ \le \theta_\gamma \le 130^\circ`$ $\land$ $`E_\gamma > 20\,\mathrm{MeV}`$
* $`0.1 \,\mathrm{GeV}^2 \le M^2_{XX} \le 0.85\,\mathrm{GeV}^2`$

## Histograms

Uniform binning is achieved with 600 bins

| variable                            | name    | lower | upper | bins |   | lower | upper | selection   |
| ----------------------------------- | ------- | ----- | ----- | ---- | - | ----- | ----- | ----------- |
| $`\theta^+/{\rm deg}`$              | `lth+`  | 50    | 130   | 80   |   | 30    | 130   | `120::6`    |
| $`\theta^-/{\rm deg}`$              | `lth-`  | 50    | 130   | 80   |   | 30    | 130   | `120::6`    |
| $`p_+`$                             | `lp+`   | 140   | 515   | 75   |   | 0     | 600   | `140:515:5` |
| $`p_-`$                             | `lp-`   | 140   | 515   | 75   |   | 0     | 600   | `140:515:5` |
| $`\vert p^z_+ \vert`$                           | `lpz+`  | 0     | 330   | 66   |   | 0     | 600   | `:330:5`    |
| $`\vert p^z_- \vert`$                           | `lpz-`  | 0     | 330   | 66   |   | 0     | 600   | `:330:5`    |
| $`p^\bot_+`$                        | `lpp+`  | 106   | 518.5 | 75   |   | 7     | 557   | `108:558:6` |
| $`p^\bot_-`$                        | `lpp-`  | 106   | 518.5 | 75   |   | 7     | 557   | `108:558:6` |
| $`M_{XX}`$                          | `lmxx`  | 316   | 922   | 60   |   | 316   | 922   | `::10`      |
| $`\xi/{\rm deg}`$                   | `lxi`   | 0     | 80    | 40   |   | 0     | 80    | `::15`      |
| $`\delta \phi/{\rm deg}`$           | `ldphi` | 0     | 180   | 60   |   | 0     | 180   | `::10`      |
| $`\theta_{\rm avg}/{\rm deg}`$      | `lthav` | 50    | 130   | 80   |   | 30    | 130   | `120::6`    |
| $`\theta_{\gamma^{(h)}}/{\rm deg}`$ | `lthg`  | 50    | 130   | 80   |   | 30    | 130   | `120::6`    |
| $`E_{\gamma^{(h)}}`$                | `leg`   | 20    | 465   | 89   |   | 0     | 500   | `24:558:6`  |
| $`M_{XX\gamma}`$                    | `lmxxg` | 270   | 1020  | 75   |   | 0     | 2000  | `81:306:3`  |
| $`M_{\text{trk}}`$                  | `lmtrk` | 0     | 500   | 50   |   | 0     | 500   | `::12`      |
| $`M_{XX}`$ if # 1                   | `lmxxc` | 316   | 922   | 60   |   | 316   | 922   | `::10`      |
| $`M_{\text{trk}}`$ if # 2           | `lmtrkc`| 0     | 500   | 50   |   | 0     | 500   | `::12`      |

For # 1 we only consider the event if
 * $`X = e  `$: $`130 {\rm MeV} < M_{\rm trk} < 220 {\rm MeV}`$
 * $`X = \mu`$: $`80   {\rm MeV} < M_{\rm trk} < 115 {\rm MeV}`$
 * $`X = \pi`$: $`130  {\rm MeV} < M_{\rm trk} < 220 {\rm MeV}`$

For # 2, only if $`0.5{\rm GeV}^2 < M_{XX}^2 < 0.7{\rm GeV}^2`$
