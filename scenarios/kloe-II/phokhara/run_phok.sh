#!/bin/bash

echo "Running PHOKHARA"&>dummy.out
../../../PHOKARA_10.0/phokhara &> dummy.out
rm input*
rm slurm*
rm vpol*
rm const*