# KLOE scenario II (small angle / untagged)

 * Process: $`ee\to ee,\mu\mu,\pi\pi + \gamma`$

 * Energy: $`\sqrt s=1.02\,\mathrm{GeV}`$
## Variables

We define

 * $`\theta_{\rm avg} = (\theta^- - \theta^+ + \pi)/2`$
 * $`p_\pm = |\vec p_\pm|`$
 * $`\delta\phi = ||\phi^+-\phi^-|-\pi|`$
 * $`\xi = |\theta^++\theta^--\pi|`$
 * $`M_{XX} = \sqrt{(p_+ + p_-)^2}`$
 * $`\vec{p}_{\widetilde\gamma} = - (\vec{p_+} + \vec{p_-})`$
 * $`\left(\sqrt{s}-\sqrt{p^2_+ + M_\text{trk}^2} - \sqrt{p^2_-+M_\text{trk}^2}\right)^2 - (\vec{p}_++\vec{p}_-)^2 \equiv 0`$ (optional)

$`p_{\gamma^{(h)}}`$ corresponds to the hardest photon, i.e. the one with the largest energy among those passing angle and energy cuts.

The track mass can be solved as
```math
M_{\rm trk}^2 = \frac{a}{4} - \frac{p_-^2 + p_+^2}{2} + \frac{\big(p_+^2-p_-^2\big)^2}{4a}
\quad\text{with}\quad
a = \big(\sqrt{s}-|\vec{p_-}+\vec{p_+}|\big)^2
```

The scattering angles are defined with respect to the electron momentum
```math
\cos \theta^{\pm} = \frac{p^{\pm} \cdot p_{e^-}}{|p^{\pm}|\,|p_{e^-}|}
```

## Cuts

We enforce cuts
 * $`50^\circ \le \theta_{\pm} \le 130^\circ`$
 * $`|p^z_{\pm}|> 90\,\mathrm{MeV}`$ $\lor$ $`|p^\bot_{\pm}|> 160\,\mathrm{MeV}`$
* $`\theta_{\widetilde\gamma} \le 15^\circ`$ $\lor$ $`\theta_{\widetilde\gamma} > 165^\circ`$
* $`0.35 \,\mathrm{GeV}^2 \le M^2_{XX} \le 0.95\,\mathrm{GeV}^2`$

## Histograms

Uniform binning is achieved with 600 bins

For # 1 we only consider the event if
 * $`X = e  `$: $`130  {\rm MeV} < M_{\rm trk} < 220 {\rm MeV}`$
 * $`X = \mu`$: $`80   {\rm MeV} < M_{\rm trk} < 115 {\rm MeV}`$
 * $`X = \pi`$: $`130  {\rm MeV} < M_{\rm trk} < 220 {\rm MeV}`$

For # 2, only if $`0.5{\rm GeV}^2 < M_{XX}^2 < 0.7{\rm GeV}^2`$

| variable                      |  name    | lower | upper | bins |   | lower | upper | selection   |
| ----------------------------- | -------- | ------| ----- | ---- | - | ----- | ----- | ----------- |
| $`\theta^+/{\rm deg}`$        | `sth+`   | 50    | 130   | 80   |   | 30    | 130   | `120::6`    |
| $`\theta^-/{\rm deg}`$        | `sth-`   | 50    | 130   | 80   |   | 30    | 130   | `120::6`    |
| $`p_+`$                       | `sp+`    | 215   | 515   | 60   |   | 215   | 515   | `::10`      |
| $`p_-`$                       | `sp-`    | 215   | 515   | 60   |   | 215   | 515   | `::10`      |
| $`\vert p^z_+ \vert`$         | `spz+`   | 0     | 375   | 75   |   | 0     | 500   | `:450:6`    |
| $`\vert p^z_-\vert`$          | `spz-`   | 0     | 375   | 75   |   | 0     | 500   | `:450:6`    |
| $`p^\bot_+`$                  | `spp+`   | 200   | 500   | 60   |   | 200   | 500   | `::10`      |
| $`p^\bot_-`$                  | `spp-`   | 200   | 500   | 60   |   | 200   | 500   | `::10`      |
| $`M_{XX}`$                    | `smxx`   | 591   | 975   | 50   |   | 591   | 975   | `::12`      |
| $`M_{\text{trk}}`$            | `smtrk`  | 0     | 500   | 50   |   | 0     | 500   | `::12`      |
| $`\xi/{\rm deg}`$             | `sxi`    | 0     | 60    | 60   |   | 0     | 60    | `::10`      |
| $`\delta \phi/{\rm deg}`$     | `sdphi`  | 0     | 25    | 50   |   | 0     | 25    | `::12`      |
| $`\theta_{\rm avg}/{\rm deg}`$         | `sthav`  | 50  | 130 | 80 | | 30   | 130   | `120::6`    |
| $`\theta_{\widetilde\gamma}/{\rm deg}`$| `sthtgl` | 165 | 180 | 60 | | 165  | 180   | `::10`      |
| $`\theta_{\widetilde\gamma}/{\rm deg}`$| `sthtgs` | 0   | 15  | 60 | | 0    | 15    | `::10`      |
| $`M_{XX}`$ if # 1                      | `smxxc`  | 591 | 975 | 50 | | 591  | 975   | `::12`      |
| $`M_{\text{trk}}`$ if # 2              | `smtrkc` | 0   | 500 | 50 | | 0    | 500   | `::12`      |
