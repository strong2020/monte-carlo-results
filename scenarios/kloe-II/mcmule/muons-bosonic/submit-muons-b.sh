#!/bin/bash
#SBATCH --partition=general
#SBATCH --time=7-00:00:00
#SBATCH --ntasks=2
#SBATCH --mem-per-cpu=120M
#SBATCH --hint=nomultithread
#SBATCH --clusters=merlin6
#SBATCH --output=slurm-%j.out

source /data/project/general/lepton-nnlo/strong2020/pymule-batch/bin/activate
pymule batch slurm menu-ee2mm.menu
