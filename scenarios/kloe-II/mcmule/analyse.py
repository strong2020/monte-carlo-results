from pymule import *

loConv=1000*conv*alpha**3
nloConv=1000*conv*alpha**4

lower = {
    'sth+':  21,
    'sth-':  21,
    'sp+':    1,
    'sp-':    1,
    'spz+':   1,
    'spz-':   1,
    'spp+':   1,
    'spp-':   1,
    'smxx':   1,
    'smtrk':  1,
    'sxi':    1,
    'sdphi':  1,
    'sthav': 21,
    'sthtgl': 1,
    'sthtgs': 1,
    'smxxc':  1,
    'smtrkc': 1,
}
upper = {
    'sth+':   -1,
    'sth-':   -1,
    'sp+':    -1,
    'sp-':    -1,
    'spz+':   76,
    'spz-':   76,
    'spp+':   -1,
    'spp-':   -1,
    'smxx':   -1,
    'smtrk':  -1,
    'sxi':    -1,
    'sdphi':  -1,
    'sthav':  -1,
    'sthtgl': -1,
    'sthtgs': -1,
    'smxxc':  -1,
    'smtrkc': -1,
}
setup(merge={
    'sth+':    6,
    'sth-':    6,
    'sp+':    10,
    'sp-':    10,
    'spz+':    6,
    'spz-':    6,
    'spp+':   10,
    'spp-':   10,
    'smxx':   12,
    'smtrk':  12,
    'sxi':    10,
    'sdphi':  12,
    'sthav':   6,
    'sthtgl': 10,
    'sthtgs': 10,
    'smxxc':  12,
    'smtrkc': 12,})

lo      = {}
loISC   = {}
dnloB   = {}
dnloBISC = {}
dnloFh  = {}
dnloFhISC = {}
dnloFl  = {}
dnloFlISC = {}
nlo     = {}
nloISC = {}
nlol   = {}
nlob   = {}

# Bhabha
setup(folder="bhabha-bosonic/out.tar.bz2")
setup(obs="0")
lo['ee']    = mergefks(sigma('eb2ebR')) * loConv
loISC['ee'] = lo['ee']
dnloB['ee'] = mergefks(sigma('eb2ebRF'),
                       sigma('eb2ebRR15162526'), sigma('eb2ebRR3536'), sigma('eb2ebRR4546')) * nloConv
dnloBISC['ee'] = dnloB['ee']
setup(folder="bhabha-fermionic/out.tar.bz2")
setup(obs="3")
dnloFh['ee'] = mergefks(sigma('eb2ebAR')) * nloConv
dnloFhISC['ee'] = dnloFh['ee']
setup(obs="2")
dnloFl['ee'] = mergefks(sigma('eb2ebAR')) * nloConv
dnloFlISC['ee'] = dnloFl['ee']

nlo['ee']   = lo['ee'] + dnloB['ee'] + dnloFh['ee'] + dnloFl['ee']
nloISC['ee'] = nlo['ee']
nlol['ee']  = lo['ee'] + dnloB['ee'] + dnloFl['ee']
nlob['ee']  = lo['ee'] + dnloB['ee']

# Mupair
setup(folder="muons-bosonic/out.tar.bz2")
setup(obs="0")
lo['mm']    = mergefks(sigma('ee2mmREE'), sigma('ee2mmREM'), sigma('ee2mmRMM')) * loConv
loISC['mm'] = mergefks(sigma('ee2mmREE')) * loConv
dnloB['mm'] = mergefks(sigma('ee2mmRFEEEE'), sigma('ee2mmRFMIXD'),  sigma('ee2mmRFMMMM'),
                       sigma('ee2mmRREEEE'), sigma('ee2mmRRMIXD'),  sigma('ee2mmRRMMMM')) * nloConv
dnloBISC['mm'] = mergefks(sigma('ee2mmRFEEEE'), sigma('ee2mmRREEEE')) * nloConv
setup(folder="muons-fermionic/out.tar.bz2")
setup(obs="3")
dnloFh['mm'] = mergefks(sigma('ee2mmAREE'), sigma('ee2mmAREM'), sigma('ee2mmARMM')) * nloConv
dnloFhISC['mm'] = mergefks(sigma('ee2mmAREE')) * nloConv
setup(obs="2")
dnloFl['mm'] = mergefks(sigma('ee2mmAREE'), sigma('ee2mmAREM'), sigma('ee2mmARMM')) * nloConv
dnloFlISC['mm'] = mergefks(sigma('ee2mmAREE')) * nloConv

nlo['mm']   = lo['mm'] + dnloB['mm'] + dnloFh['mm'] + dnloFl['mm']
nloISC['mm'] = loISC['mm'] + dnloBISC['mm'] + dnloFhISC['mm'] + dnloFlISC['mm']
nlol['mm']  = lo['mm'] + dnloB['mm'] + dnloFl['mm']
nlob['mm']  = lo['mm'] + dnloB['mm']

# Pions
setup(folder="pions-bosonic/out.tar.bz2")
setup(obs="0")
lo['uu']    = mergefks(sigma('ee2uuREE'),
                       sigma('ee2uuREU'),
                       sigma('ee2uuRUU')) * loConv
loISC['uu'] = mergefks(sigma('ee2uuREE')) * loConv
dnloB['uu'] = mergefks(sigma('ee2uuRFEEEE'), sigma('ee2uuRREEEE')) * nloConv

nlo['uu']   = lo['uu'] + dnloB['uu']
nloISC['uu'] = loISC['uu'] + dnloB['uu']
nlol['uu']  = nlo['uu']
nlob['uu']  = nlo['uu']

# Export
for k in lo.keys():
    print(f"CHANNEL {k}: run-time = {nlo[k].time:.2f}s")
    print(f"LO xsec = {lo[k].value[0]} +- {lo[k].value[1]} nb")
    print(f"LOISC xsec = {loISC[k].value[0]} +- {loISC[k].value[1]} nb")
    print(f"NLO xsec  = {nlo[k].value[0]} +- {nlo[k].value[1]} nb")
    print(f"NLOL xsec  = {nlol[k].value[0]} +- {nlol[k].value[1]} nb")
    print(f"NLOB xsec  = {nlob[k].value[0]} +- {nlob[k].value[1]} nb")
    for h in lo[k].histograms:
        w = lo[k][h][2,0] - lo[k][h][1,0]
        np.savetxt(f'data/{k}-{h}.csv', np.column_stack([
            lo [k][h][lower[h]:upper[h],0] - w/2,
            lo [k][h][lower[h]:upper[h],0] + w/2,
            lo [k][h][lower[h]:upper[h],1],
            lo [k][h][lower[h]:upper[h],2],
            loISC [k][h][lower[h]:upper[h],1],
            loISC [k][h][lower[h]:upper[h],2],
            nlo[k][h][lower[h]:upper[h],1],
            nlo[k][h][lower[h]:upper[h],2],
            nloISC[k][h][lower[h]:upper[h],1],
            nloISC[k][h][lower[h]:upper[h],2],
            nlol[k][h][lower[h]:upper[h],1],
            nlol[k][h][lower[h]:upper[h],2],
            nlob[k][h][lower[h]:upper[h],1],
            nlob[k][h][lower[h]:upper[h],2],
        ]), delimiter=',', header="xl,xh,LO,dLO,LOISC,dLOISC,NLO,dNLO,NLOISC,dNLOISC,NLOL,dNLOL,NLOnovp,dNLOnovp")
