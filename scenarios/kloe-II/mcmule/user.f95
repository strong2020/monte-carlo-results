                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use mcmule
  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nrq = 17
  integer, parameter :: nrbins = 600
  real(kind=prec), parameter :: &
     min_val(nrq) = (/  30.,  30.,   0.,   0., 200., 200., 215., 215., 591., 0., &
                         0.,  30., 165.,   0.,   0., 591.,   0.                  /)
  real(kind=prec), parameter :: &
     max_val(nrq) = (/ 130., 130., 500., 500., 500., 500., 515., 515., 975., 60., &
                        25., 130., 180.,  15., 500., 975., 500.                  /)

  integer :: userdim = 0
  integer :: hvp

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer :: namesLen=6
  integer :: filenamesuffixLen=10
  integer :: nq=nrq
  integer :: nbins=nrbins



!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU
    implicit none

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "This is a McMule userfile for the KLOE SMALL-ANGLE scenario"
  print*, " * 50deg < th_pm < 130deg"
  print*, " * |pz_pm| > 90 MeV OR |pp_pm| > 160 MeV"
  print*, " * th_tgamma < 15 OR th_tgamma > 165deg"
  print*, " * 0.35 GeV^2 < M^2_XX < 0.95 GeV^2"

  if(which_piece(1:5) == "eb2eb") then
    call initflavour("mu-e", 1.02e3**2)
    print*, " * running e-e+ --> e-e+"
  elseif(which_piece(1:5) == "ee2mm") then
    call initflavour("mu-e", 1.02e3**2)
    print*, " * running e-e+ --> mu-mu+"
  elseif(which_piece(1:5) == "ee2uu") then
    call initflavour("pi-e", 1.02e3**2)
    print*, " * running e-e+ --> pi-pi+"
  endif

  read*, hvp
  write(filenamesuffix,'(I1)') hvp
  if(hvp.eq.1) then
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, " * only hadronic vp for fermionic pieces"
  elseif(hvp.eq.2) then
    nhad = 0._prec
    nel  = 1._prec
    nmu  = 1._prec
    ntau = 1._prec
    print*, " * only leptonic vp for fermionic pieces"
  elseif(hvp.eq.3) then
    HVPmodel = 'nsk-2.9'
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, " * only nsk hadronic vp for fermionic pieces"
  endif
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real(kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real(kind=prec) :: quant(nr_q)
  real(kind=prec) :: thm, phim, pzm, ppm, pm
  real(kind=prec) :: thp, phip, pzp, ppp, pp
  real(kind=prec) :: xi, thav, dphi
  real(kind=prec) :: mxx, ptildegamma(4), ptg2, mtrk
  real(kind=prec) :: thtg, qs(4), qh(4), ths, thh, es, eh

  !! ==== keep the line below in any case ==== !!
  call fix_mu

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)
  pol2 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.

  thm  = acos(cos_th(q1, q3))*180/pi
  phim = phi(q3)*180/pi
  thp  = acos(cos_th(q1, q4))*180/pi
  phip = phi(q4)*180/pi

  xi   = abs(thp+thm-180)
  thav = (thm-thp+180)/2
  dphi = abs(abs(phip-phim)-180)

  pzm = abs(q3(3))
  pzp = abs(q4(3))
  ppm = pt(q3)
  ppp = pt(q4)
  pm  = absvec(q3)
  pp  = absvec(q4)

  mxx         = sqrt(sq(q3+q4))
  ptildegamma = -(q3+q4)
  ptg2        = absvec(ptildegamma)**2

  mtrk = sqrt( &
       &(-2*pp**2*(ptg2 - scms)**2 - 2*sqrt(ptg2*((pm**2 - pp**2)**2 - (ptg2 - scms)**2)&
       &**2*scms) + pm**4*(ptg2 + scms) + pp**4*(ptg2 + scms) + (ptg2 - scms)**2*(ptg2 +&
       & scms) - 2*pm**2*((ptg2 - scms)**2 + pp**2*(ptg2 + scms)))/(4.*(ptg2 - scms)**2))

  if(50 > thm .or. thm > 130) pass_cut = .false.
  if(50 > thp .or. thp > 130) pass_cut = .false.

  if(.not.(pzm > 90 .or. ppm > 160)) pass_cut = .false.
  if(.not.(pzp > 90 .or. ppp > 160)) pass_cut = .false.

  if(which_piece(1:5) == "eb2eb") then
    if(130 > mtrk .or. mtrk > 220) pass_cut(16) = .false.
  elseif(which_piece(1:5) == "ee2mm") then
    if( 80 > mtrk .or. mtrk > 115) pass_cut(16) = .false.
  elseif(which_piece(1:5) == "ee2uu") then
    if(130 > mtrk .or. mtrk > 220) pass_cut(16) = .false.
  endif

  if(5e5 > mxx**2 .or. mxx**2 > 7e5) pass_cut(17) = .false.

  thtg = acos(cos_th(q1,ptildegamma))*180/pi

  if(.not. xor(thtg > 15, thtg < 165)) pass_cut = .false.
  if(mxx < Sqrt(0.35)*1000. .or. mxx > Sqrt(0.95)*1000.) pass_cut = .false.

  names(1) = "sth+"
  quant(1) = thp
  names(2) = "sth-"
  quant(2) = thm
  names(3) = "spz+"
  quant(3) = pzp
  names(4) = "spz-"
  quant(4) = pzm
  names(5) = "spp+"
  quant(5) = ppp
  names(6) = "spp-"
  quant(6) = ppm
  names(7) = "sp+"
  quant(7) = pp
  names(8) = "sp-"
  quant(8) = pm
  names(9) = "smxx"
  quant(9) = mxx
  names(10) = "sxi"
  quant(10) = xi
  names(11) = "sdphi"
  quant(11) = dphi
  names(12) = "sthav"
  quant(12) = thav
  names(13) = "sthtgl"
  quant(13) = thtg
  names(14) = "sthtgs"
  quant(14) = thtg
  names(15) = "smtrk"
  quant(15) = mtrk
  names(16) = "smxxc"
  quant(16) = mxx
  names(17) = "smtrkc"
  quant(17) = mtrk

  END FUNCTION QUANT

  SUBROUTINE GET_SH_G(q5, q6, ths, thh, Es, Eh, qs, qh)
  implicit none
  real(kind=prec), parameter :: ez(4) = (/ 0., 0., 1._prec, 0._prec/)
  real(kind=prec), intent(in) :: q5(4), q6(4)
  real(kind=prec), intent(out) :: ths, thh, Es, Eh, qs(4), qh(4)
  real(kind=prec) :: e5, e6, th5, th6

  e5 = q5(4)
  e6 = q6(4)

  th5 = acos(cos_th(ez,q5))*180/pi
  th6 = acos(cos_th(ez,q6))*180/pi

  if(e5 < e6) then
    ths = th5
    es = e5
    qs = q5
    thh = th6
    eh = e6
    qh = q6
  else
    ths = th6
    es = e6
    qs = q6
    thh = th5
    eh = e5
    qh = q5
  end if

  END SUBROUTINE GET_SH_G

  FUNCTION INDETECTOR(d_thl,d_thu,d_eg, th,eg)
  implicit none
  real(kind=prec), intent(in) :: d_thl,d_thu,d_eg
  real(kind=prec), intent(in) :: th, eg
  logical :: indetector

  indetector = .false.
  if((d_thl < th .and. th < d_thu) .and. eg > d_eg) indetector = .true.
  END FUNCTION


  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
