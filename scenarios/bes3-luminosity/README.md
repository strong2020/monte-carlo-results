# BESIII luminosity scenario

*Note:* This is still a draft, use at your own risk

 * Process: $`ee\to ee`$

 * Energy: $`\sqrt s=4\,\mathrm{GeV}`$

## Variables

We define
 * $`\theta_{\rm avg} = (\theta^+ - \theta^- + \pi)/2`$
 * $`p_\pm = |\vec p_\pm|`$
 * $`\delta\phi = |\phi^+-\phi^-|-\pi`$
 * $`\xi = \theta^++\theta^--\pi`$
 * $`M_{XX} = \sqrt{(p_+ + p_-)^2}`$
 * $`M_{XX\gamma} = \sqrt{(p_+ + p_-+p_\gamma^{(h)})^2}`$

## Cuts

We enforce four cuts
 * $`|\cos\theta_\pm| < 0.8`$
 * $`1\, {\rm GeV} < |p_\pm| < 2.5\,{\rm GeV}`$
 * $`|p_+| + |p_-| > 0.9\times\sqrt{s}`$
 * $`5^\circ < \delta\phi < 40^\circ`$

## Histograms

| variable                 | name    | lower  | upper | bins |
| ------------------------ | ------- | ------ | ----- | ---- |
| $`\cos\theta_+`$         | `cos+`  | 0      | 0.8   | ?    |
| $`\cos\theta_-`$         | `cos-`  | ?      | ?     | ?    |
| $`p_+`$                  | `p+`    | ?      | ?     | ?    |
| $`p_-`$                  | `p+`    | ?      | ?     | ?    |
| $`p_+ + p_-`$            | `psum`  | ?      | ?     | ?    |
| $`\delta\phi`$           | `dphi`  | ?      | ?     | ?    |
| $`\xi`$                  | `xi`    | ?      | ?     | ?    |
| $`M_{XX}`$               | `mxx`   | ?      | ?     | ?    |
