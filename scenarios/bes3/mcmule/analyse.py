from pymule import *

loConv=1000*conv*alpha**3
nloConv=1000*conv*alpha**4

lower = {
    'cth+':  1,
    'cth-':  1,
    'pz+':  16,
    'pz-':  16,
    'pp+':  16,
    'pp-':  16,
    'p+':   16,
    'p-':   16,
    'eg':    2,
    'dphi':  1,
    'xi':    1,
    'mxx':   1,
    'mxxg':  1,
    'cthg':  1,
    'thav':  1,
}
upper = {
    'cth+': 32,
    'cth-': 32,
    'pz+':  -1,
    'pz-':  -1,
    'pp+':  -1,
    'pp-':  -1,
    'p+':   -1,
    'p-':   -1,
    'eg':   81,
    'dphi': -1,
    'xi':   -1,
    'mxx':  -1,
    'mxxg': -1,
    'cthg': -1,
    'thav': -1,
}
setup(merge={
    'cth+': 10,
    'cth-': 10,
    'pz+':   6,
    'pz-':   6,
    'pp+':   6,
    'pp-':   6,
    'p+':    6,
    'p-':    6,
    'eg':    6,
    'dphi': 12,
    'xi':   10,
    'mxx':   6,
    'mxxg':  6,
    'cthg': 12,
    'thav': 12,
})

lo      = {}
loISC   = {}
dnloB   = {}
dnloBISC = {}
dnloFh  = {}
dnloFhISC = {}
dnloFl  = {}
dnloFlISC = {}
nlo     = {}
nloISC = {}
nlol   = {}
nlob   = {}

# Bhabha
setup(folder="bhabha-bosonic/out.tar.bz2")
setup(obs="0")
lo['ee']    = mergefks(sigma('eb2ebR125'), sigma('eb2ebR35'), sigma('eb2ebR45')) * loConv
loISC['ee'] = lo['ee']
dnloB['ee'] = mergefks(sigma('eb2ebRF125'), sigma('eb2ebRF35'), sigma('eb2ebRF45'),
                       sigma('eb2ebRR15162526'), sigma('eb2ebRR3536'), sigma('eb2ebRR4546')) * nloConv
dnloBISC['ee'] = dnloB['ee']
setup(folder="bhabha-fermionic/out.tar.bz2")
setup(obs="3")
dnloFh['ee'] = mergefks(sigma('eb2ebAR125'), sigma('eb2ebAR35'), sigma('eb2ebAR45')) * nloConv
dnloFhISC['ee'] = dnloFh['ee']
setup(obs="2")
dnloFl['ee'] = mergefks(sigma('eb2ebAR125'), sigma('eb2ebAR35'), sigma('eb2ebAR45')) * nloConv
dnloFlISC['ee'] = dnloFl['ee']

nlo['ee']   = lo['ee'] + dnloB['ee'] + dnloFh['ee'] + dnloFl['ee']
nloISC['ee'] = nlo['ee']
nlol['ee']  = lo['ee'] + dnloB['ee'] + dnloFl['ee']
nlob['ee']  = lo['ee'] + dnloB['ee']

# Mupair
setup(folder="muons-bosonic/out.tar.bz2")
setup(obs="0")
lo['mm']    = mergefks(sigma('ee2mmREE'), sigma('ee2mmREM'), sigma('ee2mmRMM')) * loConv
loISC['mm'] = mergefks(sigma('ee2mmREE')) * loConv
dnloB['mm'] = mergefks(sigma('ee2mmRFEEEE'), sigma('ee2mmRFMIXD'),  sigma('ee2mmRFMMMM'),
                       sigma('ee2mmRREEEE'), sigma('ee2mmRRMIXD'),  sigma('ee2mmRRMMMM')) * nloConv
dnloBISC['mm'] = mergefks(sigma('ee2mmRFEEEE'), sigma('ee2mmRREEEE')) * nloConv
setup(folder="muons-fermionic/out.tar.bz2")
setup(obs="3")
dnloFh['mm'] = mergefks(sigma('ee2mmAREE'), sigma('ee2mmAREM'), sigma('ee2mmARMM')) * nloConv
dnloFhISC['mm'] = mergefks(sigma('ee2mmAREE')) * nloConv
setup(obs="2")
dnloFl['mm'] = mergefks(sigma('ee2mmAREE'), sigma('ee2mmAREM'), sigma('ee2mmARMM')) * nloConv
dnloFlISC['mm'] = mergefks(sigma('ee2mmAREE')) * nloConv

nlo['mm']   = lo['mm'] + dnloB['mm'] + dnloFh['mm'] + dnloFl['mm']
nloISC['mm'] = loISC['mm'] + dnloBISC['mm'] + dnloFhISC['mm'] + dnloFlISC['mm']
nlol['mm']  = lo['mm'] + dnloB['mm'] + dnloFl['mm']
nlob['mm']  = lo['mm'] + dnloB['mm']

# Pions
setup(folder="pions-bosonic/out.tar.bz2")
setup(obs="0")
lo['uu']    = mergefks(sigma('ee2uuREE'),
                       sigma('ee2uuREU'),
                       sigma('ee2uuRUU')) * loConv
loISC['uu'] = mergefks(sigma('ee2uuREE')) * loConv
dnloB['uu'] = mergefks(sigma('ee2uuRFEEEE'), sigma('ee2uuRREEEE')) * nloConv

nlo['uu']   = lo['uu'] + dnloB['uu']
nloISC['uu'] = loISC['uu'] + dnloB['uu']
nlol['uu']  = nlo['uu']
nlob['uu']  = nlo['uu']

# Export
for k in lo.keys():
    print(f"CHANNEL {k}: run-time = {nlo[k].time:.2f}s")
    print(f"LO xsec = {lo[k].value[0]} +- {lo[k].value[1]} nb")
    print(f"LOISC xsec = {loISC[k].value[0]} +- {loISC[k].value[1]} nb")
    print(f"NLO xsec  = {nlo[k].value[0]} +- {nlo[k].value[1]} nb")
    print(f"NLOL xsec  = {nlol[k].value[0]} +- {nlol[k].value[1]} nb")
    print(f"NLOB xsec  = {nlob[k].value[0]} +- {nlob[k].value[1]} nb")
    for h in lo[k].histograms:
        w = lo[k][h][2,0] - lo[k][h][1,0]
        np.savetxt(f'data/{k}-{h}.csv', np.column_stack([
            lo [k][h][lower[h]:upper[h],0] - w/2,
            lo [k][h][lower[h]:upper[h],0] + w/2,
            lo [k][h][lower[h]:upper[h],1],
            lo [k][h][lower[h]:upper[h],2],
            loISC [k][h][lower[h]:upper[h],1],
            loISC [k][h][lower[h]:upper[h],2],
            nlo[k][h][lower[h]:upper[h],1],
            nlo[k][h][lower[h]:upper[h],2],
            nloISC[k][h][lower[h]:upper[h],1],
            nloISC[k][h][lower[h]:upper[h],2],
            nlol[k][h][lower[h]:upper[h],1],
            nlol[k][h][lower[h]:upper[h],2],
            nlob[k][h][lower[h]:upper[h],1],
            nlob[k][h][lower[h]:upper[h],2],
        ]), delimiter=',', header="xl,xh,LO,dLO,LOISC,dLOISC,NLO,dNLO,NLOISC,dNLOISC,NLOL,dNLOL,NLOnovp,dNLOnovp")
