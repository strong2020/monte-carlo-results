                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use mcmule
  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nrq = 15
  integer, parameter :: nrbins = 600
  integer :: scenario
  real(kind=prec), parameter :: &
     min_val(nrq) = (/ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, &
                       0.0, 0.3, 0.0, 0.0, 0.0                           /)
  real(kind=prec), parameter :: &
     max_val(nrq) = (/ 1.8, 1.8, 2000., 2000., 2000., 2000., 2000., 2000., 4000., 2.4, &
                       1.0, 2.8, 4000., 2500.,   1.0                                   /)

  integer :: userdim = 0
  integer :: hvp

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer :: namesLen=6
  integer :: filenamesuffixLen=10
  integer :: nq=nrq
  integer :: nbins=nrbins



!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains


  SUBROUTINE FIX_MU
    implicit none

  musq = me**2

  END SUBROUTINE FIX_MU



  SUBROUTINE INITUSER
  print*, "This is a McMule userfile for the BESIII scenario"
  print*, " * |cth_pm| < 0.93"
  print*, " * |pp_pm| > 300 MeV"
  print*, " * |cth_gamma| < 0.8 AND E_gamma > 25 MeV OR ..."
  print*, " ... 0.86 < |cth_gamma| < 0.92 AND E_gamma > 50 MeV"
  print*, " * one & only one photon with E_gamma > 400 MeV in detector"
  print*, " * running e-e+ --> e-e+"

  call initflavour("mu-e", 4.e3**2)

  read*, hvp
  write(filenamesuffix,'(I1)') hvp
  if(hvp.eq.1) then
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, " * only hadronic vp for fermionic pieces"
  elseif(hvp.eq.2) then
    nhad = 0._prec
    nel  = 1._prec
    nmu  = 1._prec
    ntau = 1._prec
    print*, " * only leptonic vp for fermionic pieces"
  elseif(hvp.eq.3) then
    HVPmodel = 'nsk-2.9'
    nhad = 1._prec
    nel  = 0._prec
    nmu  = 0._prec
    ntau = 0._prec
    print*, " * only nsk hadronic vp for fermionic pieces"
  endif
  END SUBROUTINE


  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: quant(nr_q)
  real(kind=prec) :: cthm, thm, phim, pzm, ppm, pm
  real(kind=prec) :: cthp, thp, phip, pzp, ppp, pp
  real(kind=prec) :: xi, thav, dphi
  real(kind=prec) :: mxx
  real(kind=prec) :: qs(4), qh(4), ths, thh, es, eh
  real(kind=prec) :: qhtag(4), eg, cthg, mxxg
  logical :: hardin1,softin1,hardin2,softin2

  !! ==== keep the line below in any case ==== !!
  call fix_mu

  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)
  pol2 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  pass_cut = .true.

  cthm = cos_th(q1, q3)
  thm  = acos(cthm)
  phim = phi(q3)
  cthp = cos_th(q1, q4)
  thp  = acos(cthp)
  phip = phi(q4)

  xi = abs(thp+thm-pi)
  thav = (thm-thp+pi)/2
  dphi = abs(abs(phip-phim)-pi)

  pzm = abs(q3(3))
  pzp = abs(q4(3))
  ppm = pt(q3)
  ppp = pt(q4)
  pm = absvec(q3)
  pp = absvec(q4)

  mxx = sqrt(sq(q3+q4))

  if(-0.93 > cthm .or. cthm > 0.93) pass_cut = .false.
  if(-0.93 > cthp .or. cthp > 0.93) pass_cut = .false.

  if(ppm < 300) pass_cut = .false.
  if(ppp < 300) pass_cut = .false.

  call get_sh_g(q5, q6, ths, thh, Es, Eh, qs, qh) !ths/thh in rad

  hardin1 = indetector( .0, .8,25., cos(thh),eh)
  softin1 = indetector( .0, .8,25., cos(ths),es)
  hardin2 = indetector(.86,.92,50., cos(thh),eh)
  softin2 = indetector(.86,.92,50., cos(ths),es)

  if(.not.(hardin1.or.softin1.or.hardin2.or.softin2)) pass_cut = .false.

  if(hardin1.or.hardin2) then
     qhtag = qh
     if(softin1.or.softin2) then  ! hard&soft photon IN detector
       if(es>400.or.eh<400) pass_cut = .false.
     elseif(.not.(softin1.or.softin2)) then ! only hard photon IN detector
       if(eh<400) pass_cut = .false.
     endif
  elseif(softin1.or.softin2) then ! only soft photon IN detector
    qhtag = qs
    if(es<400) pass_cut = .false.
  endif

  eg = qhtag(4)
  cthg = cos_th(q1, qhtag)
  mxxg = sqrt(sq(q3+q4+qhtag))

  names(1) = "cth+"
  quant(1) = abs(cthp)
  names(2) = "cth-"
  quant(2) = abs(cthm)
  names(3) = "pz+"
  quant(3) = pzp
  names(4) = "pz-"
  quant(4) = pzm
  names(5) = "pp+"
  quant(5) = ppp
  names(6) = "pp-"
  quant(6) = ppm
  names(7) = "p+"
  quant(7) = pp
  names(8) = "p-"
  quant(8) = pm
  names(9) = "mxx"
  quant(9) = mxx
  names(10) = "xi"
  quant(10) = xi
  names(11) = "dphi"
  quant(11) = dphi/pi
  names(12) = "thav"
  quant(12) = thav
  names(13) = "mxxg"
  quant(13) = mxxg
  names(14) = "eg"
  quant(14) = eg
  names(15) = "cthg"
  quant(15) = abs(cthg)

  END FUNCTION QUANT

  SUBROUTINE GET_SH_G(q5, q6, ths, thh, Es, Eh, qs, qh)
  implicit none
  real(kind=prec), parameter :: ez(4) = (/ 0., 0., 1._prec, 0._prec/)
  real(kind=prec), intent(in) :: q5(4), q6(4)
  real(kind=prec), intent(out) :: ths, thh, Es, Eh, qs(4), qh(4)
  real(kind=prec) :: e5, e6, th5, th6

  e5 = q5(4)
  e6 = q6(4)

  th5 = acos(cos_th(ez,q5))
  th6 = acos(cos_th(ez,q6))

  if(e5 < e6) then
    ths = th5
    es = e5
    qs = q5
    thh = th6
    eh = e6
    qh = q6
  else
    ths = th6
    es = e6
    qs = q6
    thh = th5
    eh = e5
    qh = q5
  end if

  END SUBROUTINE GET_SH_G

  FUNCTION INDETECTOR(d_cthl,d_cthu,d_eg, cth,eg)
  implicit none
  real(kind=prec), intent(in) :: d_cthl,d_cthu,d_eg
  real(kind=prec), intent(in) :: cth, eg
  logical :: indetector

  indetector = .false.
  if((d_cthl < abs(cth) .and. abs(cth) < d_cthu) .and. eg > d_eg) indetector = .true.
  END FUNCTION

  SUBROUTINE USEREVENT(X, NDIM)
  integer :: ndim
  real(kind=prec) :: x(ndim)
  userweight = 1.
  END SUBROUTINE USEREVENT


                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!
