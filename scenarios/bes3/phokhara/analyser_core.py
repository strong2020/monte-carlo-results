# Merges all the results from the individual cores for a certain order and final state, plots the results and saves the log files in the Outputs folder, 
# calculates the total sigma_MC 

import pandas as pd
import numpy as np
import os
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('n_cores')
parser.add_argument('final')
parser.add_argument('order')
args = parser.parse_args()

def plotter(columns, weights, bins, error, savename, plotting=False):
      figure = plt.figure()
      axes = figure.add_subplot()
      y, bin_limits, _ = axes.hist(columns, bins=bins, weights=weights )
      bin_centers = 0.5*(bin_limits[1:] + bin_limits[:-1])
      axes.errorbar(bin_centers, y, yerr = error, marker = '.', linestyle="none")

      if (plotting):
          figure.savefig(savename)
      plt.close()
      return

sigma_MC = 0
error_MC = 0

titles = []
cores_working = []
for core in range(1,int(args.n_cores)+1):
     try: 
      with open(f"{args.final}_{args.order}_{core}/dummy.out", 'r') as file:
           contents = file.read()
           if "Warning! Max(2) too small!" not in contents and "sigma_MC" in contents:
                cores_working.append(core)
     except:
          continue
    
print(f"Len Working cores: ",len(cores_working),"cores_working = ",cores_working)
for core in cores_working:
     
 with open(f"{args.final}_{args.order}_{core}/{args.order}.dat", 'r') as file:
         # Iterate through each line in the file
         values = []
         title = None
         for line in file:
             words = line.split()       
             try:
                 for word in words:
                      values.append(float(word))
             except:
                  if len(values)!=0 and title!=None:
                       data = pd.DataFrame(np.array(values).reshape((int(len(values)/4),4)), columns=["xl", "xh", f"{args.order}", f"d{args.order}"])
                       data.to_csv(f"{title}_{args.order}_{core}_temp.csv", index=False)
                       values.clear()
                       title = None
                  if "Summed" in words:
                       title = words[1]
                       if core == cores_working[0]:
                           titles.append(title)
         if len(values)!=0 and title!=None:
                       data = pd.DataFrame(np.array(values).reshape((int(len(values)/4),4)), columns=["xl", "xh", f"{args.order}", f"d{args.order}"])
                       
                       data.to_csv(f"{title}_{args.order}_{core}_temp.csv", index=False)
                       values.clear()
                       title = None
 if core == cores_working[0]:
      os.system(f"cat {args.final}_{args.order}_{core}/dummy.out > {args.final}/Outputs/{args.order}.out")
 else:
      os.system(f"cat {args.final}_{args.order}_{core}/dummy.out >> {args.final}/Outputs/{args.order}.out")

 with open(f"{args.final}_{args.order}_{core}/dummy.out", 'r') as file:
      for line in file:
             words = line.split()
             if "sigma_MC" in words:
                  sigma_MC += float(words[3])
                  error_MC += float(words[5])**2
      if core == cores_working[-1]:
           sigma_MC = sigma_MC/(int(len(cores_working)))
           error_MC = np.sqrt(error_MC)/(int(len(cores_working)))
           os.system(f"echo 'Total sigma_MC = {sigma_MC} +/- {error_MC}\n' | cat >> {args.final}/Outputs/{args.order}.out")

for title in titles:
     for core in cores_working:
          if core == cores_working[0]:
              data = pd.read_csv(f"{title}_{args.order}_{core}_temp.csv")

          elif core not in cores_working:
               continue
          else:
              data_dummy = pd.read_csv(f"{title}_{args.order}_{core}_temp.csv")
              data[f"{args.order}"] = data_dummy[f"{args.order}"] + data[f"{args.order}"] 
              if core == cores_working[1]:     data[f"d{args.order}"] = data[f"d{args.order}"]**2         
              data[f"d{args.order}"] = data_dummy[f"d{args.order}"]**2 + data[f"d{args.order}"]
              
          
          os.remove(f"{title}_{args.order}_{core}_temp.csv")
     data[f"{args.order}"] = data[f"{args.order}"]/(int(len(cores_working)))
     data[f"d{args.order}"] = np.sqrt(data[f"d{args.order}"])/(int(len(cores_working)))
     plotter(data.xl.values, data[f"{args.order}"].values, len(data), data[f"d{args.order}"].values, f"{args.final}/Plots/{args.order}_{title}.png", plotting=True)
     data.to_csv(f"{args.final}/Results/{args.order}_{title}.csv", index=False)

if args.order == "NLO":
     if args.final=="Muons": orders = ["LO", "NLOnovp", "NLO"]
     if args.final=="Pions": orders = ["LO", "LOISR", "NLO"]

     for title in titles:
          for i, order in enumerate(orders):
               if i == 0:
                   data = pd.read_csv(f"{args.final}/Results/{order}_{title}.csv")
               else:
                   data_dummy = pd.read_csv(f"{args.final}/Results/{order}_{title}.csv")
                   data[f"{order}"] = data_dummy[f"{order}"]                  
                   data[f"d{order}"] = data_dummy[f"d{order}"]
                   
               os.remove(f"{args.final}/Results/{order}_{title}.csv")
          data.to_csv(f"{args.final}/Results/{title}.csv", index=False)
 
 