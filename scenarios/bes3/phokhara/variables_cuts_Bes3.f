c ========================================================================
c Histograms for dxs/variables for Bes3 scenario in Strong2020. 
c ------------------------------------------------------------------------
      subroutine output_variables(n, value, i)
       ! n refers to a process with 0, 1 (n=0) or 2 (n=1) photons
       ! value is the weight for the MC histograms
       ! i is 0 (1) for the (un)weighted histograms
      include 'phokhara_10.0.inc'
      logical g1_pass_cuts, g2_pass_cuts
      integer i, n, k, g1_cuts, g2_cuts
      real*8 value, cthp, cthm, ppp, ppm, pp, pm, eg, dphi, xi
      real*8 mxx, mxxg, cthg, thav, phip, phim, cthg1, cthg2
      real*8 g1_energy, g2_energy
      real*8, allocatable :: variables(:)
      allocate(variables(1:total_num_hist))

      ! Calculate variables of interest
      pp = dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2) * 1000
      pm = dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2) * 1000
      cthp = -momenta(6,3) * 1000 / pp  
      cthm = -momenta(7,3) * 1000 / pm
      ppp = dSqrt(momenta(6,1)**2+momenta(6,2)**2) * 1000
      ppm = dSqrt(momenta(7,1)**2+momenta(7,2)**2) * 1000
      xi = abs(dacos(cthp)+dacos(cthm)-pi)
      thav = (-dacos(cthp)+dacos(cthm)+pi)/2
      phip = atan2(momenta(6,2), momenta(6,1))
      phim = atan2(momenta(7,2), momenta(7,1))
      dphi = abs(abs(phip-phim) - pi)
      mxx =dSqrt((momenta(6,0) + momenta(7,0))**2 -
     &       (momenta(6,1) + momenta(7,1))**2 - 
     &       (momenta(6,2) + momenta(7,2))**2 - 
     &       (momenta(6,3) + momenta(7,3))**2) * 1000

      cthg1 = -momenta(3,3)/
     &         dSqrt(momenta(3,1)**2+momenta(3,2)**2+momenta(3,3)**2)
      cthg2 = -momenta(4,3)/
     &         dSqrt(momenta(4,1)**2+momenta(4,2)**2+momenta(4,3)**2)

      if (cthg1 /= cthg1) cthg1 = 1
      if (cthg2 /= cthg2) cthg2 = 1  ! If it is a nan, set to 1 (won't pass the cuts)

      g1_pass_cuts = (((ABS(cthg1).lt.0.8).and.(momenta(3,0).gt.0.025)
     &     ).or.((ABS(cthg1).gt.0.86).and.(ABS(cthg1).lt.0.92).and.
     &     (momenta(3,0).gt.0.05)))

      g2_pass_cuts = (((ABS(cthg2).lt.0.8).and.(momenta(4,0).gt.0.025)
     &     ).or.((ABS(cthg2).gt.0.86).and.(ABS(cthg2).lt.0.92).and.
     &     (momenta(4,0).gt.0.05)))

      g1_cuts = g1_pass_cuts
      g2_cuts = g2_pass_cuts
      g1_energy = g1_cuts * momenta(3,0)
      g2_energy = g2_cuts * momenta(4,0)

      if (g1_energy.gt.g2_energy) then
       eg = g1_energy * 1000
       cthg = cthg1
       mxxg = dSqrt((momenta(6,0) + momenta(7,0) + momenta(3,0))**2 -
     &       (momenta(6,1) + momenta(7,1) + momenta(3,1))**2 - 
     &       (momenta(6,2) + momenta(7,2) + momenta(3,2))**2 - 
     &       (momenta(6,3) + momenta(7,3) + momenta(3,3))**2) * 1000

      else
       eg = g2_energy * 1000
       cthg = cthg2
       mxxg = dSqrt((momenta(6,0) + momenta(7,0) + momenta(4,0))**2 -
     &       (momenta(6,1) + momenta(7,1) + momenta(4,1))**2 - 
     &       (momenta(6,2) + momenta(7,2) + momenta(4,2))**2 - 
     &       (momenta(6,3) + momenta(7,3) + momenta(4,3))**2) * 1000
      end if
      ! Save to variables in the same order as the requested histograms
      variables = (/ABS(cthp), ABS(cthm), ppp, ppm, pp, pm, eg, dphi/pi,   
     &              xi, mxx, mxxg, abs(cthg), thav/)

      ! Save to the corersponding histograms
      call distribute_variables(variables, i, n, value)

      return
      end

c ========================================================================
c --- Experimental cuts for Bes3 scenario Strong2020
c ------------------------------------------------------------------------
      subroutine advanced_testcuts(accepted)
      include 'phokhara_10.0.inc'
      logical accepted, ph1_pass_cuts, ph2_pass_cuts
      real*8 cthp, cthm,cthg1, cthg2
      real*8 ppp, ppm

      ! Cut on cth+ and pp+
      cthp = -momenta(6,3)/
     &         dSqrt(momenta(6,1)**2+momenta(6,2)**2+momenta(6,3)**2)

      ppp = dSqrt(momenta(6,1)**2+momenta(6,2)**2)

      accepted = (accepted.and.(ABS(cthp).lt.0.93).and.(ppp.gt.0.3))
      if (.not.accepted) return
      
      ! Cut on cth- and pp-
      cthm = -momenta(7,3)/
     &         dSqrt(momenta(7,1)**2+momenta(7,2)**2+momenta(7,3)**2)

      ppm = dSqrt(momenta(7,1)**2+momenta(7,2)**2)

      accepted = (accepted.and.(ABS(cthm).lt.0.93).and.(ppm.gt.0.3))
      if (.not.accepted) return

      ! Cuts on cth photons
      cthg1 = -momenta(3,3)/
     &         dSqrt(momenta(3,1)**2+momenta(3,2)**2+momenta(3,3)**2)
      cthg2 = -momenta(4,3)/
     &         dSqrt(momenta(4,1)**2+momenta(4,2)**2+momenta(4,3)**2)

      if (cthg1 /= cthg1) cthg1 = 1
      if (cthg2 /= cthg2) cthg2 = 1  ! If it is a nan, set to 1 (won't pass the cuts)

      ph1_pass_cuts = (((ABS(cthg1).lt.0.8).and.(momenta(3,0).gt.0.025)
     &     ).or.((ABS(cthg1).gt.0.86).and.(ABS(cthg1).lt.0.92).and.
     &     (momenta(3,0).gt.0.05)))

      ph2_pass_cuts = (((ABS(cthg2).lt.0.8).and.(momenta(4,0).gt.0.025)
     &     ).or.((ABS(cthg2).gt.0.86).and.(ABS(cthg2).lt.0.92).and.
     &     (momenta(4,0).gt.0.05)))
      

      if (ph1_pass_cuts.and.ph2_pass_cuts) then

          if (momenta(3,0).gt.momenta(4,0)) then
              accepted = accepted.and.(momenta(3,0).gt.0.4)
     &                   .and.(momenta(4,0).lt.0.4)
          else
              accepted = accepted.and.(momenta(4,0).gt.0.4)
     &                  .and.(momenta(3,0).lt.0.4)
          end if

      elseif (ph1_pass_cuts) then
           accepted = accepted.and.(momenta(3,0).gt.0.4)
      elseif (ph2_pass_cuts) then
          accepted = accepted.and.(momenta(4,0).gt.0.4)   
      else
          accepted = .false.
      end if

      return
      end 