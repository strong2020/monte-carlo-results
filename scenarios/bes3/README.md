# BESIII scenario

 * Process: $`ee\to ee,\mu\mu,\pi\pi + \gamma`$

 * Energy: $`\sqrt s=4\,\mathrm{GeV}`$

## Variables

We define
 * $`\theta_{\rm avg} = (\theta^- - \theta^+ + \pi)/2`$
 * $`p_\pm = |\vec p_\pm|`$
 * $`\delta\phi = ||\phi^+-\phi^-|-\pi|`$
 * $`\xi = |\theta^++\theta^--\pi|`$
 * $`M_{XX} = \sqrt{(p_+ + p_-)^2}`$
 * $`M_{XX\gamma} = \sqrt{(p_+ + p_-+p_{\gamma^{(h)}})^2}`$

$`p_{\gamma^{(h)}}`$ corresponds to the hardest photon, i.e. the one with the largest energy among those passing angle and energy cuts.

The scattering angles are defined with respect to the electron momentum
```math
\cos \theta^{\pm} = \frac{p^{\pm} \cdot p_{e^-}}{|p^{\pm}|\,|p_{e^-}|}
```

## Cuts

We enforce cuts
 * $`|\cos\theta_\pm| < 0.93`$ $\land$ $`p^\bot_{\pm} > 300\,\mathrm{MeV}`$
 * ($`|\cos\theta_{\gamma}| < 0.8`$ $\land$ $`E_\gamma > 25\,{\rm MeV}`$)  $\lor$ ($`0.86<|\cos\theta_{\gamma}|<0.92`$ $\land$ $`E_{\gamma}>50\,\mathrm{MeV}`$)
 * $`\exists! \text{photon with } E_{\gamma} > 400\,{\rm MeV} \text{ passing the cut}`$

## Histograms

Uniform binning is achieved with 600 bins

| variable                     | name   | lower  | upper | bins |  | lower | upper | selection |
| ---------------------------- | ------ | ------ | ----- | ---- |--| ----- | ----- | --------- |
| $`\vert\cos\theta_+\vert`$   | `cth+` | 0      | 0.93  | 31   |  | 0     | 1.8   | `:310:10` |
| $`\vert\cos\theta_-\vert`$   | `cth-` | 0      | 0.93  | 31   |  | 0     | 1.8   | `:310:10` |
| $`p^\bot_+`$                 | `pp+`  | 300    | 2000  | 85   |  | 0     | 2000  | `90::6`   |
| $`p^\bot_-`$                 | `pp-`  | 300    | 2000  | 85   |  | 0     | 2000  | `90::6`   |
| $`p_+`$                      | `p+`   | 300    | 2000  | 85   |  | 0     | 2000  | `90::6`   |
| $`p_-`$                      | `p-`   | 300    | 2000  | 85   |  | 0     | 2000  | `90::6`   |
| $`E_{\gamma^{(h)}}`$         | `eg`   | 25     | 2000  | 79   |  | 0     | 2500  | `6:480:6` |
| $`\delta\phi/\pi`$           | `dphi` | 0      | 1.00  | 50   |  | 0     | 1.0   | `::12`    |
| $`\xi`$                      | `xi`   | 0      | 2.4   | 60   |  | 0     | 2.4   | `::10`    |
| $`M_{ee}`$                   | `mxx`  | 0      | 4000  | 100  |  | 0     | 4000  | `::6`     |
| $`M_{\mu\mu}`$               | `mxx`  | 80     | 4080  | 100  |  | 80    | 4080  | `::6`     |
| $`M_{\pi\pi}`$               | `mxx`  | 80     | 4080  | 100  |  | 80    | 4080  | `::6`     |
| $`M_{\pi\pi}`$ (optional)    | `mxxrw` | 0    | 2400   | 600  |  | 0    | 2400   |      |
| $`M_{XX\gamma}`$             | `mxxg` | 0      | 4000  | 100  |  | 0     | 4000  | `::6`     |
| $`\vert\cos\theta_{\gamma^{(h)}}\vert`$ | `cthg` | 0 | 1.00|50|  | 0     | 1.0   | `::12`    |
| $`\theta_{\rm avg}`$         | `thav` | 0.3    | 2.8   | 50   |  | 0.3   | 2.8   | `::12`    |
